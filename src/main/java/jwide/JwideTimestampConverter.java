/*
 * Created on 28/02/2005
 */
package jwide;

import org.apache.commons.beanutils.Converter;
import org.glassfish.json.JsonParserImpl;
import org.apache.commons.beanutils.ConversionException;
import java.util.Date;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author MHRG
 */

public class JwideTimestampConverter implements Converter {
	
   JwideUtil jwUtil = JwideUtil.getInstance();
	
   public JwideTimestampConverter() {   	
   }

   /* (non-Javadoc)
      * @see org.apache.commons.beanutils.Converter#convert(java.lang.Class,
   java.lang.Object)
   */
   
   @SuppressWarnings({ "rawtypes", "unchecked" })
   public Object convert(Class type, Object value) throws ConversionException {
	   if (value == null) return null;

      // Support Calendar and Timestamp conversion
      if (value instanceof Timestamp) {
      	 return value;         
      }
      else if (value instanceof Date) {
         return new Timestamp(((Date)value).getTime());
      }
      else if (value instanceof Calendar) {
         return new Timestamp(((Calendar)value).getTime().getTime());
      }
      else if (value instanceof String) {
          return jwUtil.strToTimestamp(value.toString().trim());    	  
      } else if (value instanceof JsonParserImpl){
    	  return jwUtil.strToTimestamp(((JsonParserImpl) value).toString());
      }
      else {
         throw new ConversionException("Type not supported: " +
         value.getClass().getName());
      }
   }    
    
}

