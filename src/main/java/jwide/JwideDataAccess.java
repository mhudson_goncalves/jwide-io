package jwide;

import jwide.JwideVO;
import jwide.annotations.Bean;
import jwide.annotations.Sequence;
import jwide.JwideConsts;
import jwide.JwideDatabaseObjects;
import jwide.JwideQuery;
import jwide.JwideDTO;
import jwide.JwideUtil;
import jwide.JwideDataSource;
import jwide.JwideReflection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtilsBean;

public class JwideDataAccess {	
	
	private JwideDataObject propertiesValueObject = null;
	
	private JwideDatabaseObjects DbObjects = new JwideDatabaseObjects();
	private JwideReflection Reflect = JwideReflection.getInstance();
	private JwideUtil       JwUtil  = JwideUtil.getInstance();
	
	private List<JwideVO> RelationsVO = new ArrayList<>();
	private List<String>  RelationsType  = new ArrayList<>();	
	private List<String>  RelationsSql  = new ArrayList<>();
	private List<String>  RelationsAlias= new ArrayList<>();	
	
	private List<Object>  UserParameterValues= new ArrayList<>();
	private List<String>  UserParameterNames= new ArrayList<>();	
	
	private JwideDataSource DataSource = null;
	private PropertyUtilsBean PropUtils=new PropertyUtilsBean();
    private String fields=null;
    private String where=null;
    private String groupBy=null;
    private String having=null;
    private String orderBy=null;
    private String sql="";
    
    private static final String _SELECT=" select ";
    private static final String _FROM=" from ";
    private static final String _WHERE=" where ";
    private static final String _GROUP_BY=" group by ";
    private static final String _HAVING=" having ";
    private static final String _ORDER_BY=" order by ";
    private static final String _AND=" and ";
    private static final String _ON=" on ";    
    private static final String _LEFT_JOIN=" left join ";
    private static final String _INNER_JOIN=" inner join ";
    private static final String _UPDATE=" update ";
    private static final String _INSERT=" insert into ";
    private static final String _VALUES=" values ";
    private static final String _DELETE=" delete from ";
    private static final String _LIMIT=" limit ";
    
    public int maxRows=0;
	
	public JwideDataAccess() {
		
	}
		
	public JwideDTO find(JwideVO VO) throws Exception {
		String subQuery=null;
		return this.find(VO,subQuery);
	}
	
	public JwideDTO find(JwideVO VO,String subQuery) throws Exception {
		prepareDMLCommands(VO);		

		StringBuilder sql = new StringBuilder("");
		sql.append(mountSelect(VO));
		sql.append(mountFrom(VO,subQuery));
		sql.append(mountWhere(VO));
		sql.append(mountGroupBy(VO));
		sql.append(mountHaving(VO));
		sql.append(mountOrderBy(VO));			
		return executeQuery(sql.toString(),VO);	
	}
		
	public Integer update(JwideVO VO) throws Exception {
		prepareDMLCommands(VO);

		StringBuilder sql = new StringBuilder("");
		sql.append(mountUpdate(VO)); 			
		sql.append(mountWhere(VO)); 						
		return executeUpdate(sql.toString(),VO); 			 
	}
     
	public Integer insert(JwideVO VO) throws Exception {
		prepareDMLCommands(VO);		
		StringBuilder sql = new StringBuilder("");
		sql.append(mountInsert(VO));
		this.generateSequences(VO);
		return this.executeUpdate(sql.toString(),VO);
	}
	
	public Integer delete(JwideVO VO) throws Exception {
		prepareDMLCommands(VO);

		StringBuilder sql = new StringBuilder("");			
		sql.append(mountDelete(VO));
		sql.append(mountWhere(VO));			
		return executeUpdate(sql.toString(),VO);		  	 
	}
	
    public String getSql() {
    	return sql;
    }
	public JwideDataAccess setFields(String fields) {
		this.fields=fields;
		return this;
	}
	public String getFields() {
		return this.fields;
	}
	public JwideDataAccess setWhere(String where) {
		this.where=where;
		return this;
	}
	public JwideDataAccess addWhere(String where){
		StringBuilder bWhere = new StringBuilder(this.where!=null?this.where:"");
		bWhere.append(" ");
		bWhere.append(where);
		this.where=bWhere.toString();
		return this;
	}
	public JwideDataAccess setGroupBy(String groupBy) {
		this.groupBy=groupBy;	
		return this;
	}
	public JwideDataAccess setHaving(String having) {
		this.having=having;	
		return this;
	}
	public JwideDataAccess setOrderBy(String orderBy) {
		this.orderBy=orderBy;	
		return this;
	}
	// relacionamento entre tabelas via left join
	public JwideDataAccess addLeftRelation(JwideVO VO,String sql) {
		this.addLeftRelation(VO,sql,this.getRawTableName(VO));
		return this;
	}
	public JwideDataAccess addLeftRelation(JwideVO VO,String sql,String alias) {
		RelationsVO.add(VO);
		RelationsSql.add(sql);
		RelationsAlias.add((alias!=null?alias.trim():this.getRawTableName(VO)));
		RelationsType.add(_LEFT_JOIN);
		return this;
	}
	// relacionamento entre tabelas via inner join
	public JwideDataAccess addInnerRelation(JwideVO VO,String sql) {
		this.addInnerRelation(VO,sql,this.getRawTableName(VO));
		return this;
	}
	public JwideDataAccess addInnerRelation(JwideVO VO,String sql,String alias) {
		RelationsVO.add(VO);
		RelationsSql.add(sql);
		RelationsAlias.add((alias!=null?alias.trim():this.getRawTableName(VO)));
		RelationsType.add(_INNER_JOIN);
		return this;
	}
	// parametros do usuario
	public JwideDataAccess addParameterValue(String parameterName,Object parameterValue) {
		UserParameterNames.add(parameterName);
		UserParameterValues.add(parameterValue);
		return this;
	}
	// numero maximo de colunas
	public JwideDataAccess setMaxRows(int maxRows) {
		this.maxRows=maxRows;
		return this;
	}
	
	private void createDataSource(String connectionString) throws Exception  {
		DataSource = new JwideDataSource(connectionString);
	}
	
	private void populatePropertiesValueObject(JwideVO VO) throws Exception {
		if (!VO.getClass().isAnnotationPresent(Bean.class)) {
			propertiesValueObject = new JwideDataObject(
					                                    Arrays.asList("connection_string", "primary_key", "aowner"),
					                                    Arrays.asList(new Object[]{"", "", ""})
					                                   );
		} else {
			Bean bean =(Bean) VO.getClass().getAnnotation(Bean.class);
			propertiesValueObject = new JwideDataObject(
					                                    Arrays.asList("connection_string", "primary_key", "aowner"),
					                                    Arrays.asList(bean.connection(), bean.primaryKey(), bean.aowner())
					                                   );
		}
	}
	
	private JwideDTO executeQuery(String sql, JwideVO VO) throws Exception {

		this.sql=sql;
		JwideDTO Dto = new JwideDTO();					

		JwideQuery Query = this.prepareQuery(sql,VO);
		Query.executeQuery();
		Dto.populate(Query.getResultSet());
		Query.closeStatement();
		Query.closeResultSet();
		
		this.clear();

		return Dto;		
	}
	
	private Integer executeUpdate(String sql,JwideVO VO) throws Exception {
		this.sql=sql;

		JwideQuery Query = this.prepareQuery(sql, VO);
		Query.executeUpdate();			
		Query.closeStatement();	
		this.clear();
		return Query.getUpdateCount();
	}
	
	private void generateSequences(JwideVO VO) throws Exception {
		JwideSimpleQuery simpleQuery = null;
		
		for (Field field : VO.getClass().getDeclaredFields()) {			
		    if (field.isAnnotationPresent(Sequence.class)) {
		    	
		    	if (simpleQuery==null){
		    		simpleQuery = new JwideSimpleQuery();
		    	}
		    	
		        String sequence = field.getAnnotation(Sequence.class).name();
		        field.setAccessible(true);		        
		        field.set(VO, simpleQuery.nextVal(DataSource.getConnection(), sequence));
		    }
		}	
	}
	
	private JwideQuery prepareQuery(String sql,JwideVO VO) throws Exception {		
		JwideQuery Query = new JwideQuery();
		
		Query.setSql(sql);	
		Query.setConnection(DataSource.getConnection());
		if (JwideDatabaseObjects.isPostgreSqlConnection(DataSource.getConnection())){
			if (maxRows>0){
				Query.setSql(Query.getSql()+_LIMIT+maxRows);			
			}
		}

		Query.setMaxRows(maxRows);
		Query.prepareStatement();

		// setar os parametros do vo			
		for (String parameterName:Query.getParameterNames()) {
			if (!isUserParameter(parameterName)) {				
				Query.setParameterValue(parameterName,PropUtils.getSimpleProperty(VO,parameterName.toLowerCase()));				
			}
		}
		// setar os parametros do usuario
		int u=0;
		for (String parameterName:UserParameterNames) {			
			Query.setParameterValue(parameterName,UserParameterValues.get(u++));			
		}
		
		return Query;
	}	

	private Boolean isUserParameter(String name) {
		for (String parameterName:UserParameterNames) {
		   if (parameterName.equalsIgnoreCase(name)) return true;	
		}
		return false;
	}
	
	private String mountSelect(JwideVO VO) throws Exception {
		StringBuilder selectClause= new StringBuilder("");
		List<String> fieldNames=new ArrayList<String>();
		if (fields!=null) fieldNames=getFields(fields);
		else {
			// campos da tabela principal
			for (String fieldName:getFields(VO)) {
			    fieldNames.add(this.getRawTableName(VO)+"."+fieldName);	    		
			}			
			// campos das tabelas relacionadas
			int l=0;
			for (JwideVO RelationVO:RelationsVO) {
				for (String fieldName:getFields(RelationVO)) {					 
					fieldNames.add(this.getRelationTableNameOrAlias(RelationVO,l)+"."+fieldName);	    		
				}	
				l++;
			}
		}
		
	    if (fieldNames.size()==0) {
	    	throw new IllegalArgumentException("There is no Fields on "+VO.getClass().getName());
	    }
	    else {
	    	selectClause.append(_SELECT);
	    	for (String name:fieldNames) {
	    		if (!selectClause.toString().equals(_SELECT)) selectClause.append(",");	    	
	    		selectClause.append(name);
	    	}
	    }
		return selectClause.toString();
	}
	
	
	private String mountFrom(JwideVO VO,String subQuery) throws Exception {
		StringBuilder fromClause = new StringBuilder("");
		
		fromClause.append(_FROM);	
		if (subQuery!=null){
			fromClause.append(subQuery);
			fromClause.append(" ");
			fromClause.append(this.getRawTableName(VO));
			fromClause.append(" ");
		} else {
			fromClause.append(this.getTableName(VO));
			fromClause.append(" ");
			fromClause.append(this.getRawTableName(VO));
			fromClause.append(" ");
		}
		// tabelas relacionadas via left ou inner join
		int r=0;
		for (JwideVO RelationVO:RelationsVO) {			
			fromClause.append(RelationsType.get(r));
			fromClause.append(this.getTableName(RelationVO));
			fromClause.append(" ");
			fromClause.append(this.getRelationTableNameOrAlias(RelationVO,r));
			fromClause.append(_ON);
			fromClause.append(RelationsSql.get(r));
			fromClause.append("\n");			
			r++;					
		}
		return fromClause.toString();
	}
	
	private String getRelationTableNameOrAlias(JwideVO VO,int position) {
		String relationTableName=RelationsAlias.get(position);
		if (relationTableName.equals("")) relationTableName=this.getRawTableName(VO);
		return relationTableName;
	}
	
	private String mountUpdate(JwideVO VO) throws Exception {
		StringBuilder updateClause = new StringBuilder("");
		
		ArrayList<String> fieldNames = new ArrayList<String>();
		if (fields!=null) {
			for (String fieldName:getFields(fields)) {
				if (fieldName.indexOf("=")>=0) fieldNames.add(fieldName);
				else fieldNames.add(fieldName.trim()+"=:"+fieldName.trim());			    	    		
			}			
		} else {
			for (String fieldName:getFields(VO)) {
				fieldNames.add(fieldName+"=:"+fieldName);			    	    		
			}		
		}
		if (fieldNames.size()==0) {
			throw new IllegalArgumentException("There is no Fields on "+VO.getClass().getName());
		}
	    else {	    	
	    	updateClause.append(_UPDATE);
	    	updateClause.append(this.getTableName(VO));
	    	updateClause.append (" set ");
	    	int f=0;
	    	for (String fieldName:fieldNames) {
	    		if (f>0) updateClause.append(",");
	    		updateClause.append(fieldName);	  
	    		f++;
	    	}
	    }
		
		return updateClause.toString();		
	}
	
	private String mountInsert(JwideVO VO) throws Exception {
		StringBuilder insertClause = new StringBuilder("");
		
		ArrayList<String> fieldNames = new ArrayList<String>();
		if (fields!=null) {
			for (String fieldName:getFields(fields)) {				
				fieldNames.add(fieldName);			    	    		
			}			
		} else {
			for (String fieldName:getFields(VO)) {
				fieldNames.add(fieldName);			    	    		
			}		
		}
		if (fieldNames.size()==0){
			throw new IllegalArgumentException("There is no Fields on "+VO.getClass().getName());
		}
	    else {	    	
	    	insertClause.append(_INSERT);
	    	insertClause.append(this.getTableName(VO));
	    	
	    	insertClause.append (" (");
	    	int f=0;
	    	for (String fieldName:fieldNames) {
	    		if (f>0) insertClause.append(",");
	    		insertClause.append(fieldName);	  
	    		f++;
	    	}
	    	insertClause.append (") ");
	    	
	    	insertClause.append (_VALUES);
	    	insertClause.append (" (");
	    	f=0;
	    	for (String fieldName:fieldNames) {
	    		if (f>0) insertClause.append(",");
	    		insertClause.append(":"+fieldName);	  
	    		f++;
	    	}
	    	insertClause.append (") ");
	    }
		
		return insertClause.toString();		
	}
	
	private String mountDelete(JwideVO VO) throws Exception {
		StringBuilder deleteClause = new StringBuilder("");
		deleteClause.append(_DELETE);
		deleteClause.append(this.getTableName(VO));
		deleteClause.append(" ");
		return deleteClause.toString();		
	}
	
	private String mountWhere(JwideVO VO) throws Exception {
		StringBuilder whereClause = new StringBuilder("");
		
		if (where!=null) {
			if (!where.equals("")) {
				whereClause.append(_WHERE);
				whereClause.append(where);
			}
		}
		else {			
			List<String>Pk=this.getPK(VO);
			if (Pk.size()>0) {
				whereClause.append(_WHERE);
				for (String fieldName:Pk) {
					if (!whereClause.toString().equals(_WHERE)) {
						whereClause.append(_AND);					
					}
					whereClause.append(this.getRawTableName(VO)+"."+fieldName);
					whereClause.append("=:");
					whereClause.append(fieldName);
				}
			}
		}
		return whereClause.toString();		
	}
	
	private String mountGroupBy(JwideVO VO) {
		StringBuilder groupClause = new StringBuilder("");
		if (groupBy!=null) {
			groupClause.append(_GROUP_BY);
			groupClause.append(groupBy);
		}		
		return groupClause.toString();
	}
	
	private String mountHaving(JwideVO VO) {
		StringBuilder havingClause = new StringBuilder("");
		if (having!=null) {
			havingClause.append(_HAVING);
			havingClause.append(having);
		}		
		return havingClause.toString();
	}
	
	private String mountOrderBy(JwideVO VO) {
		StringBuilder orderClause = new StringBuilder("");
		if (orderBy!=null && !orderBy.trim().equals("")) {
			orderClause.append(_ORDER_BY);
			orderClause.append(orderBy);
		}
		
		return orderClause.toString();
	}
	
	private List<String> getFields(String stringFields) {
		List<String> fields = new ArrayList<String>();
		for (String f:JwUtil.lineToArray(stringFields)){
			fields.add(f.trim());
		}
		return fields;
	}
	
	private ArrayList<String> getFields(JwideVO VO) {
		return Reflect.getReadableMethods(VO);
	}
		 
	private String getRawTableName(JwideVO VO) {
		String rawTableName = (
		                       VO.getClass().getCanonicalName().toLowerCase().    // nome em minusculo
                               replaceFirst(VO.getClass().getPackage().getName()+".","")
                              );   // retirar o nome do pacote
		rawTableName=rawTableName.substring(0,rawTableName.length()-JwideConsts.VO_SUFIX.length());
		return rawTableName;
	}
		 
	private String getTableName(JwideVO VO) throws Exception {	
		
		StringBuilder tableName = new StringBuilder();
		if (!propertiesValueObject.getValue("aowner").equals("")){
			tableName.append(propertiesValueObject.getValue("aowner"));
			tableName.append("."); 
		}
		
		tableName.append(this.getRawTableName(VO));
		return tableName.toString();
	}
	
	private List<String> getPK(JwideVO VO) throws Exception {
		StringBuilder pk=new StringBuilder(propertiesValueObject.getValue("primary_key").toString());
		// se n?o encontrar no xml, pegar pela banco de dados
		if (pk.toString().equals("")) {					
			JwideDTO PkDTO=DbObjects.getTablePrimaryKey(DataSource.getConnection(), this.getTableName(VO));
			PkDTO.beforeFirst();
			while (PkDTO.next()) {
				if (!pk.toString().equals("")) pk.append(",");
				pk.append(PkDTO.getString("COLUMN_NAME").toLowerCase());
			}
		}
		return this.getFields(pk.toString());
	}
	
	private void prepareDMLCommands(JwideVO VO) throws Exception {
		this.populatePropertiesValueObject(VO);		
		String connectionString=propertiesValueObject.getValue("connection_string").toString();
		this.createDataSource(!connectionString.equals("")?connectionString:JwUtil.getRawContextPath()); // conex�o n�o indicada
	}
	
	private void clear(){
		fields=null;
		where=null;
		groupBy=null;
		having=null;
		orderBy=null;
		DataSource=null;
		
		sql="";
		    
		RelationsVO.clear();
		RelationsType.clear();
		RelationsSql.clear();
		RelationsAlias.clear();		
		UserParameterValues.clear();
		UserParameterNames.clear();
	}
	
}
