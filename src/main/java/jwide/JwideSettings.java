package jwide; 

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class JwideSettings {
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected HttpSession session;
	
	public JwideSettings(HttpServletRequest  request,
                         HttpServletResponse response,
                         HttpSession         session){
		this.request=request;
		this.response=response;
		this.session=session;
		
		startSettings();
	}
	public JwideSettings(){
		
	}
	private void startSettings() {
		request.setAttribute(JwideConsts.LAST_PAGE,request.getServletPath()); // pegar o nome desta pagina e move para a sessao	
	}
}
