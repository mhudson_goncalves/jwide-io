package jwide;

/**
 * @author MHRG
 * Created On: 20/10/2005 
 */

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRException;
import jwide.JwideDTO;
import jwide.JwideReflection;

public class JwideJasperDataSource implements JRDataSource { 

  /* Exemplo de Execucao:
     Map parameters = new HashMap();
     parameters.put("Titulo", "Relatorio de Planos");

     JwideDTO Psa010 = Psa010Dao.findAll();

     JwideJasperDataSource ds = new JwideJasperDataSource(Psa010);
     JasperPrint jasperPrint = JasperManager.fillReport(jasperReport, parameters, ds);

     JasperManager.printReportToPdfFile(jasperPrint, "planos.pdf");
  */

  private JwideDTO Report;
  private JwideReflection Reflect = JwideReflection.getInstance();

  public JwideJasperDataSource(JwideDTO Report) { 
     this.Report=Report;
     this.Report.beforeFirst();
  } 

  public Object getFieldValue(JRField field) {	
	  return getFieldValue(field.getName(),field.getValueClassName());
	  /*
	 if ((field.getName().length()>03) &&
	     (field.getName().trim().substring(00,03).equalsIgnoreCase("get")) &&
	     (field.getName().trim().substring(03,04).toUpperCase().equals(field.getName().trim().substring(03,04)))) {
		 return Reflect.invokeMethod(Report,field.getName());
	 }
	 if (Report.get(field.getName())==null) return null;
	 
	 String cl=field.getValueClassName();
	 
	 if (cl.equals("java.lang.String"))     return Report.getString(field.getName());
	 if (cl.equals("java.lang.Byte"))       return Report.getByte(field.getName());
	 if (cl.equals("java.util.Date"))       return Report.getDate(field.getName());
	 if (cl.equals("java.sql.Timestamp"))   return Report.getTimestamp(field.getName());
	 if (cl.equals("java.sql.Time"))        return Report.getTime(field.getName());
	 if (cl.equals("java.lang.Double"))     return Report.getDouble(field.getName());
	 if (cl.equals("java.lang.Float"))      return Report.getFloat(field.getName());
	 if (cl.equals("java.lang.Integer"))    return Report.getInteger(field.getName());
	 if (cl.equals("java.lang.Long"))       return Report.getLong(field.getName());
	 if (cl.equals("java.lang.Short"))      return Report.getShort(field.getName());
	 if (cl.equals("java.math.BigDecimal")) return Report.getBigDecimal(field.getName());
	 if (cl.equals("java.lang.Boolean"))    return Report.getBoolean(field.getName());	 
	 
     return Report.get(field.getName());
     */ 
  }
  
  public Object getFieldValue(String fieldName,String className) {
	  if ((fieldName.length()>03) &&
			  (fieldName.trim().substring(00,03).equalsIgnoreCase("get")) &&
			  (fieldName.trim().substring(03,04).toUpperCase().equals(fieldName.trim().substring(03,04)))) {
		  try {
			  return Reflect.invokeMethod(Report,fieldName);
		  } catch (Exception e){
			 e.printStackTrace();
		  }
		  
		  
	  }
	  if (Report.get(fieldName)==null) return null;

	  String cl=className;

	  if (cl.equals("java.lang.String"))     return Report.getString(fieldName);
	  if (cl.equals("java.lang.Byte"))       return Report.getByte(fieldName);
	  if (cl.equals("java.util.Date"))       return Report.getDate(fieldName);
	  if (cl.equals("java.sql.Timestamp"))   return Report.getTimestamp(fieldName);
	  if (cl.equals("java.sql.Time"))        return Report.getTime(fieldName);
	  if (cl.equals("java.lang.Double"))     return Report.getDouble(fieldName);
	  if (cl.equals("java.lang.Float"))      return Report.getFloat(fieldName);
	  if (cl.equals("java.lang.Integer"))    return Report.getInteger(fieldName);
	  if (cl.equals("java.lang.Long"))       return Report.getLong(fieldName);
	  if (cl.equals("java.lang.Short"))      return Report.getShort(fieldName);
	  if (cl.equals("java.math.BigDecimal")) return Report.getBigDecimal(fieldName);
	  if (cl.equals("java.lang.Boolean"))    return Report.getBoolean(fieldName);	 

	  return Report.get(fieldName); 

  }

  public boolean next() throws JRException { 
     return Report.next();
  }
  
}

