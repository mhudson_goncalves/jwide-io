package jwide;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;    
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;  

public class JwideFTP {  	

	public static final int UNKNOW_FILE_TYPE=-1;
	public static final int ASCII_FILE_TYPE=FTPClient.ASCII_FILE_TYPE;
	public static final int BINARY_FILE_TYPE=FTPClient.BINARY_FILE_TYPE;
	
	private static final String SEPARATOR=System.getProperty("file.separator");
	private JwideUtil jUtil = JwideUtil.getInstance();
	private JwideValidator validator = new JwideValidator();	
	
	public boolean sendFiles(JwideFTPVO FtpVO,String[] fileNames) throws Exception {
		return this.sendFiles(FtpVO,null, fileNames);
	}
	
	public boolean sendFiles(JwideFTPVO FtpVO,String rootFolder,String[] fileNames) throws Exception {
		this.validateFileType(FtpVO.getFileType());		
		this.setProxyProperties(FtpVO.getProxyHost(),FtpVO.getProxyPort());
		
		boolean Store=false;
		try{
			FTPClient ftp = new FTPClient();
			this.connect(ftp,FtpVO.getProvider(),FtpVO.getUser(),FtpVO.getPassword());			
			ftp.sendNoOp();// Evitar o timeout
			int reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)){
				ftp.disconnect();
				return false;
			}

			for (String fileName:fileNames){
				ftp.enterLocalPassiveMode();
				InputStream in = new FileInputStream(fileName);	              
				this.setFileType(ftp,FtpVO.getFileType(),fileName);
				Store = ftp.storeFile(this.formatFileName(rootFolder,fileName), in);
				in.close();
			}
			ftp.logout();
		} catch (Exception ex){
			throw new IllegalArgumentException("On FTP.sendFiles: "+ ex.getMessage());			
		}
		
		return Store;		
	}
	
	public FTPFile[] listFiles(JwideFTPVO FtpVO, String folder) throws Exception {		
		FTPClient ftp = new FTPClient();  
		this.connect(ftp, FtpVO.getProvider(), FtpVO.getUser(), FtpVO.getPassword());
		
		if (folder!=null){
			ftp.enterLocalPassiveMode();
			ftp.changeWorkingDirectory(folder);
		}
				
		FTPFile[] files = ftp.listFiles();
		ftp.disconnect();	
		return files;		
	}
	
	public boolean retrieveFile(JwideFTPVO FtpVO, String ftpFileName, String targetFile) throws Exception {

		FTPClient ftp = new FTPClient();

		this.connect(ftp, FtpVO.getProvider(), FtpVO.getUser(), FtpVO.getPassword());	
		this.validateFileType(FtpVO.getFileType());	
		ftp.setFileType(FtpVO.getFileType());
		ftp.enterLocalPassiveMode();
		ftp.setAutodetectUTF8(true);
		
		InputStream inputStream = ftp.retrieveFileStream(ftpFileName);
		FileOutputStream fileOutputStream = new FileOutputStream(targetFile);		
		IOUtils.copy(inputStream, fileOutputStream);
		fileOutputStream.flush();
		IOUtils.closeQuietly(fileOutputStream);
		IOUtils.closeQuietly(inputStream);
		boolean ok = ftp.completePendingCommand();
		
		return ok;
	} 
	
	// este m�tido tb move arquivos de uma pasta para outra, basta renomear o path
	public boolean renameFile(JwideFTPVO FtpVO, String fromFile, String toFile) throws Exception {

		this.validateFileType(FtpVO.getFileType());		
		this.setProxyProperties(FtpVO.getProxyHost(),FtpVO.getProxyPort());

		FTPClient ftp = new FTPClient();  
		
		File f =new File(fromFile);
		f.getName();

		this.connect(ftp,FtpVO.getProvider(),FtpVO.getUser(),FtpVO.getPassword());	
		boolean ok = ftp.rename(fromFile, toFile);
		ftp.disconnect();
		
		return ok;
	} 
	
	public void moveFiles(JwideFTPVO FtpVO, String[] files, String folder) throws Exception {

		this.validateFileType(FtpVO.getFileType());		
		this.setProxyProperties(FtpVO.getProxyHost(),FtpVO.getProxyPort());

		FTPClient ftp = new FTPClient();  
		this.connect(ftp,FtpVO.getProvider(),FtpVO.getUser(),FtpVO.getPassword());	
		
		for (String file:files){
			ArrayList<String> f = jUtil.lineToArray(file.replace("\\", "/"), "/");
			String fileTo = (folder.endsWith("/")? folder + f.get(f.size()-1): folder + "/" + f.get(f.size()-1));
			boolean ok = ftp.rename(file, fileTo);
			if (!ok){
				validator.addError("JwideFTP.moveFiles: Cannot move file '" + file +"' to '" + folder +"'");
			}
		}		
		ftp.disconnect();
		validator.flush();		
	} 
	
	private void validateFileType(int fileType) throws Exception {
		if (fileType!=UNKNOW_FILE_TYPE && fileType!=FTPClient.ASCII_FILE_TYPE && fileType!=FTPClient.BINARY_FILE_TYPE) {
			throw new IllegalAccessException("JwideFTP: Invalid fileType ! Use UNKNOW_FILE_TYPE, ASCII_FILE_TYPE or BINARY_FILE_TYPE");
		}			
	}
	
	private void setProxyProperties(String proxyHost,int proxyPort){
		if (proxyHost!=null && !proxyHost.equals("")){
			System.getProperties().put("socksProxyHost",proxyHost);
			System.getProperties().put("socksProxyPort",proxyPort+"");
		}
	}
	
	private void connect(FTPClient ftp,String provider,String user,String password) throws Exception {
		ftp.connect(provider);
		if (FTPReply.isPositiveCompletion(ftp.getReplyCode()) ) {  
			ftp.login(user,password);  
		} else {  
			//erro ao se conectar  
			ftp.disconnect();  
			throw new IllegalAccessException("JwideFTP: Connection Refused.");                 
		}  		
	}
	
	private void setFileType(FTPClient ftp,int fileType,String fileName) throws Exception {
		if (fileType!=UNKNOW_FILE_TYPE){
			ftp.setFileType(fileType);
		} else if(fileName.endsWith(".txt") ) {  
			ftp.setFileType(FTPClient.ASCII_FILE_TYPE);  
		} else if(fileName.endsWith(".jpg") ) {  
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);  
		} else {  
			ftp.setFileType(FTPClient.ASCII_FILE_TYPE);  
		}  		
	}
	
	private String formatFileName(String rootFolder,String fileName){
		String newName=new String(fileName);
		while (newName.indexOf(SEPARATOR)>=0){
			newName=newName.substring(newName.indexOf(SEPARATOR)+1,newName.length());		
		}
		while (newName.indexOf("/")>=0){
			newName=newName.substring(newName.indexOf("/")+1,newName.length());		
		}
		
		if (rootFolder!=null && !rootFolder.trim().equals("")){
			if (!rootFolder.endsWith("\\") && !rootFolder.endsWith("/")){
				rootFolder = rootFolder + "/";
			}
			return rootFolder + newName;
		}
		
		return newName;
	}
	
}  