/*
 * Created on 26/08/2004
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

package jwide;

/**
 * @author marcos Hudson
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.sql.Date;
import java.sql.ResultSet;
import java.lang.reflect.Field;
import java.sql.CallableStatement;
import java.sql.Timestamp;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jwide.annotations.Bean;
import jwide.annotations.Validate;
import jwide.annotations.Validate.Type;

public class JwideProcedure {	

	private CallableStatement cs=null;	
	private String sql = null;
	private ResultSet rs=null;	
	private Connection con;
	private static Pattern parameter = Pattern.compile(":(\\w+)");
	private List<String> parameterNames = new ArrayList<String>();	
	private JavaTypesDTO javaTypes = new JavaTypesDTO();	
	private JwideDataSource dataSource = null;
	
	private JwideValidator validator = new JwideValidator();
	
	public JwideProcedure() {		
	}	
	
	protected void setResultSet(ResultSet rs) {
		this.rs=rs;
	}
	
	public ResultSet getResultSet() {
		return rs;
	}		
	
	public void setSql(String sql) {
		this.sql = sql;		
	}
	
	public void execute(JwideVO value) throws Exception {	
		validator.flush(value);
		
	    this.releaseDataSource();
		this.setConnection(this.getConnection(value)); 
		this.setSql(this.mountSql(value, this.getConnection()));
		this.prepareCall();
		
		for (Field field : value.getClass().getDeclaredFields()) {			
		
			if (field.isAnnotationPresent(Validate.class)) {
				field.setAccessible(true);	
				Type type = field.getAnnotation(Validate.class).type();
				
				if (type==Type.IN){
					this.setParameterValue(field.getName(), field.get(value));
				} else if (type==Type.OUT){
					this.registerOutParameter(field.getName(), javaTypes.getJavaType(field.getType()));
				} else if (type==Type.INOUT){
					this.setParameterValue(field.getName(), field.get(value));				
					this.registerOutParameter(field.getName(), javaTypes.getJavaType(field.getType()));
				}
			}
		}
		
		this.execute();
		
		for (Field field : value.getClass().getDeclaredFields()) {			
			
			if (field.isAnnotationPresent(Validate.class)) {
				field.setAccessible(true);	
				Type type = field.getAnnotation(Validate.class).type();
				
				if (type==Type.OUT || type==Type.INOUT){
				    		
					if (JwideRefCursorDTO.class.isAssignableFrom(field.getType())){ // cursor oracle
						ResultSet Rs = (ResultSet) this.getParameterObject(field.getName());					
						JwideRefCursorDTO cursorDTO = new JwideRefCursorDTO();
						cursorDTO.populate(Rs);
						Rs.close();	
						field.set(value, cursorDTO);
					} else {
						field.set(value, this.getParameterObject(field.getName()));
					}
				}
			}
		}
		
		this.closeStatement();
	}
	
	private Connection getConnection(JwideVO value) throws Exception {
		Bean bean =(Bean) value.getClass().getAnnotation(Bean.class);
		return this.createDataSource(bean.connection()).getConnection();
	}
	
	private String mountSql(JwideVO value, Connection Con){
	
	    StringBuilder columns = new StringBuilder();
		for (Field field : value.getClass().getDeclaredFields()) {			
			
			if (field.isAnnotationPresent(Validate.class)) {
			   if (columns.length()>0){
				   columns.append(", ");
			   }
			   columns.append(":" + field.getName());
			}
		}
		
		StringBuilder Sql = new StringBuilder();			

		Sql.append("{ call ");
		Sql.append(this.getAower(value));
		Sql.append(this.getPackageName(value));
		Sql.append(this.getProcName(value));
		Sql.append("(" + columns.toString() + ")");
		Sql.append(" }");

		return Sql.toString();	
	}
	
	private String getProcName(JwideVO value) {
		String rawTableName = (
				value.getClass().getCanonicalName().toLowerCase().
                               replaceFirst(value.getClass().getPackage().getName()+".","")
                              );   // retirar o nome do pacote
		rawTableName=rawTableName.substring(0,rawTableName.length()-JwideConsts.VO_SUFIX.length());
		return rawTableName;
	}
	
	private String getAower(JwideVO value){
		String aowner = ((Bean) value.getClass().getAnnotation(Bean.class)).aowner();
		return ((aowner!=null && !aowner.trim().equals(""))?aowner.trim()+".":"");
	}
	
	private String getPackageName(JwideVO value){
		String packageName = ((Bean) value.getClass().getAnnotation(Bean.class)).packageName();
		return ((packageName!=null && !packageName.trim().equals(""))?packageName.trim()+".":"");
	}
	
	public void prepareCall() throws SQLException {		
		String sqlJdbc=parseSQL(sql);
		cs = getConnection().prepareCall(sqlJdbc);
	}
	
	public void executeQuery() throws SQLException {
		rs=cs.executeQuery();
		setResultSet(rs);  			
	}
    
	public void executeUpdate() throws SQLException {    	
		cs.executeUpdate();  			
	}   
    
	public void execute() throws SQLException {
		cs.execute();  		
	}  
    
	public void closeStatement() throws SQLException {
		cs.close();
	}	
	
	private String parseSQL(String sql) {
		if ((sql.indexOf("?")>=0) && (sql.indexOf(":")>=0)) {			
			throw new IllegalArgumentException("JwideProcedure: Cannot Handle '?' and ':' parameters together");
		}	

		Matcher m = parameter.matcher(sql);
		StringBuffer sb = new StringBuffer();
		parameterNames = new ArrayList<String>();

		while (m.find()) {
			parameterNames.add(m.group(1));
			m.appendReplacement(sb, "?");
		}
		m.appendTail(sb);	   

		return sb.toString();
	}	

    //  --- inicio dos sets dos parametros
    
    public void setParameterValue(int parameterIndex, Object parameterValue) throws SQLException {
    	if (parameterValue!=null) cs.setObject(parameterIndex,parameterValue);
    	else                      cs.setObject(parameterIndex, null, java.sql.Types.VARCHAR); 
    	//O Sql Server N?o Suporta: stm.setNull(parameterIndex,parameterIndex);        
    }   
    
    // parametros indexados por nome    
    public void setParameterValue(String parameterName,Object parameterValue) throws SQLException {    	 
    	for (int indice:getParameterIndexes(parameterName)) setParameterValue(indice,parameterValue);    	
    }	
    
    public Object getParameterObject(int parameterIndex) throws SQLException {    	
    	return cs.getObject(parameterIndex);
    }
    
    public Integer getParameterInteger(int parameterIndex) throws SQLException {    	
    	return cs.getInt(parameterIndex);
    }
    public String getParameterString(int parameterIndex) throws SQLException {    	
    	return cs.getString(parameterIndex);
    }
    
    public Double getParameterDouble(int parameterIndex) throws SQLException {    	
    	return cs.getDouble(parameterIndex);
    }
    
    public Boolean getParameterBoolean(int parameterIndex) throws SQLException {    	
    	return cs.getBoolean(parameterIndex);
    }
    
    public Date getParameterDate(int parameterIndex) throws SQLException {    	
    	return cs.getDate(parameterIndex);
    }
    
    public Timestamp getParameterTimestamp(int parameterIndex) throws SQLException {
    	Date dt=getParameterDate(parameterIndex);
    	if (dt!=null) return new Timestamp(dt.getTime()); 
    	else return null;	
    }
    
    public Object getParameterObject(String parameterName) throws SQLException {    	 
   		return getParameterObject(getParameterIndexes(parameterName).get(0).intValue());   	
    }
    public Integer getParameterInteger(String parameterName) throws SQLException {    	 
   		return getParameterInteger(getParameterIndexes(parameterName).get(0).intValue());   	
    }
    public String getParameterString(String parameterName) throws SQLException {    	
    	return getParameterString(getParameterIndexes(parameterName).get(0).intValue());   
    }
    
    public Double getParameterDouble(String parameterName) throws SQLException {    	
    	return getParameterDouble(getParameterIndexes(parameterName).get(0).intValue());   
    }
    
    public Boolean getParameterBoolean(String parameterName) throws SQLException {    	
    	return getParameterBoolean(getParameterIndexes(parameterName).get(0).intValue());   
    }
    
    public Date getParameterDate(String parameterName) throws SQLException {    	
    	return getParameterDate(getParameterIndexes(parameterName).get(0).intValue()); 
    }
    
    public Timestamp getParameterTimestamp(String parameterName) throws SQLException {
    	return getParameterTimestamp(getParameterIndexes(parameterName).get(0).intValue()); 	
    }
    
    public void registerOutParameter(int parameterIndex,int type) throws SQLException {
    	cs.registerOutParameter(parameterIndex,type);
    }    
    public void registerOutParameter(String parameterName,int type) throws SQLException {    	
    	for (int indice:getParameterIndexes(parameterName)) registerOutParameter(indice,type);
    }      
    
    public void setConnection(Connection con) {
		this.con=con;
	}
	
	public Connection getConnection() {
		return con;
	}
	
	private JwideDataSource createDataSource(String connectionString) throws Exception  {
		if (dataSource==null){
			dataSource = new JwideDataSource(connectionString);
		}
		return dataSource;
	}
	
	private void releaseDataSource() {
		dataSource = null;
	}
	
	private ArrayList<Integer> getParameterIndexes(String parameterName) {
		ArrayList<Integer> parameterIndex = new ArrayList<Integer>();
		for (int i=0;i<parameterNames.size();i++) {
			if (parameterNames.get(i).toString().equalsIgnoreCase(parameterName)) {
				parameterIndex.add(i+1);    			    			
			}
		}    	
		if (parameterIndex.size()==0) {
			throw new IllegalArgumentException("JwideProcedure: Invalid Parameter Name "+parameterName);
		}
		return parameterIndex;
	}	

}
