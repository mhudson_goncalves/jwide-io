package jwide;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jwide.JwideRandom;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.SimpleBookmark;

public class JwideJasperConcatenateReport {
    
	List<byte[]> reports = new ArrayList<byte[]>();
	
	private static String FILE_PREFIX="concat";
	private static String FILE_EXTENSION=".pdf";	
	
	JwideRandom random = new JwideRandom();
	
	public void addReport(byte[] report){	
		if (report!=null) reports.add(report);
	}
	
	public void setReports(List<byte[]> reports){
		this.reports=reports;
	}
	
	public byte[] concatenateReports() throws Exception {
		return this.joinFiles(reports);
	}
	boolean reportsAdded=false;
	
	private byte[] joinFiles(List<byte[]> reportContents) throws Exception {
		int pageOffset = 0;
		
		List<HashMap<String, Object>> master = new ArrayList<HashMap<String, Object>>();
		int f = 0;

		String masterFileName=FILE_PREFIX+random.range(Integer.MAX_VALUE);
		File outFile = File.createTempFile(masterFileName,FILE_EXTENSION);
		
		Document document = null;
		PdfCopy  writer = null;
		for (byte[] reportContent:reportContents) {
			reportsAdded=true;
			// we create a reader for a certain document
			//PdfReader reader = new PdfReader(fileName);
			PdfReader reader = new PdfReader(reportContent);
			reader.consolidateNamedDestinations();
			// we retrieve the total number of pages
			int n = reader.getNumberOfPages();
			
			List<HashMap<String, Object>> bookmarks = SimpleBookmark.getBookmark(reader);
			if (bookmarks != null) {
				if (pageOffset != 0)
					SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
				master.addAll(bookmarks);
			}
			pageOffset += n;

			if (f == 0) {
				// step 1: creation of a document-object
				document = new Document(reader.getPageSizeWithRotation(1));
				// step 2: we create a writer that listens to the document
				writer = new PdfCopy(document, new FileOutputStream(outFile));
				// step 3: we open the document
				document.open();
			}
			// step 4: we add content
			PdfImportedPage page;
			for (int i = 0; i < n; ) {
				++i;
				page = writer.getImportedPage(reader,i);
				writer.addPage(page);
			}
			PRAcroForm form = reader.getAcroForm();
			if (form != null)
				writer.copyDocumentFields(reader);
			f++;
		}
		
		// document is empty
		if (!reportsAdded) {
			outFile.delete();
			return new byte[]{};
		}
		
		if (!master.isEmpty())
			writer.setOutlines(master);
		// step 5: we close the document
		document.close();
		
		FileInputStream inputStream = new FileInputStream(outFile);
	    byte fileContent[]=new byte[(int)outFile.length()];	
	  	  
	    int i=-1;		
		while ((i=inputStream.read(fileContent))!=-1){
			inputStream.read(fileContent,0,i);
		}  
		inputStream.close();	
		
		outFile.delete();
		
        return fileContent;		
	}		

}
