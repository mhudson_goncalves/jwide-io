package jwide;

import java.sql.Connection;

public class JwideDataSource {
	
	private String name=null;	

	private static ThreadLocal<JwideLookupResources> resourceInstance = new ThreadLocal<JwideLookupResources>() {

		@Override
		protected JwideLookupResources initialValue() {
			return new JwideLookupResources();
		}

	};
	
	public JwideDataSource(String name) {
		this.name = name;
	}	

	public Connection getConnection() throws Exception {
		return resourceInstance.get().getConnection(this.name);
	}		

	public String getName()  {
		return name;
	}
	
	public static void commit(){
		resourceInstance.get().commit();	
	}
	
	public static void rollback(){
		resourceInstance.get().rollback();	
	}
	
	public static void close(){
		resourceInstance.get().close();	
	}

}
