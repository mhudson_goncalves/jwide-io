package jwide;

import java.util.ArrayList;
import java.util.List;

public class JwideMailVO {
	
	private String smtpHost;
	private String smtpPort;
	private String userName;
	private String password;
	private String to;
    private String cc;
    private String bcc;
    private String from;
    private String subject="";
    private String text;
    private String mimeType=JwideMail.TEXT_PLAIN_MIME;
    private boolean needAuthentication=true;
    private boolean startTls=false;
    private List<String> attachedFile = new ArrayList<String>();
    
    //smtp.sao.terra.com.br
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public boolean isNeedAuthentication() {
		return needAuthentication;
	}
	public void setNeedAuthentication(boolean needAuthentication) {
		this.needAuthentication = needAuthentication;
	}
	
	public boolean isStartTls(){
		return startTls;
	}
	
	public void setStartTls(boolean startTls){
		this.startTls=startTls;
	}
	
	public void setMimeType(String mimeType) {
		this.mimeType=mimeType;
	}
	public String getMimeType() {
		return this.mimeType;
	}	
	public void addAttachedFile(String url){
       if (url!=null) attachedFile.add(url);		
	}
	public List<String> getAttachedFiles(){
		return attachedFile;
	}
	
}
