package jwide;

import java.io.FileInputStream;
import java.util.Properties;

public class JwideProperties {

	private static final String PROPERTIES_FILE = "jwide.properties";	
	private String folder = null;
	private Properties properties = null;
	
	public JwideProperties(String folder) {
		this.folder = folder;
	}	
	
	public String getProperty(String name) throws Exception {
		if (properties==null) {
			properties = new Properties();		
			properties.load(new FileInputStream(this.formatFileName()));		
		}
		
		return properties.getProperty(name);
	}
	
	
	private String formatFileName() throws Exception {
		if (!folder.endsWith("/") && !folder.endsWith("\\")){
			return folder + "/" + PROPERTIES_FILE;
 		} else {
 			return folder + PROPERTIES_FILE;
 		}
	}

}
