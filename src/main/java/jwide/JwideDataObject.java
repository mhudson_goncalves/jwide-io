package jwide;

import java.util.ArrayList;
import java.util.List;

public class JwideDataObject {

	private List<String> columns;
	private List<Object> values;

	public JwideDataObject(List<String> columns,List<Object> values) {
		this.columns=columns;
		this.values=values;
	}
	
	public JwideDataObject() {
		this.columns = new ArrayList<>();
		this.values = new ArrayList<>();
	}
	
	public void addColumn(String column){
		this.columns.add(column);
	}
	
	public void addValue(Object value){
		this.values.add(value);
	}
	
	public void addValue(String column, Object value){
		this.addColumn(column);
		this.addValue(value);
	}
	
	public List<String> getColumns(){
		return columns;
	}
	
	public List<Object> getValues(){
		return values;
	}
	
	public Object getValue(String name) throws Exception {
		if (!columns.contains(name)){
			throw new IllegalArgumentException("JwideDataObject.getValue - Invalid column name '" + (name!=null?name:"") +"'");
		}
		return values.get(columns.indexOf(name));
	}

}
