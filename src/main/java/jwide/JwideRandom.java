package jwide;

import java.util.Date;

final public class JwideRandom {
	private long seed;
	/*
	 *  JwideRandom Rnd = new JwideRandom();
        int Key=Rnd.range(999999999);        
	 */
	
	public JwideRandom() {
		Date date = new Date();
		seed = date.getTime();
	}
	
	public JwideRandom(long s) {
		seed = s;
	}
	
	public JwideRandom(JwideRandom r) {
		seed = r.seed;
	}
	
	public long seed() {
		return seed;
	}
	
	public boolean flip() {
		random();
		return (seed & 1) == 0;
	}
	
	public int range(int range) {
		int	number;
		
		random();
		number = (int) ((seed / 65535) % range);
		return number < 0 ? -number : number;
	}
	
	public long range(long range) {
		long	number;
		
		random();
		number = (int) ((seed / 65535) % range);
		return number < 0 ? -number : number;
	}
	
	void random() {
		seed = seed * 1103515245 + 12345;
	}
}
