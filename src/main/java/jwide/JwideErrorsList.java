/*
 * Created on 07/03/2005
 */
package jwide;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * @author MHRG 
 */

public class JwideErrorsList {
	
	private JwideErrors je=null;	
	private ArrayList<?> err;
	private Properties userMessages=null;
	private Properties jwideMessages=null;	
	private JwideUtil jUtil = JwideUtil.getInstance();
	
	public JwideErrorsList() {
	}
	
	public void setErrors(JwideErrors errors) {
		this.je=errors;
	}
	
	public JwideErrors getErrors() {
		return je;
	}
	
	public String getLastErrorMessage(JwideErrors errors){		
		 err=(ArrayList<?>) errors.getErrorList();
		 ArrayList<?> error = (ArrayList<?>) err.get(err.size()-1);		  		     
	     return this.getErrorRow(error).get(1).toString();
	}
	
	public JwideDTO getErrorsList() {
		JwideDTO ErrList = new JwideDTO();
		ErrList.setColumns(new String[]{"id","mensagem"});
		err=(ArrayList<?>) je.getErrorList();
		
		// --- < trazer os erros >---\\
		for (int i=0;i<err.size();i++) {
		     ArrayList<?> error = (ArrayList<?>) err.get(i);		  		     
		     ArrayList<Object> row = this.getErrorRow(error);		    
		     ErrList.addRow(row);
		}		
		ErrList.beforeFirst();
		// ---<fim>--- \\
		
		return ErrList;
	}	
	
	private ArrayList<Object> getErrorRow(List<?> error) {
		String id=error.get(0).toString(); 
	     String[] param = (String[]) error.get(1);
	     String msg=null;
	     
	     msg=this.getErrorMessage(id);  
	     
	     if ((msg==null) || (msg.equals(""))) msg="Unknow Error "+id;
	     
	     // substitiuir os parametros da mensagem
	     for (int e=0;e<param.length;e++) {
	    	 try {
	    		 msg=msg.replace("%"+(e+1),param[e]);
	    	 } catch (Exception error_) {
	    		 msg=msg.replace("%"+(e+1),"Unknow "+(e+1)+" On "+msg);
	    	 }
	     }		     
	     
	     ArrayList<Object> row = new ArrayList<Object>();
	     row.add(id);
	     row.add(msg);	     
	    
	    return row;
	}
	
	private String getErrorMessage(String key) {		
	   String m=null;
	   //C:/tomcat/webapps/lite/WEB-INF/classes/jwide_properties/
	  	   
	   // carregar arq de mensagens do usuario
	   if (userMessages==null) {
		   userMessages = new Properties();
		   try { 			 
			   InputStream in = this.openInputStream(jUtil.getJwideResourcePath("user-messages.properties"));
			   userMessages.load(in);
			   in.close();
		   } catch (Exception e) {
			  e.printStackTrace();
		   }		   
	   }
	   
	   // carregar arq de mensagens do jwide	   
	   if (jwideMessages==null) {
		   jwideMessages = new Properties();
		   try {                 	       
              InputStream in = this.openInputStream(jUtil.getJwideResourcePath("jwide-messages.properties"));      
              jwideMessages.load(in);	       
              in.close();
		   } catch (Exception e) {
			   e.printStackTrace();
		   }
	   }
	   
	   try {           
           // verificar de qual arquivo deve trazer a mensagem
           if (key.length()>06) {
              if (key.substring(0,6).equals("error.")) {
            	  m=jwideMessages.getProperty(key);              
              } else {
            	  m=userMessages.getProperty(key);        
              }
           }
           
        } catch (Exception e) {
            e.printStackTrace();            
        }        
	   
	    return m;
	}
	
	private InputStream openInputStream(String name) throws Exception {
		return new FileInputStream(new File(name));	
	}
}
