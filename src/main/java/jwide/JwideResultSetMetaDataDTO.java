package jwide;
import java.sql.Types;

public class JwideResultSetMetaDataDTO extends JwideDTO {
	//aa
	public JwideResultSetMetaDataDTO() {		
	}
	 
	
	public Boolean isColumnNumber(String columnName) {
		if (!this.hasFieldName("column_type_number")) return false;
		
		if (this.getInteger("column_type_number").equals(Types.BIGINT) ||
		    this.getInteger("column_type_number").equals(Types.DECIMAL) ||
		    this.getInteger("column_type_number").equals(Types.DOUBLE) ||
		    this.getInteger("column_type_number").equals(Types.FLOAT) ||
		    this.getInteger("column_type_number").equals(Types.INTEGER) ||
		    this.getInteger("column_type_number").equals(Types.NUMERIC) ||
		    this.getInteger("column_type_number").equals(Types.REAL) ||
		    this.getInteger("column_type_number").equals(Types.SMALLINT)) return true;
		
		return false;
	}
	

}
