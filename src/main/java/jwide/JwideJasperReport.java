package jwide;

import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

import java.util.Map;
import java.util.HashMap;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

public class JwideJasperReport {
	 
	 private Map<String,Object> parameters = new HashMap<String,Object>();
	 	  
	 // response.setContentType(?application/pdf?);
	 // response.addHeader(
	 //   ?Content-Disposition?,
	 //   ?attachment; filename=PO - ? + po.getReference() + ?.pdf?);
	
	public JwideJasperReport() {		
	}
	
	public void addParameter(String parameterName,Object value) {
		parameters.put(parameterName,value);
	}
	
	public byte[] exportReportToPdf(String reportFileName,JwideDTO dataSource) throws Exception {
		// "com/mycomp/po/pof.jasper"
		JwideJasperDataSource DataSource = new JwideJasperDataSource(dataSource);
       // File reportFile = new File(reportFileName);      
		
		//return JasperRunManager.runReportToPdf(reportFileName,				
          //                                     parameters,
            //                                   DataSource);
		JasperPrint Report=JasperFillManager.fillReport(reportFileName,				
                parameters,
                DataSource);

      return JasperExportManager.exportReportToPdf(Report);
	}
	
	public byte[] exportReportToPdf(String reportFileName,Connection con) throws Exception {
		return JasperRunManager.runReportToPdf(reportFileName,				
                                               parameters,
                                               con);
		
	}
	
	public void exportReportToPdf(String reportFileName,JwideDTO dataSource,String pdfFileName) throws Exception {
		JwideJasperDataSource DataSource = new JwideJasperDataSource(dataSource);
		OutputStream outFile = (new FileOutputStream(pdfFileName));
		JasperRunManager.runReportToPdfStream(getClass().getClassLoader().getResourceAsStream(reportFileName),
		                                      outFile,
                                              parameters,
                                              DataSource);
	}
	
	public void  printDirectlyToPrinter(String printerName,String reportFileName,JwideDTO dataSource)  throws Exception    {     

		//Lista todas impressoras at? encontrar a enviada por parametro  
		PrintService serviceFound = null;  
		PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);  
		for(PrintService service:services){  
			if(service.getName().trim().equals(printerName.trim()))  
				serviceFound = service;  
		}  

		JwideJasperDataSource DataSource = new JwideJasperDataSource(dataSource);

		JasperPrint Report=JasperFillManager.fillReport(reportFileName,				
				parameters,
				DataSource);           

		if (serviceFound == null)  
			throw new Exception("JwideJasperReport: Impressora n?o encontrada !");  


		PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();

		SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();	           
		configuration.setPrintService(serviceFound);
		configuration.setPrintRequestAttributeSet(printRequestAttributeSet);	         
		configuration.setDisplayPageDialog(false);
		configuration.setDisplayPrintDialog(false);

		JRPrintServiceExporter exporter = new JRPrintServiceExporter();
		exporter.setConfiguration(configuration);
		exporter.setExporterInput(new SimpleExporterInput(Report));
		exporter.exportReport();
	}

}
