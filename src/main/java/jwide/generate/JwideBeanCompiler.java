package jwide.generate;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;

import jwide.JwideDTO;
import jwide.JwideDigester;
import jwide.JwideFileHandler;
import jwide.JwideUtil;
import jwide.JwideVO;

public class JwideBeanCompiler {
	
	private static JwideFileHandler fh = new JwideFileHandler();
	private List<String> imports=null;
				
	private static char PATH_SEPARATOR = File.pathSeparatorChar;
	private static String PACKAGE_DEFAULT="beans";
	private static String JWIDE_PATH=JwideUtil.getInstance().getResourcePath("/").replace("/META-INF/","/WEB-INF/lib/jwide.jar");
	private static String CLASS_PATH = JWIDE_PATH + PATH_SEPARATOR + JwideUtil.getInstance().getResourcePath("/").replace("/META-INF/","/WEB-INF/classes");
	private static String ROOT_PATH = JwideUtil.getInstance().getResourcePath("/").replace("/META-INF/", "/");
	private static String EXPR_CLASS_PATH = JwideUtil.getInstance().getResourcePath("/").replace("/META-INF/","/WEB-INF/classes/")+PACKAGE_DEFAULT;
	private List<String> opts = Arrays.asList("-classpath",CLASS_PATH,"-d", ROOT_PATH);
	
	private static final String QUOTE = new String("'");
	private static final String BLANK = new String("?"); // NAO MEXER NESTA LINHA, ESTE ESPACO EM BRANCO E O CODIGO (160) ---> " &NBSP; " E NAO UM ESPACO COMUM CODIGO(32)                              
	private static final char[] INVALIDS={'?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?',QUOTE.charAt(0),'?','?','?',BLANK.charAt(0),'?','?','?','"','`','?','?','?','?','?'};                                
	private static final char[] VALIDS = {'A','A','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','U','U','U','U','Y','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','o','n','o','o','o','o','o','u','u','u','u','y','y',' ',' '            ,' ','.',' ',' '            ,' ',' ',' ',' ',' ',' ','1','2','3','i'};
	   
    private JwideGenerateUtils gUtil = JwideGenerateUtils.getInstance();
    
    public JwideBeanCompiler(JwideDTO expr) throws Exception {		
		this.compile(null,expr);
	}
      	
	public JwideBeanCompiler(List<String> imports, JwideDTO expr) throws Exception {		
		this.compile(imports,expr);
	}	
	
	private void compile(List<String> imports , JwideDTO dto) throws Exception{
		this.validateExpr(dto);		
		this.imports=imports;	
		
		// o nome do arq ? criado com o hash da express?o, se n?o houver altera??o desde a ?ltima compila??o, carregar a classe j? compilada
		if (!this.isClassExists(dto)){
			String source=this.mountSource(this.createSourceName(dto),dto);
			this.compile(this.createSourceName(dto),dto,new StringJavaFileObject(this.createSourceName(dto),source));
		} 
	}
	
	private void validateExpr(JwideDTO dto) throws Exception {
		if (dto==null) this.raiseError("Data Transfer Object is null");
		if (dto.getColumns().length==0) this.raiseError("Data Transfer Has No Columns");
		
		List<String> cols = new ArrayList<String>();
		StringBuffer duplicate = new StringBuffer();
		for (String c:dto.getColumns()){
			String col=this.fmtColName(c);
		    if (cols.indexOf(col)>=0){
		    	if (duplicate.length()>0) duplicate.append(", ");
		    	duplicate.append(col);
		    } else {
		    	cols.add(col);
		    }			
		}
		if (duplicate.length()>0) {
			this.raiseError("Duplicate Column Name: "+duplicate.toString());
		}
	}
	
	public JwideVO instance(JwideDTO dto) throws Exception {
		return this.createClassInstance(dto);		
	}
	
	protected String mountSource(String className, JwideDTO dto) {
		StringBuffer sb = new StringBuffer(""); 
		
		sb.append("package "+PACKAGE_DEFAULT+";");
		sb.append("\n");
		sb.append("\n");
		
		sb.append("import jwide.JwideVO;");
		sb.append("\n");	
		sb.append("import java.io.Serializable;");
		sb.append("\n");
		
		// user imports
		if (imports!=null) for (String i:imports){
			sb.append("import "+i+";");
			sb.append("\n");
		}
		
		sb.append("public class "+className+" extends JwideVO implements Serializable {");
		sb.append("\n");
		
		// Var declarations
		for (String c:dto.getColumns()){
			sb.append("protected ");
			sb.append(this.getColType(dto, c));			
			sb.append(" ");
			sb.append(this.fmtColName(c));
			sb.append(";");
			sb.append("\n");
		}			
		
		sb.append("public "+className+"(){");
		sb.append("\n");	
		sb.append("}");
		sb.append("\n");	
		
		// getters and setters
		// get
		for (String c:dto.getColumns()){
			sb.append("public ");
			sb.append(this.getColType(dto, c));			
			sb.append(" get");
			sb.append(this.fmtColName(c));
			sb.append("(){");
			sb.append("\n");
			sb.append("   return "+this.fmtColName(c));			
			sb.append(";");
			sb.append("\n");
			sb.append("}");
			sb.append("\n");			
		}
		// set
		for (String c:dto.getColumns()){
			sb.append("public void set");
			sb.append(this.fmtColName(c));
			sb.append("(");
			
			sb.append(this.getColType(dto, c));
			sb.append(" ");
			sb.append(this.fmtColName(c));			
			sb.append("){");
			sb.append("\n");
			sb.append("this."+this.fmtColName(c));			
			sb.append("="+this.fmtColName(c));
			sb.append(";");
			sb.append("\n");
			sb.append("}");
			sb.append("\n");			
		}
				
		sb.append("}");
		sb.append("\n");
		return sb.toString();
	}
	
	private void compile(String className,JwideDTO dto,JavaFileObject... source) throws Exception {
				
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
				
		Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(source);
			
		CompilerDiagnosticListener listener = new CompilerDiagnosticListener();				
		boolean compilerResult = compiler.getTask( null, null,listener, opts, null, compilationUnits).call();		
		if (!compilerResult){
			this.raiseError("Compiler Error: "+listener.getLog());
		}
	}
	
	private JwideVO createClassInstance(JwideDTO dto) throws Exception {	
		JwideVO instance=null;
		try {
			Class<?> clazz=Class.forName(PACKAGE_DEFAULT+"."+this.createSourceName(dto)); 
			instance=(JwideVO)clazz.getConstructor().newInstance();			
		} catch (Exception e){
			this.raiseError(" on create class instance "+e.getMessage());
		}
		return instance;
	}

	private String createSourceName(JwideDTO dto) {
		String sourceName=null;
		try {
			sourceName="Bean"+JwideDigester.digestMD5(this.extractBeanDef(dto));
		} catch (Exception e){
			this.raiseError("on create source name: "+e.getMessage());
		}
		return sourceName;	
	}
	
	private boolean isClassExists(JwideDTO dto) {
		String className=EXPR_CLASS_PATH+"/"+this.createSourceName(dto)+".class";		
		return fh.isFileCreated(className);		
	}
	
	private class StringJavaFileObject extends SimpleJavaFileObject {
		private String source; 
		public StringJavaFileObject(String name, String source) {
			super(URI.create("string:///" + name.replace('.','/') + Kind.SOURCE.extension),Kind.SOURCE);
			this.source = source;
		}
		@Override
		public CharSequence getCharContent(boolean ignoreEncodingErrors) {
			return this.source;
		}
	}	
	
	private String getColType(JwideDTO dto,String column){
		if (dto.getType(column).equals("UNKNOW")) {
			return "String";
		} else {
			return dto.getType(column);
		}
	}
	
	private String fmtColName(String col){
		return gUtil.firstUpperLastLower(this.replaceInvalidChar(col).replace(" ","_"));
	}
	
	private String extractBeanDef(JwideDTO dto){
		StringBuffer def = new StringBuffer();
		for (String c:dto.getColumns()){
			if (def.length()>0) def.append(",");
			def.append(c);
			def.append("|");
			def.append(dto.getType(c));			
		}	
		return def.toString();
	}
	
	public String replaceInvalidChar(String value) {
		return replaceInvalidChar(value, INVALIDS, VALIDS);
	}
	
	private String replaceInvalidChar(String value, char[] invalidChars, char[] validChars){
		if (value==null) return null;

		for (int i=0;i<invalidChars.length;i++) {
			value=value.replaceAll(String.valueOf(invalidChars[i]),String.valueOf(validChars[i]));
		} 
		return value;
	}
	
	private void raiseError(String msg){
		throw new IllegalArgumentException(this.getClass().getName() + " - " + msg);
	}
	
	protected class CompilerDiagnosticListener implements DiagnosticListener<JavaFileObject>{
		
		StringBuffer log = new StringBuffer("");
		
		public CompilerDiagnosticListener(){					
		}
		
		public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
			log.append("Code->" +  diagnostic.getCode());
			log.append("\n");
			log.append("Column Number->" + diagnostic.getColumnNumber());
			log.append("\n");
			log.append("End Position->" + diagnostic.getEndPosition());
			log.append("\n");
			log.append("Kind->" + diagnostic.getKind());
			log.append("\n");			
			log.append("Line Number->" + diagnostic.getLineNumber());
			log.append("\n");
			log.append("Message->"+ diagnostic.getMessage(Locale.ENGLISH));
			log.append("\n");
			log.append("Position->" + diagnostic.getPosition());
			log.append("\n");
			log.append("Source" + diagnostic.getSource());
			log.append("\n");
			log.append("Start Position->" + diagnostic.getStartPosition());
			log.append("\n");
		}
		
		public String getLog(){
			return log.toString();
		}
	}	
}
