package jwide.generate;


import java.util.ArrayList;
import java.util.List;
import jwide.JwideConsts;
import jwide.JwideUtil;

public class JwideGenerateUtils {
    private static JwideGenerateUtils instance;
    private JwideUtil jUtil = JwideUtil.getInstance();
	
    public final String QUOTE='"'+"";
    public final String CR='\n'+"";
	
    /** Creates a new instance of IdeUtil */
    private JwideGenerateUtils() {    	 	
    }
    
    public synchronized static JwideGenerateUtils getInstance() {
	    if( instance == null ) {
	        instance = new JwideGenerateUtils();
	    }
	    return instance;
	}
    
    public String align(int tabs) {
    	StringBuffer a = new StringBuffer() ;
    	for (int i=1;i<=tabs;i++) {
    		//a.append("   ");
    		a.append("	"); // tab
    	}
    	return a.toString();
    }
    
    public boolean isNumber(String javaType) {
        boolean i=false;
        
        i=((javaType.toLowerCase().equals("java.math.bigdecimal"))  ||
           (javaType.toLowerCase().equals("double"))  ||
           (javaType.toLowerCase().equals("float"))   ||
           (javaType.toLowerCase().equals("int"))     ||
           (javaType.toLowerCase().equals("integer")) ||
           (javaType.toLowerCase().equals("short"))   ||
           (javaType.toLowerCase().equals("long")));       
        
        return i;       
    }
    
    public boolean isDate(String javaType) {
        // testar se o campo ? data
        boolean i=false;
        i=((javaType.equals("java.sql.Timestamp")) || 
           (javaType.equals("java.sql.Date")));
        return i;
    }
    
    public boolean isString(String javaType) {
        // testar se o campo ? data       
        boolean i=false;
        i=(javaType.equals("String"));          
        return i;
    } 
    
    // action.find-all-by-name or action.findAllByName
    public String toCamelCase(String value){
		StringBuilder fmt = new StringBuilder();					
		for (String a:jUtil.lineToArray(value,"-")){ 
			fmt.append(this.firstUpper(a));			
		}											
		return fmt.toString();
	}
    
    // action.findAllByName to action.find-all-by-name
    public String fromCamelCase(String value){
    	StringBuilder fmt = new StringBuilder();
    	if (value!=null){
    		for (char c:value.toCharArray()){
    			if (c>='A' && c<='Z'){
    				if (fmt.length()>0){
    					fmt.append("-");
    				}
    				fmt.append(String.valueOf(c).toLowerCase());
    			} else {
    				fmt.append(c);
    			}
    		}
    	}								
    	return fmt.toString();
    }
    
	public String extractControllerName(String uri){

		List<String> url=jUtil.lineToArray(uri, "/");
		StringBuilder controller = new StringBuilder();
		
		if (url.get(0).equals(JwideConsts.CONTROLLER_FILTER_BAR)){
			url.remove(0); // /jd
			url.remove(url.size()-1); // action
		}
		
		int i=-1;
		for (String u:url){
			i++;
			if (u==null || u.trim().equals("")){
				continue;
			}

			if (controller.length()>0){
				controller.append(".");
			}
			if (i==url.size()-1){
				controller.append(this.toCamelCase(u.replace(JwideConsts.CONTROLLER_FILTER_DOT,"")));
			} else {
				controller.append(u);
			}
		}

		return controller.toString();
	}
		    
    public String firstUpper(String var){        
        // converte a primeira letra p/ maiuscula e nao
        // altera as demais
        
        String Clast=var;
        String Cupper=var.toUpperCase();
        String c;
        if (var.length()>1)
           c=Cupper.charAt(0)+String.copyValueOf(Clast.toCharArray(),01,Clast.length()-1);
        else
           c=Cupper;
        return c;        
    }
    
    public String firstLower(String var){        
        // converte a primeira letra p/ minuscula e nao
        // altera as demais
    	
    	if (var==null || var.length()<=1){
    		return var;
    	}
    	
    	return var.substring(0,1).toLowerCase() + var.substring(1, var.length());      
    }
    
    public String firstUpperLastLower(String var){        
        // converte a primeira letra p/ maiuscula e 
        // as demais p/ minuscula
        
        String Clower=var.toLowerCase();
        String Cupper=var.toUpperCase();
        String c;
        if (var.length()>1)
           c=Cupper.charAt(0)+String.copyValueOf(Clower.toCharArray(),01,Clower.length()-1);
        else
           c=Cupper;
        return c;        
    }
    
    public Boolean isNull(String isNullable) {
    	return (isNullable.trim().equalsIgnoreCase("yes"));
    }
    
    public String toLabel(String columnName) {
    	ArrayList<String> lbl =jUtil.lineToArray(columnName.replace("_",","));

    	StringBuffer label=new StringBuffer("");		
    	int i=0;
    	for (String l:lbl) {
    		if (i>0) label.append(" ");
    		label.append(this.firstUpperLastLower(l));
    		i++;
    	}
    	return label.toString();
    }


}
