package jwide;

import javax.json.JsonNumber;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class JwideShortConverter implements Converter {
	
	   JwideUtil jwUtil = JwideUtil.getInstance();
		
	   public JwideShortConverter() {   	
	   }
	   
	   @SuppressWarnings("unchecked")
	   public Object convert(@SuppressWarnings("rawtypes") Class type, Object value) throws ConversionException {
		   if (value == null) return null;

	      // Support Calendar and Timestamp conversion
	      if (value instanceof Short) {
	      	 return value;         
	      }
	      else if (value instanceof String) {
	    	  try {
	             return new Short(value.toString().trim().replace(",",""));
	    	  } catch (Exception e) {
	    		 return null;
	    	  }
	      } else if (value instanceof JsonNumber){
	    	  return ((JsonNumber) value).intValue();
	      } else {
	         throw new ConversionException("Type not supported: " +
	         value.getClass().getName());
	      }
	   }

}
