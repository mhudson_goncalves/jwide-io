package jwide;

public interface IJwideValidator {	

	public final String CHECK        ="{error.check}";
	public final String UPPER        ="{error.upper}";
	public final String LOWER        ="{error.lower}";
	public final String REQUIRED     ="{error.required}";
	public final String NOT_REQUIRED ="{error.not_required}";
	public final String RANGE        ="{error.range}";
	public final String SIZE         ="{error.size}";
	public final String EMAIL        ="{error.email}";
}
