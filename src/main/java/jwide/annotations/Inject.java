package jwide.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jwide.JwideBO;

// @Inject(name=TesteBO.class, method="find")
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {	
	Class<? extends JwideBO> name();
	String method();  
}
