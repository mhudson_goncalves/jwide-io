package jwide.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// declarar antes do public class... 
@Retention(RetentionPolicy.RUNTIME)
public @interface Bean {
    String connection()  default "";
    String primaryKey()  default "";
    String aowner()      default "";
    String packageName() default "";
}
