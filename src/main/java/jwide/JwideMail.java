package jwide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.InternetAddress;

public class JwideMail {
	
	  public static final String TEXT_PLAIN_MIME="text/plain;charset=ISO-8859-1";
	  public static final String HTML_MIME="text/html;charset=ISO-8859-1";
	  private static final String EMAIL_SEPARATOR=";";
	  
	  private JwideUtil jUtil = JwideUtil.getInstance();
	  
	  /* Exemplo:
	     JwideMailVO vo = new JwideMailVO();
         JwideMail Mail = new JwideMail();
         vo.setSmtpHost("smtp.sao.terra.com.br");
         vo.setUserName("mhudson@terra.com.br");
         vo.setPassword("MinhaSenhaDeEmail");
  
         vo.setFrom("Marcos Hudson");
         vo.setTo("ti@santamalia.com.br");
         vo.setText("teste de email");
         vo.setSubject("Teste de email");
  
         Mail.send(vo);
      */
	public JwideMail() {
	}

	public void send(JwideMailVO msg) throws Exception {
		Multipart mimeMultiPart = new MimeMultipart();
	   	      
		Properties mailProps = new Properties();		
		mailProps.put("mail.smtp.host", msg.getSmtpHost());
		
		if (msg.getSmtpPort()!=null) {
			mailProps.put("mail.smtp.port", msg.getSmtpPort());
		}
		
		if (msg.isStartTls()){
			mailProps.put("mail.smtp.starttls.enable", "true");
		}

		JwideMailAutenticator auth = null;
		Session mailSession =null;
		if (msg.isNeedAuthentication()) {
			// Autorizacao Necess?ria
			auth = new JwideMailAutenticator(msg);
			mailProps.put("mail.smtp.auth","true");                
			mailProps.put("mail.user",auth.userName);
			mailProps.put("mail.from",msg.getFrom());
			mailProps.put("mail.to", msg.getTo());
			mailSession = Session.getInstance(mailProps,auth);
		} else {
			mailProps.put("mail.smtp.auth", "false");
			mailSession = Session.getInstance(mailProps);
		}			 
		
		mailSession.setDebug(false);
		Message email = new MimeMessage(mailSession);
		
		email.setRecipients(Message.RecipientType.TO,this.mailToAdress(msg.getTo()));
		if (msg.getCc()!=null && !msg.getCc().equals("")) {
			email.setRecipients(Message.RecipientType.CC,this.mailToAdress(msg.getCc()));
		}
		if (msg.getBcc()!=null && !msg.getBcc().equals("")) {
			email.setRecipients(Message.RecipientType.BCC,this.mailToAdress(msg.getBcc()));
		}
		
		// o corpo do email deve vir primeiro, sen?o ser? incluido como anexo se houverem arquivos anexados
		MimeBodyPart messageMimeBodyPart= new MimeBodyPart();	
		messageMimeBodyPart.setContent(msg.getText(),msg.getMimeType());
		mimeMultiPart.addBodyPart(messageMimeBodyPart);
				
		// Arquivos Anexados
		for (String attachedFile:msg.getAttachedFiles()) {
			MimeBodyPart attachedMimeBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(attachedFile);
			attachedMimeBodyPart.setDataHandler(new DataHandler(fds));				
			attachedMimeBodyPart.setFileName(new File(attachedFile).getName());
			//attachedMimeBodyPart.setDescription(Part.ATTACHMENT);
			attachedMimeBodyPart.setDescription(attachedFile);
			mimeMultiPart.addBodyPart(attachedMimeBodyPart);
		}	
	
		email.setFrom(new InternetAddress(msg.getFrom()));
		email.setSubject(msg.getSubject()!=null?msg.getSubject():"");			            
		email.setContent(mimeMultiPart);
		Transport.send(email);		
	}	
	
	private InternetAddress[] mailToAdress(String emails) throws Exception{
		// email group
		List<InternetAddress> adress = new ArrayList<InternetAddress>();
		for (String mailTo:jUtil.lineToArray(emails,JwideMail.EMAIL_SEPARATOR)){			
			adress.add(new InternetAddress(mailTo));
		}	
		return adress.toArray(new InternetAddress[]{});
	}
}
