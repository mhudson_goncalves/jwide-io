package jwide;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;

public class JwideConvertUtils {
	private static JwideConvertUtils instance;
	
	private JwideConvertUtils() {
    	// Conversor para campos 		
  	    ConvertUtils.register(new JwideTimestampConverter(),Timestamp.class);
  	    ConvertUtils.register(new JwideDoubleConverter(), Double.class);
  	    ConvertUtils.register(new JwideLongConverter(), Long.class);
  	    ConvertUtils.register(new JwideIntegerConverter(), Integer.class);
  	    ConvertUtils.register(new JwideShortConverter(), Short.class);    	
  	    ConvertUtils.register(new JwideBigDecimalConverter(), BigDecimal.class);
  	    ConvertUtils.register(new JwideStringConverter(), String.class);
    }    
    
    public synchronized static JwideConvertUtils getInstance() {
	    if( instance == null ) {
	        instance = new JwideConvertUtils();
	    }
	    return instance;
	}
    
    public Object convert(Class<?> clazz, Object value){
    	if (value==null) {
    		return null;
    	}
    	
    	Converter converter = ConvertUtils.lookup(clazz);
    	return converter.convert(clazz, value);
    }
}
