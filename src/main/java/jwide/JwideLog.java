package jwide;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.BufferedWriter;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JwideLog {

	private static final Integer MAX_LOG_FILE = (32 * 1024 * 1024); // 32MB	
	private static final String DATE_FORMAT = "yyyyMMdd";	
	private static final String HOUR_FORMAT = "HHmmssSSS";	
	
	private int bytes_written = 0;	
	private static JwideLog instance=null;	
	private JwideStack stack = JwideStack.getInstance();
	private JwideUtil jUtil = JwideUtil.getInstance();
	
	public enum LogType {
		INFO,
		WARNING,
		ERROR
	}
	
	private JwideLog() {
	}

	public static JwideLog getInstance(){
		if(instance==null){
			synchronized(JwideLog.class){
				instance=new JwideLog();
			}			
		}
		return instance;
	}
	
	private File getCurrentLogFile() {		
		if (stack.get("log.file")==null){
			this.refreshLogFileName();
		} 
		return new File(stack.get("log.file").toString());
	}
	
	private void refreshLogFileName() {
		stack.set("log.file", this.formatLogFileName());
	}

	public void writeLog(Class<?> clazz, Exception exception) {
		this.writeLog(clazz, exception, null);	
	}	
	
	public void writeLog(Class<?> clazz, Exception exception, String description) {
		this.writeLog(clazz.getName(), exception, description);	
	}
	
	public void writeLog(String source, Exception exception, String description) {

		StringBuffer message = new StringBuffer();		
		message.append(source);
		message.append(" ");	
		message.append(description!=null?description+" ":"");
		message.append(exception.getMessage());	
		
		StringWriter trace = new StringWriter();
		exception.printStackTrace(new PrintWriter(trace));
		message.append("\n");
		message.append(trace.toString());
		
		this.writeLog(LogType.ERROR,message.toString());		
	}	

	public void writeLog(LogType LogType, String message) {
		synchronized(instance){
					
			try (BufferedWriter writer = Files.newBufferedWriter( Paths.get(this.getCurrentLogFile().toURI()), UTF_8, APPEND, CREATE)) {				
				writer.write(LogType.toString());
				writer.write(" [");
				writer.write(jUtil.formatDate(jUtil.getTodayTimestamp()));
				writer.write("] ");				
				writer.write(message.toCharArray(), 0, message.length());				
				writer.newLine();	
				bytes_written+=message.length();				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (bytes_written>MAX_LOG_FILE){
					this.refreshLogFileName(); 
					bytes_written=0;
				}
			}
		}
	}
	
	private String formatLogFileName() {				
		return jUtil.getJwideResourcePath( 
				                          "jwide-" + 
		                                  jUtil.formatDate(DATE_FORMAT,jUtil.getTodayTimestamp()) +
		                                  "-"+
		                                  jUtil.formatDate(HOUR_FORMAT,jUtil.getTodayTimestamp()) +
		                                  ".log"
		                                 );		
	}	

}
