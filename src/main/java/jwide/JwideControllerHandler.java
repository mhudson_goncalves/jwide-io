package jwide;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import jwide.JwideDTO;
import jwide.JwideExecuteHandler;
import jwide.generate.JwideGenerateUtils;

import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class JwideControllerHandler {	
	
	private JwideExecuteHandler eHand = new JwideExecuteHandler();
	private JwideXmlReader xmlReader=new JwideXmlReader();	
	private JwideJsonReader jsonReader = new JwideJsonReader();
	private JwideUtil jUtil=JwideUtil.getInstance();
	private JwideGenerateUtils gUtil=JwideGenerateUtils.getInstance();
	private JwideErrorsList ErrorsList = new JwideErrorsList();	
		
	private HttpServletRequest  request=null;
	private HttpServletResponse response=null;
	private ServletContext      context=null;   
    
    private static final String SEPARATOR=System.getProperty("file.separator");
    private static final String ENCODING = "UTF-8";
        
    private JwideReflection Reflect = JwideReflection.getInstance();    
    private JwideController controllerInstance=null;
    
    private String actionName = null;
    
	public JwideControllerHandler(
			                       HttpServletRequest  request,
                                   HttpServletResponse response,
                                   ServletContext      context
                                 ) {
		this.request=request;
		this.response=response;
		this.context=context;					
	}
	
	protected void setControllerName(String controller) {
		if (controller==null) throw new IllegalArgumentException("JwideControllerHandler - Controller Not Indicated");
		
		String controllerName=controller+JwideConsts.CONTROLLER_SUFIX;		
		try {
		   controllerInstance = this.createControllerInstance(controllerName);
		} catch (Exception e) {
		   throw new IllegalArgumentException("JwideControllerHandler - Controller Cannot be loaded: "+this.getClassPath()+SEPARATOR+controllerName+"\n"+e.getMessage());		  	
		}		
	}
	
	protected void setActionName(String actionName) {
		this.actionName = actionName;
	}
	
	private JwideController createControllerInstance(String controllerName) throws Exception {
		return (JwideController)Class.forName(controllerName).getDeclaredConstructor().newInstance();
	}	
	
	public void execute() throws Exception {		

		Map<String, Object> parameters = this.getParameterMap(request);		
		String actionName = this.formatActionMethod(parameters);	
		List<String> metods = Reflect.getMethods(controllerInstance);		   

		if (metods.contains(actionName)) {
			// executar a action
			controllerInstance.setAction(actionName);
			eHand.execute(controllerInstance, request, response, context, actionName, parameters );

			if (controllerInstance.validator.isEmpty()){
				JwideDataSource.commit();	
			} else {
				JwideDataSource.rollback();
			}

			JwideDataSource.close(); // close all threadsafe connections
			// tratamento de paginas
			if (controllerInstance.getLastResult().equals(JwideController.TO_XML)) {
				if (controllerInstance.validator.getErrors().isEmpty()){
					this.write("text/xml", HttpServletResponse.SC_OK, controllerInstance.getParseContent() );
				} else {
					this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, this.extractErrors(controllerInstance.validator.getErrors()) );
				}				
			} else if (controllerInstance.getLastResult().equals(JwideController.TO_JSON)) {			
				if (controllerInstance.validator.getErrors().isEmpty()){
					this.write("application/json", HttpServletResponse.SC_OK, controllerInstance.getParseContent());
				} else {
					this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, this.extractErrors(controllerInstance.validator.getErrors()));
				}				
			} else if (controllerInstance.getLastResult().equals(JwideController.TO_REPORT)) {
				if (controllerInstance.validator.getErrors().isEmpty()){						   
					this.write("application/pdf", HttpServletResponse.SC_OK, controllerInstance.reportContent);	
				} else {
					this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, this.extractErrors(controllerInstance.validator.getErrors()));
				}
			} else if (controllerInstance.getLastResult().equals(JwideController.TO_EXCEL)) {
				if (controllerInstance.validator.getErrors().isEmpty()){
					response.setHeader("Content-Disposition", "attachment; filename=\""+ controllerInstance.excelFileName + "\"");
					this.write("application/vnd.ms-excel", HttpServletResponse.SC_OK, controllerInstance.getParseContent());	
				} else {					   
					this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, this.extractErrors(controllerInstance.validator.getErrors()));
				}				
			} else if (controllerInstance.getLastResult().equals(JwideController.TO_TEXT)) {
				if (controllerInstance.validator.getErrors().isEmpty()){
					this.write("text/plain", HttpServletResponse.SC_OK, controllerInstance.getParseContent());
				} else {
					this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, this.extractErrors(controllerInstance.validator.getErrors()));
				}			
			} else if (controllerInstance.getLastResult().indexOf(JwideController.TO_PAGE)==0){
				if (controllerInstance.validator.getErrors().isEmpty()){
					String page=controllerInstance.getLastResult().replace(JwideController.TO_PAGE,"");
					request.getRequestDispatcher(page).forward(request,response);
				} else {
					this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, this.extractErrors(controllerInstance.validator.getErrors()));
				}			
			}				   
		} else { // se senhuma acao foi encontrada, enviar erro para a pag	   
			StringBuffer message = new StringBuffer();
			message.append("Invalid Action '" + controllerInstance.getClass().getName() +"'");
			message.append("\n");
			message.append("Content '" + parameters.toString() +"'");
			this.write("text/plain", HttpServletResponse.SC_BAD_REQUEST, message.toString());		  
		}
	}
	
	private void write(String type, Integer responseCode, String content) throws IOException {
		response.setContentType(type);	
		response.setStatus(responseCode);
		response.setCharacterEncoding("utf-8");
		
	    PrintWriter writer = response.getWriter();
	    writer.write(content);
	    writer.close();	   
	}
	
	private void write(String type, Integer responseCode, byte[] content) throws IOException {		
		response.setContentType(type);
		response.setStatus(responseCode);
		response.getOutputStream().write(content);
		response.getOutputStream().flush();
		response.getOutputStream().close();		
	}
	
	private String extractErrors(JwideErrors Errors){			
		ErrorsList.setErrors(Errors); 
		JwideDTO ErrorsDTO =  ErrorsList.getErrorsList();		   

		StringBuffer errBuffer = new StringBuffer("");
		ErrorsDTO.beforeFirst();

		while (ErrorsDTO.next()) {		    
			if (errBuffer.length()>0) {
				errBuffer.append("\n");
			}
			errBuffer.append(ErrorsDTO.get("mensagem"));
		} 	

		return errBuffer.toString();   
	}	
	
	private String getClassPath() {		
		return context.getRealPath("/WEB-INF/classes");
	}

	@SuppressWarnings("rawtypes")
	private Map<String,Object> getParameterMap(HttpServletRequest request) throws Exception {	

		java.util.Map<String, Object> params=new java.util.HashMap<>();
		
		// incluir todos os parametros da URL (api-docs do swagger est� sendo gerado com o ?action.find...)
		Map mapRequest = request.getParameterMap();
		Map.Entry entryRequest;
		Iterator<?> iteratorRequest = mapRequest.entrySet().iterator();		
		String key;
		while( iteratorRequest.hasNext() )	{
			entryRequest = ( Map.Entry ) iteratorRequest.next();
			key = (String) entryRequest.getKey();
			if (key!=null){
				for( int i = 0; i < request.getParameterValues( key ).length; i++ ){
					params.put(key,request.getParameterValues( key )[ i ]);
				}
			}		
		}		

		// testar json e xml
		if (jUtil.isJsonContentType(request.getContentType())) { 					
			JwideDTO JsonDTO=this.getJsonContent(this.extractHeaderContent(request));
			this.bindToParams(JsonDTO, params);				
		} else if (jUtil.isXmlContentType(request.getContentType())) { 				
			JwideDTO JsonDTO=this.getXmlData(this.extractHeaderContent(request));
			this.bindToParams(JsonDTO, params);			
		} 

		return params;	
	}
	
	private String formatActionMethod(Map <String, Object> params) {		

		if (actionName==null){ // pode estar no body
			for (Map.Entry<String, Object> pair : params.entrySet()) {
				String key = pair.getKey();
				if (key!=null){
					if (key.trim().indexOf(JwideConsts.URL_ACTION_PREFIX)==0){					
						actionName = gUtil.toCamelCase(key.replace(JwideConsts.URL_ACTION_PREFIX, ""));
					}
				}
			}
		}

		if (actionName!=null){
			return (JwideConsts.ACTION_METHOD_PREFIX + actionName);
		} else {
			return "Empty";
		}
	}	
	
	private String extractHeaderContent(HttpServletRequest request){
		try {
			return IOUtils.toString(request.getInputStream(), ENCODING);
		} catch (Exception e){
			return null;
		}
	}	
	
	private JwideDTO getXmlData(String content) throws Exception {
		JwideDTO dataDTO = new JwideDTO();
		if (content!=null && !content.trim().equals("")){			
			dataDTO.populate(xmlReader.parseFromString(content.trim(), "/root/row", JwideConsts.XML_ENCODING));	
		}	
		return dataDTO;	
	}		

	private JwideDTO getJsonContent(String content) throws Exception {		
		JwideDTO dataDTO = new JwideDTO();

		if (content!=null && !content.trim().equals("")){			
			dataDTO.populate(jsonReader.parse(content));	
		}
		return dataDTO;		
	}
	
	private void bindToParams(JwideDTO valuesDTO,Map<String, Object> params){		
		valuesDTO.beforeFirst();
		while (valuesDTO.next()){
			for (String column:valuesDTO.getColumns()) {				
				if ((valuesDTO.get(column)!=null) && (!valuesDTO.get(column).equals(""))) {							
					params.put(column,jUtil.decodeCharacter(valuesDTO.get(column)));
				}
			}
		}	
	}	
	
}
