package jwide;

import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import jwide.annotations.Request;

public class JwideRequestService implements IJwideValidator {
	
	/*
	  import jwide.annotations.Request;
	  @Request (url="${url-na-jwide-properties}/aa/bbb")
	  public MyRequestService extends JwideRequestService {
	  
	     public List<JwideDataObject> findByID(BigDecimal id) throws Exception
	        return super.mapping("/api/services/service/find-by-id/?id={id}", id).sendPOST().parseResponse(); 
	     }
	     
	     public List<JwideDataObject> insert(BeneficiarioVO beneficiario) throws Exception
	        return super.mapping("/api/services/service/insert").body(beneficiario).sendPOST().parseResponse(); 
	     }
	  }
	*/
	
	private String rootUrl = null;
	private JwideUtil jUtil = JwideUtil.getInstance();
	
	private static final Pattern PROPERTIES_PATTERN = Pattern.compile("\\$\\{(.*?)\\}"); //${id}

	public JwideRequestService() {
	}	
	
	public JwideRequestResponse mapping(String path) throws Exception {
		return new JwideRequestResponse().url(this.formatUrl(path));		
	}
	
	public JwideRequestResponse mapping(String path, Object... values) throws Exception {
		return new JwideRequestResponse().url(this.formatUrl(path)).mappingValues(values);		
	}	
	
	protected void beforeSend(JwideRequestResponse  request) throws Exception {
		
	}
	
	private String formatUrl(String path) throws Exception {
		if (rootUrl==null){
			if (this.getClass().isAnnotationPresent(Request.class)) { 
				Request service =(Request) this.getClass().getAnnotation(Request.class);
				rootUrl = service.url();
			} else {
				rootUrl = "";
			}

			StringBuffer url = new StringBuffer();
			Matcher m = PROPERTIES_PATTERN.matcher(rootUrl);				

			while (m.find()) {				
				m.appendReplacement(url, jUtil.getProperty(m.group(1)));
			}
			m.appendTail(url);	

			rootUrl = url.toString();
		}	
		return rootUrl + path;		
	}	
	
	public class JwideRequestResponse {
		
		private String url=null;
		private Object[] mappingValues = null;
		
		private JwideJsonReader jsonReader = new JwideJsonReader(true);
		private JwideXmlReader xmlReader = new JwideXmlReader();
		private JwideHttpRequest httpRequest = new JwideHttpRequest();
		private JwideValidator validator = new JwideValidator();
		
		private String body = null;
		private String response = null;
		private int responseCode = -1;
		
		private JwideUtil jUtil = JwideUtil.getInstance();		
		private final Pattern PARAMS_PATTERN = Pattern.compile("\\{(.*?)\\}"); //{id}
				
		public JwideRequestResponse(){	
			
		}
		
		public JwideRequestResponse url(String url){
			this.url = url;
			return this;
		}
		
		public JwideRequestResponse mappingValues(Object... values){
			this.mappingValues = values;
			return this;
		}
		
		public JwideRequestResponse body(JwideVO body) throws Exception {
			this.body =  jsonReader.toJson(new JwideDTO().populate(body).toDataObject().get(0));
			return this;
		}
		
		public JwideRequestResponse body(JwideDataObject body) throws Exception {
			this.body =  jsonReader.toJson(body);
			return this;
		}
		
		public JwideRequestResponse body(List<JwideDataObject> body) throws Exception {
			this.body =  jsonReader.toJson(body);
			return this;
		}
		
		public JwideRequestResponse body(String body) throws Exception {
			this.body =  body;
			return this;
		}
		
		public String getBody(){
			return this.body;
		}
		
		public JwideRequestResponse addHeaderValues(String name, String value) throws Exception {
			this.httpRequest.addHeaderValues(name, value);
			return this;
		}
			
		public JwideRequestResponse sendPOST() throws Exception {
			beforeSend(this);
			response = httpRequest.postURL(this.getUrl() , JwideHttpRequest.ContentType.JSON, this.getBody());
			responseCode = httpRequest.getResponseCode();
			return this;
		}		
				
		public JwideRequestResponse sendPUT() throws Exception {
			beforeSend(this);
			response = httpRequest.putURL(this.getUrl() , JwideHttpRequest.ContentType.JSON, this.getBody());
			responseCode = httpRequest.getResponseCode();
			return this;
		}	
				
		public JwideRequestResponse sendGET() throws Exception {
			beforeSend(this);
			response = httpRequest.getURL(this.getUrl() , JwideHttpRequest.ContentType.JSON, null);
			responseCode = httpRequest.getResponseCode();
			return this;
		}	
				
		public JwideRequestResponse sendDELETE() throws Exception {
			beforeSend(this);
			response = httpRequest.deleteURL(this.getUrl() , JwideHttpRequest.ContentType.JSON, this.getBody());
			responseCode = httpRequest.getResponseCode();
			return this;
		}
		
		public JwideRequestResponse sendSOAP() throws Exception {
			beforeSend(this);
			response = httpRequest.postURL(this.getUrl() , JwideHttpRequest.ContentType.SOAP, this.getBody());
			responseCode = httpRequest.getResponseCode();
			return this;
		}
		
		public JwideRequestResponse throwsExceptionOnResponseError() throws Exception {
			if (responseCode>=HttpServletResponse.SC_BAD_REQUEST){
				validator.flush(response);
			}
			return this;
		}
		
		public String getResponse(){
			return this.response;
		}
		
		public Integer getResponseCode(){
			return responseCode;
		}
		
		public List<JwideDataObject> parseResponse() throws Exception {
			return jsonReader.parse(this.response);
		}
		
		public List<JwideDataObject> parse(String json) throws Exception {
			return jsonReader.parse(json);
		}
		
		public List<JwideDataObject> parseResponseXml(String node) throws Exception {
			return xmlReader.parseFromString(this.response, node);
		}	
		
		public List<JwideDataObject> parseXml(String xml, String node) throws Exception {
			return xmlReader.parseFromString(xml, node);
		}
		
		public String getUrl() throws Exception {			
			StringBuffer sb = new StringBuffer();
			if (this.mappingValues!=null){
				Matcher paramsMatcher = PARAMS_PATTERN.matcher(this.url);				

				int i=0;
				while (paramsMatcher.find()) {		
					if (this.mappingValues.length<=i){
						throw new IllegalArgumentException("JwideRequestService: mapping values is less than mapping params");
					}
					paramsMatcher.appendReplacement(sb, this.formatValue(this.mappingValues[i++]));
				}
				paramsMatcher.appendTail(sb);				
			} else {
				sb.append(this.url);
			}

			return sb.toString();
		}
		
		private String formatValue(Object value) throws Exception {			

			String fmt = null;
			
			if (value==null){
				fmt = "";
			} else if (value instanceof Timestamp){				
				fmt = jUtil.formatDate(JwideJsonReader.DATE_FORMAT, (Timestamp)value).replace(" 00:00:00.000","");
			} else {
				fmt = String.valueOf(value);
			}
			
			return URLEncoder.encode(fmt, JwideHttpRequest.CHARSET);
		}
		
	}	

}
