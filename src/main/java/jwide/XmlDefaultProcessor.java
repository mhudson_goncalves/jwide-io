package jwide;

import org.dom4j.Element;
import org.dom4j.ElementPath;
import org.dom4j.ElementHandler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XmlDefaultProcessor implements ElementHandler {

	private List<JwideDataObject> values = new ArrayList<>();

	public XmlDefaultProcessor() {	   
	} 

	public List<JwideDataObject> getValues() {	
		return values;
	}   

	public void onStart(ElementPath path) {		
	}

	public void onEnd(ElementPath path)   {	    
		Element element = path.getCurrent();
		Iterator<?> itr = element.elementIterator();

		JwideDataObject row = new JwideDataObject();

		while(itr.hasNext())  {
			Element child = (Element) itr.next();
			if (child.isTextOnly()) { // n�o possui outros n�s de xml dentro de si
				row.addValue(child.getQualifiedName(), child.getData());
			} else {
				row.addValue(child.getQualifiedName(), child.asXML());         
			}
		}
		element.detach();

		values.add(row);
	}

}
