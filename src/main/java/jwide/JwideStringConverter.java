package jwide;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class JwideStringConverter implements Converter {

	JwideUtil jwUtil = JwideUtil.getInstance();

	public JwideStringConverter() {   	
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.Converter#convert(java.lang.Class,
		   java.lang.Object)
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object convert(Class type, Object value) throws ConversionException {

		if (value == null) return null;		
		if (value.toString().equals("")) return null;		
		return value.toString();
	}

}
