package jwide;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JwideParam extends JwideDTO {

	//private BeanUtilsBean BeanUtils = BeanUtilsBean.getInstance();
	private JwideUtil jUtil=JwideUtil.getInstance();
	private JwideConvertUtils convertUtils = JwideConvertUtils.getInstance();

	public JwideParam() {
		this.init();
	}
	
	public JwideParam(JwideDTO dto){
		this.init();
		this.populate(dto);		
		this.absolute(dto.getCurrentRow()); 
	}
	
	private void init(){
		JwideConvertUtils.getInstance();
	}
	
	public void populate(Map<String, Object> params) {
		List<String> cols = new ArrayList<String>();
		List<Object> row = new ArrayList<Object>();

		Set <Map.Entry<String,Object>> set = params.entrySet();	
		Iterator<Map.Entry<String,Object>> iter = set.iterator();
		while (iter.hasNext()) {
			Map.Entry<String,Object> n = (Map.Entry<String,Object>) iter.next();	
			cols.add(n.getKey().toString());

			if (n.getValue() instanceof String[]) row.add(((String[])n.getValue())[0]);	  
			else                                  row.add(n.getValue());	 
		} 

		this.clearRows();
		this.setColumns(cols.toArray(new String[] {}));
		this.addRow(row.toArray(new Object[] {}));
		this.first();
	}

	public Object get(String columnName) {
		if (!this.hasColumn(columnName))
			return null;
		return super.get(columnName);
	}

	public Object get(int columnPos) {
		if (columnPos == -1)
			return null;
		return super.get(columnPos);
	}

	public int getColumnPos(String columnName) {
		if (!this.hasColumn(columnName))
			return -1;
		return super.getColumnPos(columnName);
	}

	public Timestamp getTimestamp(int columnPos) {
		if (this.get(columnPos)==null) return null;
		if (get(columnPos) instanceof String) {
			try{
				return jUtil.strToTimestamp(get(columnPos).toString().trim());
			} catch(Exception e){}
		}
        return super.getTimestamp(columnPos);
	}

	public JwideParam assignCurrentRow(JwideDTO dto) {
		this.clearRows();
		this.setColumns(dto.getColumns());
		// prever colunas calculadas (n?o utilizar o getRow)
		List<Object> row = new ArrayList<Object>();
		for (String c:dto.getColumns()) row.add(dto.get(c));
		this.addRow(row);
		
		this.first();
		return this;
	}

	public JwideVO copyProperties(JwideVO vo) {		

		// Original n�o binda valores oriundos de parse de json
		//  for (Field f:vo.getClass().getDeclaredFields()){
		//  	f.setAccessible(true);
		//  }
		//  BeanUtils.populate(vo,this.toHashMap());

		for (Field field : vo.getClass().getDeclaredFields()) {				
			if (this.hasColumn(field.getName())) {
				field.setAccessible(true);	
				try {
					field.set(vo, convertUtils.convert(field.getType(), this.get(field.getName())));	
				} catch (Exception e){
				}
			}
		}			
		//BeanUtils.populate(vo,this.toHashMap());

		return vo;
	}
	
	public Integer[] toIntegerArray(String columnName){	
		return this.toIntegerArray(columnName,",");
	}
	public Integer[] toIntegerArray(String columnName,String separator){		
		List<Integer> values = new ArrayList<Integer>();
		try{
			for (String value:jUtil.lineToArray(this.getString(columnName).replace("[","").replace("]",""),separator)){
				values.add(new Integer(value.trim()));				
			}
		} catch (Exception e){}
		return values.toArray(new Integer[]{});		
	}
	
	public Double[] toDoubleArray(String columnName){
		return this.toDoubleArray(columnName,",");
	}
	public Double[] toDoubleArray(String columnName,String separator){
		List<Double> values = new ArrayList<Double>();
		try{
			for (String value:jUtil.lineToArray(this.getString(columnName).replace("[","").replace("]",""),separator)){
				values.add(new Double(value.trim()));				
			}
		} catch (Exception e){}
		return values.toArray(new Double[]{});	
	}
	
	public Timestamp[] toTimestampArray(String columnName){
		return this.toTimestampArray(columnName,",");
	}
	public Timestamp[] toTimestampArray(String columnName,String separator){
		List<Timestamp> values = new ArrayList<Timestamp>();
		try{
			for (String value:jUtil.lineToArray(this.getString(columnName).replace("[","").replace("]",""),separator)){
				values.add(jUtil.strToTimestamp(value));			
			}
		} catch (Exception e){}
		return values.toArray(new Timestamp[]{});	
	}
	
	public String[] toStringArray(String columnName){
		return this.toStringArray(columnName,",");		
	}
	public String[] toStringArray(String columnName,String separator){
		return this.lineToArray(columnName,separator).toArray(new String[]{});	
	}
	
    public ArrayList<String> lineToArray(String columnName){
    	return this.lineToArray(columnName,",");
    }
    
    public ArrayList<String> lineToArray(String columnName,String separator){
    	try{
    		return jUtil.lineToArray(this.getString(columnName).replace("[","").replace("]",""),separator);
    	} catch (Exception e){
    		return new ArrayList<String>();
    	}
    }	
  
    /*
	private Map<String,Object> toHashMap(){
		java.util.Map<String, Object> params = new java.util.HashMap<String, Object>();
		for (String column : this.getColumns()) {			
			params.put(column, this.formatColumnToExport(column));
		}
		return params;
	}	
	*/
 
}
