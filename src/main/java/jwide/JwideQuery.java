
/*
 * Created on 07/12/2004
 */

package jwide;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.sql.SQLException;

/**
 * @author MHRG
 */

public class JwideQuery {	
	
	private ResultSet rs=null;
	private Connection Conn=null;
	private PreparedStatement stm=null;	
	private int maxRows=0;
	public  boolean FOUND=false;
	private int updateCount=0;
	private String sql=null;
	private static Pattern parameter = Pattern.compile(":(\\w+)");
	protected List<String> parameterNames = new ArrayList<String>();	
	
	public JwideQuery() {		
	}			
	
	public void setSql(String sql) {
		this.sql=sql;
	}
	
	public String getSql() {
		return sql;
	}
	
	public void prepareStatement() throws SQLException {		
		this.prepareStatement(sql,0,false);
	}
	
	public void prepareStatement(int fetchSize) throws SQLException {		
		this.prepareStatement(sql,fetchSize,false);
	}
	
	public void prepareStatementForBuilk(int fetchSize) throws SQLException {
		this.prepareStatement(sql, fetchSize,true);
	}
	
	public void setConnection(Connection Conn) {
		this.Conn = Conn;
	}
	
	protected void setResultSet(ResultSet rs) {
		this.rs=rs;
	}
	
	public ResultSet getResultSet() {		
		return rs;
	}	
	
	public void setMaxRows(int maxRows) {
		if (maxRows<0) throw new IllegalArgumentException("JwideQuery: Invalid MaxRows: "+maxRows);		
		this.maxRows=maxRows;
	}
	
	public int getMaxRows() {
		return maxRows;
	}
	
	public int getUpdateCount() {
		return updateCount;
	}
	
    private String parseSQL(String sql) {
       if ((sql.indexOf("?")>=0) && (sql.indexOf(":")>=0)) {			
			throw new IllegalArgumentException("JwideQuery: Cannot Handle '?' and ':' parameters together");
	   }	
       
       Matcher m = parameter.matcher(sql);
	   StringBuffer sb = new StringBuffer();
	   parameterNames = new ArrayList<String>();

	   while (m.find()) {
	      parameterNames.add(m.group(1));
	      m.appendReplacement(sb, "?");
	   }
	   m.appendTail(sb);	   
	   
	   return sb.toString();
	}
    
    public List<String> getParameterNames() {
    	return parameterNames;
    }
	
	protected void prepareStatement(String sql, int fetchSize,boolean forBuilk) throws SQLException {
		String sqlJdbc=parseSQL(sql);
		// Aten??o: Cuidado ao colocar parametros no prepareStatement
		//stm=con.prepareStatement(sqlJdbc,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		// java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY
		if (!forBuilk) stm=Conn.prepareStatement(sqlJdbc);
		else           stm=Conn.prepareStatement(sqlJdbc,java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
		
		if (fetchSize>0) stm.setFetchSize(fetchSize);		
		stm.setMaxRows(maxRows);
	}
	
	public void executeQuery() throws SQLException {       
		rs=stm.executeQuery();                                   
    } 
    
    public void executeUpdate() throws SQLException {     	
    	stm.executeUpdate();
    	this.updateCount=stm.getUpdateCount();                        
    }
    
    public boolean next() throws SQLException {    	
    	FOUND=rs.next();  		    	
    	return FOUND;    	
    }
    
    public List<String> getColumns() throws SQLException {
    	List<String> columns;
    	
    	ResultSetMetaData rsmd = rs.getMetaData(); 
    	columns = new ArrayList<String>(rsmd.getColumnCount());
    	for (int i=1;i<=rsmd.getColumnCount();i++) { 
    		columns.add(rsmd.getColumnName(i));
    		//types.add(rsmd.getColumnTypeName(i));
    		//sizes.add(Integer.toString(rsmd.getColumnDisplaySize(i)));
    		//rsmd.getScale(i); -- casas decimais
    	} 	    	
    	return columns;
    }
    
//  --- inicio dos sets dos parametros
    
    public void setParameterValue(int parameterIndex, Object parameterValue) throws SQLException {
    	boolean isNAN=false;
    	// tratamento de NAN e Infinty    	
    	if (parameterValue!=null){    		
    		if ((parameterValue instanceof Double) && (Double.isNaN((Double)parameterValue))) isNAN=true;
    		if ((parameterValue instanceof Float) && (Float.isNaN(((Float)parameterValue)))) isNAN=true;
    		
    		if ((parameterValue instanceof Double) && (Double.isInfinite((Double)parameterValue))) isNAN=true;
    		if ((parameterValue instanceof Float) && (Float.isInfinite(((Float)parameterValue)))) isNAN=true;   		    		
    	}

    	if (parameterValue==null) {
    		if (JwideDatabaseObjects.isOracleConnection(Conn)) {
                stm.setNull(parameterIndex,java.sql.Types.NULL);
    		} else {
    			stm.setNull(parameterIndex,java.sql.Types.OTHER);
    		}
    	}
    	else if (isNAN) {
    		stm.setObject(parameterIndex,new Double(0));
    	}
    	else {
    		stm.setObject(parameterIndex,parameterValue);
    	}    	      
    }   
    
    // parametros indexados por nome    
    public void setParameterValue(String parameterName,Object parameterValue) throws SQLException {
    	int parameterIndex=-1;
    	for (int i=0;i<parameterNames.size();i++) {
    		if (parameterNames.get(i).toString().equalsIgnoreCase(parameterName)) {
    			parameterIndex=(i+1);
    			setParameterValue(parameterIndex,parameterValue);    			
    		}
    	}
    	
    	if (parameterIndex==-1) {
    		throw new IllegalArgumentException("JwideQuery: Invalid Parameter Name "+parameterName);
    	}   	
    }
    
    // gets
    public String getString(String field) throws SQLException {    	
    	return rs.getString(field);
    }
    
    public int getInt(String field) throws SQLException {    	
    	return rs.getInt(field);
    }
    
    public Timestamp getTimestamp(String field) throws SQLException {
    	return rs.getTimestamp(field);       
    }	
    
    public double getDouble(String field) throws SQLException {    	
    	return rs.getDouble(field);
    }	
    
    public void closeResultSet() throws SQLException {    	
    	rs.close();     	
    }
    
    public void closeStatement() throws SQLException {    
    	stm.close();     
    }
    
}