/*
 * Created on 13/09/2004
 */
package jwide;

import java.sql.ResultSetMetaData;
import java.util.Collections;
import java.util.List;

import javax.json.JsonValue;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.sql.Timestamp;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import jwide.JwideFileHandler.JwideFileReader;

/**
 * @author MHRG 
 */

public class JwideDTO {
	 
	private List<String>          columns         = new ArrayList<String>();
	private List<List<Object>>    rows            = new ArrayList<List<Object>>();
	private List<String>          types           = new ArrayList<String>();
	private List<Integer>         sizes           = new ArrayList<Integer>();
	private List<List<String>>    columnAtributes = new ArrayList<List<String>>();
	private List<JwideCalcColumn> calcColumns     = new ArrayList<JwideCalcColumn>();
	
	private static final JwideReflection Reflect = JwideReflection.getInstance();
	private DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
	private PropertyUtilsBean PropUtils=new PropertyUtilsBean();
	//private static JwideUtil jUtil = JwideUtil.getInstance();
	
	private static final String OLD_COLUMN_SUFIX="_old";
	private static final String CSV_SEPARATOR=";"; 
	
	private static final JwideUtil jUtil = JwideUtil.getInstance(); // ConnectionsPool ? est?tico, o jwUtil estava nula
	private JwideJsonReader jSonReader = new JwideJsonReader();
	
	private int currentRow=0;
	
	public JwideDTO() {			
	}
	
	public JwideDTO(String[] columns){
		this.setColumns(columns);
	}
			
	public JwideDTO(JwideDTO fromDTO) {		
		populate(fromDTO);
		afterPopulate();
	}
	
	public JwideDTO populate(java.sql.ResultSet rs) throws Exception { 
		int cols=this.loadMetaData(rs);  

		// incluir os campos	    	    	
		while (rs.next()) {
			ArrayList<Object> row = new ArrayList<Object>(cols);
			// obter todas as colunas da linha corrente
			for (int i=1;i<=cols;i++) {
				row.add(rs.getObject(i));	          	   
			}
			// incluir a coluna
			addRow(row);
		}

		// posicionar na primeira linha
		this.first();
		this.afterPopulate();
		return this;
	}
	
	public JwideDTO populate(JwideDTO DTO) {
		this.populate(DTO,DTO.getColumns());	
		return this;
	}
		
	public JwideDTO populate(JwideDTO DTO,String[] columns) {
		// Limpar as linhas e os tipos correntes
		this.clearRows();		
		this.clearTypes();
		this.clearCalcColumns();
		// Sobrescrever as colunas e atributos
		this.setColumns(columns);
		this.setColumnAttributes(DTO.getColumnAttributes());
		if (this.hasSameColumns(DTO)) this.setCalcColumns(DTO.getCalcColumns());
		// appendar dto
		this.append(DTO,columns);		
		// types
		for (String name:columns) types.add(DTO.getType(name));
		
		this.first();	
		this.afterPopulate();
		return this;
	}
	
	public JwideDTO populate(JwideVO VO){
		JwideDTO Dto = new JwideDTO();
		Dto.setColumns(Reflect.getReadableMethods(VO).toArray(new String[]{}));
				
		List<Object> row = new ArrayList<Object>();
		for (String methodName:Reflect.getReadableMethods(VO)){
			try {
				row.add(PropUtils.getSimpleProperty(VO,methodName));				
			} catch (Exception e){
				row.add(null);
			}
			try {			
				Dto.types.add(PropUtils.getSimpleProperty(VO,methodName).getClass().toString().replace("class ","").replace("java.lang.",""));
			} catch (Exception e){				
				Dto.types.add("UNKNOW");
			}
		}
		
		Dto.addRow(row.toArray(new Object[]{}));
		this.populate(Dto);
		return this;
	}
	
	public JwideDTO populateFromCSV(String fileName) throws Exception {
		JwideDTO dto = new JwideDTO();
		dto.setColumns(this.getColumns());
		
		JwideFileReader reader = new JwideFileHandler().newFileReader(fileName);	
		
		while (reader.hasNextLine()){
			
			String texto=reader.readLine();
			
			if (texto!=null && !texto.trim().equals("")) {

				List<String> content = JwideUtil.getInstance().lineToArray(texto, CSV_SEPARATOR);

				if (dto.getColumns()==null || dto.getColumnCount()==0){
					dto.setColumns(content.toArray(new String[]{}));				
				} else {
					List<Object> row = new ArrayList<Object>();
					for (String c:content) row.add(c.trim());
					dto.addRow(row);
				}
			}					
		}
		reader.close();
		
		return this.populate(dto);
	}
	
	public JwideDTO populate(List<JwideDataObject> values) throws Exception {
		this.clearRows();
		List<String> columns = new ArrayList<>();
		List<List<Object>> rows = new ArrayList<>();
		
		for (JwideDataObject dataObject:values){
			List<Object> row = new ArrayList<>();
			
			for (String c:dataObject.getColumns()){
				if (!columns.contains(c)){
					columns.add(c);
				}
				while (row.size()<columns.size()){
					row.add(null);
				}
				
				Object value = dataObject.getValue(c);
				if (value!=null){
					if (value instanceof JsonValue) {
						if ( ((JsonValue)value).equals(JsonValue.NULL)){
							value = null;
						} else if ( ((JsonValue)value).equals(JsonValue.FALSE)){
							value = false;
						} else if ( ((JsonValue)value).equals(JsonValue.TRUE)){
							value = true;
						} 
					}	
				}
				
				row.set(columns.indexOf(c), value);
			}
			
			rows.add(row);
		}
		
		// normalizar 
		this.setColumns(columns.toArray(new String[]{}));
		for (List<Object> row:rows){
			while (row.size()<columns.size()){
				row.add(null);
			}
			this.addRow(row);
		}
		this.first();
		return this;
	}
	
	public Integer loadMetaData(java.sql.ResultSet rs){
		this.clearRows();
		this.clearCalcColumns();
		columns.clear();
		types.clear();
		sizes.clear();
		ResultSetMetaData rsmd;
		JavaTypesDTO JavaTypes = new JavaTypesDTO(); // n?o mude o contexto
		
		try { 
	       rsmd = rs.getMetaData(); 
	       columns = new ArrayList<String>(rsmd.getColumnCount());
	       for (int i=1;i<=rsmd.getColumnCount();i++) { 
	            columns.add(rsmd.getColumnName(i));	
	            
	            types.add(JavaTypes.getJavaType(rsmd.getColumnTypeName(i),
	            		                        rsmd.getColumnType(i),
	            		                        rsmd.getColumnDisplaySize(i),
	            		                        rsmd.getScale(i)));
	       }
	       return rsmd.getColumnCount();
	    }
	    catch (Exception e) {
	       throw new IllegalArgumentException("JwideDTO.populate.columns: "+e.getMessage());    	
	    }	  
	}
	
	protected void afterPopulate(){
		
	}
	
	public void assign(JwideDTO DTO){
		if (this.getColumnCount()==0){
			this.populate(DTO);
		} else {
			this.append(DTO);
		}
	}
	
	public void append(JwideDTO DTO) {
		this.append(DTO,DTO.getColumns());
	}
	
	public void append(JwideDTO DTO,String[] columns) {		
		int currentRow=DTO.getCurrentRow();
						
		// rows
		DTO.beforeFirst();
		while (DTO.next()) {			
			// DTO e columns s?o iguais
			if (this.hasSameColumns(DTO)){
				this.addRow(DTO.getRow());
			} else {
				List<Object> row = new ArrayList<Object>();
				for (String name:columns) row.add(DTO.get(name));
				this.addRow(row);
			}			
		}
		
		DTO.absolute(currentRow);
		this.last();		
	}
	
	private boolean hasSameColumns(JwideDTO dto){
		List<String> aDtoCols = new ArrayList<String>();
		for (String c:dto.getColumns()) aDtoCols.add(c);
		
		List<String> aCols = new ArrayList<String>();
		for (String c:columns) aCols.add(c);
		return (jUtil.arrayToLine(aDtoCols).equals(jUtil.arrayToLine(aCols)));		
	}
	
	public void setColumns(String... cols) {
		int i=1;
		columns.clear();
		for (String c:cols)	{
			if (c==null) c="unknow";
			if (columns.indexOf(c)<0){
				columns.add(c);		
			} else {
				columns.add(c + (++i));
			}
		}
	}
	
	public String[] getColumns() {
		return columns.toArray(new String[]{});
	}
	
	public void replaceToCalcColumn(JwideCalcColumn calcColumn){
		int i=this.getColumnPos(calcColumn.getColumnName());		
		if (i<0){
			throw new IllegalArgumentException("Column '"+calcColumn.getColumnName()+"' does not exists");			
		}		
		// substtituir o nome original da coluna
		columns.set(i,columns.get(i)+OLD_COLUMN_SUFIX);
		// incluir a coluna calculada
		this.addCalcColumn(calcColumn);
	}
	
	public void addCalcColumn(JwideCalcColumn calcColumn){
		if (columns.indexOf(calcColumn.getColumnName())>=0){
			throw new IllegalArgumentException("Column '"+calcColumn.getColumnName()+"' already exists");			
		}
		calcColumns.add(calcColumn);
		columns.add(calcColumn.getColumnName());		
	}	
	
	public boolean isCalcColumn(String columnName){
		return (getCalcColumnPos(columnName)>=0);
	}
	
	private Object getCalcValue(String columnName){
		Object value=calcColumns.get(getCalcColumnPos(columnName)).get(this);		
		return value;
	}
	
	private void setCalcColumns(List<JwideCalcColumn> calcColumns){
		this.calcColumns=calcColumns;
	}
	
	private List<JwideCalcColumn> getCalcColumns(){
		return calcColumns;
	}
	
	private void clearCalcColumns(){
		this.calcColumns.clear();
	}
	
	public boolean hasCalcColumns(){
		return (this.getCalcColumns().size()>0);
	}
	
	public int getCalcColumnPos(String columnName){
		int i=0;
		for (JwideCalcColumn calcCol:calcColumns) {
			if (calcCol.getColumnName().equals(columnName)) return i;
			i++;
		}
		return -1;
	}
	
	protected List<List<String>> getColumnAttributes(){
		return this.columnAtributes;
	}
	
	protected void setColumnAttributes(List<List<String>> columnsAttributes){
		this.columnAtributes=columnsAttributes;
	}
	
	public void addColumnAttributes(String columnName,String attribName,String attribValue){
		if (!this.hasColumn(columnName)) throw new IllegalArgumentException("JwideDTO.addRow: Ivalid Column Name:"+columnName);
		
		List<String> attrib=new ArrayList<String>();
		attrib.add(columnName);
		attrib.add(attribName);
		attrib.add(attribValue);
		
		this.columnAtributes.add(attrib);
	}
	
	public JwideDTO getColumnAttributes(String columnName){
		JwideDTO dtoAttrib=new JwideDTO();
		dtoAttrib.setColumns(new String[]{"column_name","attribute_name","attribute_value"});
		
		for (List<String> attribs:columnAtributes) {
			if (attribs.get(0).equals(columnName)) {
			   dtoAttrib.addRow(new Object[]{attribs.get(0),attribs.get(1),attribs.get(2)});
			}
		}
		dtoAttrib.first();
		return dtoAttrib;
	}
	
	public boolean hasColumn(String columnName) {
		return this.hasFieldName(columnName);
	}
	
	public boolean hasFieldName(String fieldName) {
		return (this.getColumnIndex(fieldName)>=0);
	}
	
	public void renameColumn(String oldName, String newName) throws Exception {
		int i = this.getColumnIndex(oldName);
		if (i>=0){
			columns.set(i, newName);
		} else {
			throw new IllegalArgumentException("JwideDTO.renameColumn. Invalida ColumnName " + oldName);
		}
	}
	
	public Integer getColumnIndex(String column) {
		if (this.getColumns()==null) return -1;
		int i=0;
		for (String columnName:this.getColumns()) {
			if (columnName.equalsIgnoreCase(column)) return i;
			i++;
		}
		return -1;
	}	
	
	public void addRowAt(Integer index,List<Object> row){
		isElementsEqualsColumns(row);
		rows.add(index, row);
	}
	
	private void isElementsEqualsColumns(List<Object> row) {		
		if (row.size()!=(columns.size()-calcColumns.size())) {
			throw new IllegalArgumentException("JwideDTO.addRow: number of elements is diferent than number of columns");			
		}		
	}
	
	public void addColumn(String columnName) {
		if (this.getRowCount()!=0) {
			throw new IllegalArgumentException("JwideDTO.addColumn: There are rows on DTO");
		}
		
		String[] cols = new String[this.getColumns().length+1];
		for (int i=0;i<this.getColumns().length;i++) {
			cols[i]=this.getColumns()[i];
		}
		
		cols[this.getColumns().length]=columnName;		
		this.setColumns(cols);
	}
	
	public void addRow(Object... row) {
		// remontar a linha em arraylist
		List<Object> r = new ArrayList<Object>();
		for (Object value:row) {
			r.add(value);			
		}
		isElementsEqualsColumns(r);
		rows.add(r);		
	}
	
	public void addRow(List<Object> row) {
		isElementsEqualsColumns(row);
		rows.add(row);		
	}
	
	// copia de todos os valores, incluindo as colunas calculadas
	public List<Object> getValues(){
		List<Object> row = new ArrayList<Object>();		
		for (String c:this.getColumns()) row.add(this.get(c));			
		return row;		
	}
	
	public List<Object> getRow() {
		if (currentRow<1) {
			throw new IllegalArgumentException("JwideDTO.getRow: invalid cursor position: "+currentRow);
		}
		
		return (List<Object>)rows.get((currentRow-1));	
	}	
	
	public void clearRows() {
	   rows.clear();
	   calcColumns.clear();
	   currentRow=0;
	}
	
	private void clearTypes(){
		types.clear();
	}
	
	public void removeRow(int row) {
		this.absolute(row);
		rows.remove(row);
	}
	
	public void removeRow() {		
		this.removeRow(currentRow);
	}
	
	public void updateRow(int row,List<Object> value) {
		this.absolute(row);
		isElementsEqualsColumns(value);
		rows.set(row, value);
	}
	
	public void updateRow(List<Object> value) {
		this.absolute(currentRow);
		isElementsEqualsColumns(value);
		rows.set(currentRow, value);
	}
	
	public void updateColumn(String columnName,Object value){			
	   this.getRow().set(this.getColumnPos(columnName),value);
	}	
	
	public List<Object> getRow(int row) {	
		// Aten??o: sempre passar a row por REFER?NCIA (n?o usar c?pia de valores)
		return (List<Object>)rows.get(row);
	}
	
	public void beforeFirst() {
		currentRow=0;
	}
	
	public boolean isEmpty() {
		return (rows.size()==0);
	}
	
	public void first() {
		if (rows.size()>0) currentRow=1;	    	
		else               currentRow=0;
	}
	
	public boolean next() {
		boolean n=false;
	    if (currentRow<rows.size()) {
	    	currentRow++;
	    	n=true;
	    }	    
	    return n;
    }
	
	public boolean previous() {
		boolean n=false;
	    if (currentRow>1) {
	    	currentRow--;
	    	n=true;
	    }	    
	    return n;
    }
	
	public void absolute(int row) {
		if (row>rows.size()) {
			throw new IllegalArgumentException("JwideDTO.absolute: row "+row+" is bigger than rowcount: "+rows.size());
		}		
		currentRow=row;
    }
	
	public void last() {
		currentRow=rows.size();
	}
	
	public Object get(String columnName) {
		int pos=getColumnPos(columnName);
		if (currentRow==0) return null;
		return this.get(pos);		
	}
	
	public Object get(int columnPos) {
		if (columnPos>getColumnCount()) 
			throw new IllegalArgumentException("JwideDTO.get: column position "+columnPos+" is invalid ");
		if (currentRow==0) return null;
		
		// se for campo calculado
		if (isCalcColumn(columns.get(columnPos))){			
			return getCalcValue(columns.get(columnPos));
		}		
		// se n?o for, descontar a real posi??o dos campos calculados, pois n?o h? valor na row para eles.
		int realPos=-1;
		while (columnPos>=0){
			// se n?o for coluna calculada, a posi??o ? v?lida
			if (!this.isCalcColumn(this.getColumnName(columnPos))) realPos++;
			columnPos--;			
		}
		return getRow().get(realPos);		
	}
	
	private boolean isNullorEmpty(Integer columnPos){
		Object v=this.get(columnPos);
		return (v==null || v.toString().trim().equals(""));
	}
	
	public Object getNotNull(String columnName) {
		// tratamento de null		
		return (get(columnName)!=null?get(columnName):"");		
	}
	
	public Object getNotNull(int columnPos) {
		// tratamento de null		
		return (get(columnPos)!=null?get(columnPos):"");		
	}
	
	public int getSize(String columnName) {
		int pos=getColumnPos(columnName);		
		return Integer.parseInt(sizes.get(pos).toString());
	}
	
	public String getType(int columnPos) {
		if (types.size()>columnPos) return types.get(columnPos);
		else  {
			return new JavaTypesDTO().getJavaTypeDescription(this.get(columnPos)); 
		}
	}
	
	public String getType(String columnName) {
		return this.getType(this.getColumnPos(columnName));
	}
	
	public String getString(String columnName) {
		if (get(columnName)==null) return null;
		return get(columnName).toString();		
	}
	
	public String getString(int columnPos) {
		if (get(columnPos)==null) return null;
		return get(columnPos).toString();		
	}
	
	public Integer getInteger(String columnName) {
		return getInteger(this.getColumnPos(columnName));						
	}
	
	public Integer getInteger(int columnPos) {		
		if (this.isNullorEmpty(columnPos)) return null;
		return new Integer(new Double(this.toNumberFormat(columnPos)).intValue());
	}
	
	public Timestamp getTimestamp(String columnName) {
		return getTimestamp(this.getColumnPos(columnName));		
	}
	
	public Timestamp getTimestamp(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;

		Object value=get(columnPos);
		if (value instanceof java.sql.Timestamp) {
			return ((Timestamp) value);
		} else if (value instanceof java.sql.Date) {			
			return new Timestamp(((java.sql.Date)value).getTime());
		} else {
			return jUtil.strToTimestamp(this.getString(columnPos));
		}
	}
	
	public Byte getByte(String columnName) {		
		return getByte(this.getColumnPos(columnName));		
	}
	
	public Byte getByte(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;	
		Byte b = (Byte) get(columnPos); 
		return b;
	}
	
	public Time getTime(String columnName) {			
		return getTime(this.getColumnPos(columnName));		
	}
	
	public Time getTime(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;		
		Time t = (Time) get(columnPos); 
		return t;
	}
	
	public Float getFloat(String columnName) {			
		return getFloat(this.getColumnPos(columnName));		
	}
	
	public Float getFloat(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;
		return new Float(this.toNumberFormat(columnPos));
	}
	
	public Long getLong(String columnName) {		
		return getLong(this.getColumnPos(columnName));		
	}
	
	public Long getLong(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;		
		return new Long(this.toNumberFormat(columnPos));
	}
	
	public Short getShort(String columnName) {
		return getShort(this.getColumnPos(columnName));		
	}
	
	public Short getShort(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;	
		return new Short(this.toNumberFormat(columnPos));
	}
	
	public BigDecimal getBigDecimal(String columnName) {		
		return getBigDecimal(this.getColumnPos(columnName));		
	}
	
	public BigDecimal getBigDecimal(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;	
		return new BigDecimal(this.toNumberFormat(columnPos));
	}

	public Boolean getBoolean(String columnName) {		
		return getBoolean(this.getColumnPos(columnName));		
	}
	
	public Boolean getBoolean(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;	
		return new Boolean(this.getString(columnPos).trim());		
	}	
	
	public Date getDate(String columnName) {		
		int pos=getColumnPos(columnName);
		Date dt;		
		SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");		
		try {        
	       dt=new java.sql.Timestamp(fmt.parse(this.getString(pos)).getTime());
	    }
	    catch (java.text.ParseException e) {
	       dt=null;          
	    }        
	    return dt;								
	}
	
	public Double getDouble(String columnName) {		
		return getDouble(this.getColumnPos(columnName));						
	}
	
	public Double getDouble(int columnPos) {
		if (this.isNullorEmpty(columnPos)) return null;
		return new Double(this.toNumberFormat(columnPos));						
	} 
	
    private String toNumberFormat(int columnPos){
    	if (this.get(columnPos)==null) return "";    	
    	return this.getString(columnPos).trim().replace(",","");		
	}
	
	public int getColumnPos(String columnName) {
		int pos=-1;
		for (int i=0;i<columns.size();i++) {
			if (columns.get(i).equalsIgnoreCase(columnName)) pos=i;
		}
		if (pos<0) {
			throw new IllegalArgumentException("JwideDTO.getColumnPos: unknow column name: "+columnName);
		}
		return pos;
	}	
	
	public String getColumnName(int columnPos) {		
		if (columnPos<0 || columnPos>columns.size()) {
			throw new IllegalArgumentException("JwideDTO.getColumnName: invalid column position: "+columnPos);
		}
		return columns.get(columnPos).toString();		
	}	
	
	public int getRowCount() {
		return rows.size();
	}
	
	public int getCurrentRow() {
		return currentRow;
	}
	
	public int getColumnCount() {
		return columns.size();
	}
	
	public String toXml() throws Exception {
	   return toXml(getColumns());
	}	

	protected String toXml(String[] columns) throws Exception {	

		DocumentBuilder icBuilder;  	
		icBuilder = icFactory.newDocumentBuilder();
		Document doc = icBuilder.newDocument();
		Element mainRootElement = doc.createElement("root");
		doc.appendChild(mainRootElement); 
		
		if (!this.isEmpty()){
			this.beforeFirst();
			while (this.next()){			
				mainRootElement.appendChild(createChilds(doc,columns,this.getRow().toArray()));
			}
		} else if (this.getColumnCount()>0){
			List<Object> row = new ArrayList<>();
			for (int c=0;c<this.getColumnCount();c++){
				row.add(null);
			}
			mainRootElement.appendChild(createChilds(doc,columns,row.toArray()));
		}		
		
		StreamResult result=null;

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
		DOMSource source = new DOMSource(doc);

		result = new StreamResult(new StringWriter());
		transformer.transform(source, result); 

		return  result.getWriter().toString();
	}
	
	private static Node createChilds(Document doc, String[] columns,Object[] values) {
		Element child = doc.createElement("row");
		
		int i=0;
		for (String c:columns){
			child.appendChild(getElementValue(doc, c, values[i++]));
		}
				
		return child;
	}
	
	private static Node getElementValue(Document doc, String name, Object value) {		
		Element node = doc.createElement(name);
		
		if (value==null){
			// null
		} else if (value instanceof Integer){
			node.appendChild(doc.createTextNode( Integer.toString((Integer)value) ));
		} else if (value instanceof Double){
			node.appendChild(doc.createTextNode( Double.toString((Double)value) ));
		} else if (value instanceof BigDecimal){				
			node.appendChild(doc.createTextNode( String.valueOf ((BigDecimal)value) ));
		} else if (value instanceof Timestamp){				
			node.appendChild(doc.createTextNode( jUtil.formatDate((Timestamp)value)));
		} else if (value instanceof Date){
			node.appendChild(doc.createTextNode( jUtil.formatDate((Date)value)));
		} else if (value instanceof Boolean){
			node.appendChild(doc.createTextNode( Boolean.toString((Boolean)value)));
		} else if (value instanceof Long){
			node.appendChild(doc.createTextNode( Long.toString((Long)value) ));
		} else {
			node.appendChild(doc.createTextNode( value.toString() ));
		}
				
		return node;
	}
	
	public String toHtml(){
		return this.toHtml(this.getColumns());		
	}
	
	public String toHtml(String[] columns){
		StringBuffer html = new StringBuffer();
		html.append("<table><tr>");
		for(String col : columns) {        			
			html.append("<td><b>"+col+"</b></td>");
        }
		html.append("</tr>"); 
		
		this.beforeFirst();
		while (this.next()){
			html.append("<tr>");
			for(String col:columns) {        			
				html.append("<td>"+this.formatColumnToHtml(col)+"</td>");
	        }
			html.append("</tr>");
		}
		html.append("</table>");
        
		return html.toString();        
	}	
	
	private String formatColumnToHtml(String columnName){
		StringBuffer f = new StringBuffer("");
		if (this.get(columnName)==null) f.append("");
		else if (this.get(columnName) instanceof Timestamp) f.append( jUtil.formatDate(this.getTimestamp(columnName)));
		else if (this.isNumber(columnName)) f.append(jUtil.formatFloat("##################.############",this.getDouble(columnName)).replace(".",","));
		else f.append(this.getString(columnName));	
		
		return f.toString();
	}
	
	private boolean isNumber(String columnName){
		if (this.getBoolean(columnName)== null) return false;
		return (this.get(columnName) instanceof Integer || this.get(columnName) instanceof Double || 
				this.get(columnName) instanceof BigDecimal || this.get(columnName) instanceof BigInteger);
	}
	
	public List<JwideDataObject> toDataObject() {
		List<JwideDataObject> values = new ArrayList<>();
		this.beforeFirst();
		while (this.next()){
			JwideDataObject data = new JwideDataObject();
			for (String c:this.getColumns()){
				data.addValue(c, this.get(c));
			}
			values.add(data);
		}
		
		return values;
	}
	
	public String toCsv() {
		return this.toCsv(this.getColumns());		
	}
	
	public String toCsv(String[] cols) {
		StringBuffer csv = new StringBuffer("");

		for (String c:cols){
			if (!csv.toString().equals("")) csv.append(CSV_SEPARATOR);
			csv.append(c);			
		}	
		csv.append("\n");		    

		this.beforeFirst();	 
		while (this.next()) {
			StringBuffer row = new StringBuffer("");
			int p=0;	    	   
			for (String c:cols) {
				if (p>0) row.append(CSV_SEPARATOR);
				if (get(c)!=null) {
					if (jUtil.isNumber(get(c))){
						row.append(jUtil.formatFloat("##################.############",this.getDouble(c)).replace(".",","));
					} else {
						row.append(formatColumnToExport(c));
					}
				}
				p++;       
			}	
			csv.append(row.toString());
			csv.append("\n");
		}  

		return csv.toString();		
	}
		
	public String toJson() {
		return this.toJson(getColumns());
	}		

	public String toJson(String[] cols) {
		/*
		 * {"data" : [
		 *    {"nome":"Jo?o", "nota1":8, "nota2":6, "nota3":10 },
		 *    {"nome":"Maria", "nota1":5, "nota2":9, "nota3":8 },
		 *    {"nome":"Pedro", "nota1":2, "nota2":7, "nota3":5 }
		 *  ]}
		 */
		
		StringBuffer json = new StringBuffer();
		json.append(jSonReader.jSonBegin());
		json.append("\n");		
	
		//return JSonBuilder.build().toString();
	

		this.beforeFirst();
		if (this.getRowCount()>0) {
			while (next()) {
				if (currentRow>1) {
					json.append(",");
					json.append("\n");	
				}
				List<Object> row = new ArrayList<>();
				for (String col:cols) {
					row.add(this.get(col));
				}
				try {
					JwideDataObject data = new JwideDataObject(Arrays.asList(cols), row);
					json.append(jSonReader.toJson(data));
				} catch (Exception e){
                   throw new IllegalArgumentException("JwideDTO.toJson " + e.getMessage());
				}
			}  
		} else if (cols.length>0) { // ha colunas mas n?o h? linhas			
			List<Object> row = new ArrayList<>();
			
			for (int i=0;i<cols.length;i++) {
				row.add(null);
			}
			try {
				JwideDataObject data = new JwideDataObject(Arrays.asList(cols), row);
				json.append(jSonReader.toJson(data));
			} catch (Exception e){
               throw new IllegalArgumentException("JwideDTO.toJson " + e.getMessage());
			}	 
		} 
		json.append(jSonReader.jSonEnd());	

		return json.toString();		
	}
	
	protected Object formatColumnToExport(Integer columnPos){
		if (this.get(columnPos)==null) return null;

		// formatar data 
		if (this.get(columnPos) instanceof Date) return jUtil.formatDate((Date)this.get(columnPos));
		if (this.get(columnPos) instanceof Timestamp) return jUtil.formatDate((Timestamp)this.get(columnPos));
				
		// formatar string
		if (get(columnPos) instanceof String){
			// se n?o for um xml dentro do outro
			if (this.getString(columnPos).indexOf("<")<0 && this.getString(columnPos).indexOf(">")<0) return this.getString(columnPos).replace("&","E");
			else return this.getString(columnPos); //return jUtil.toXmlValidChars(getString(columnPos));
		}
		//if (get(columnPos) instanceof BigDecimal){
	//		return jwUtil.formatFloat("#################0.########", (BigDecimal)this.get(columnPos));			
		//}
		
		// demais	
		return this.get(columnPos);     
	}
	
	
	public Object formatColumnToExport(String columnName){
		return this.formatColumnToExport(this.getColumnPos(columnName));     
	}
		
	public boolean locate(String columnName,Object columnValue) {
		return locate(new String[]{columnName},new Object[]{columnValue});
	}
	
	public boolean locate(String[] columnNames,Object columnValues[]) {
		// localizar o campo
		// numero de colunas n?o bate com numero de valores
		if (columnNames.length!=columnValues.length) {
			throw new IllegalArgumentException("JwideDTO.locate: columns.length is diferent than values.length ");
		}
		
		boolean l=false;		
		int rowNumber=currentRow;
		// percorrer todos os registros
		for (int i=1;i<=rows.size();i++) {
			absolute(i);	
			int qtyEquals=0; // qtd de valores iguais
			
			// pegar todas as colunas
			for (int col=0;col<columnNames.length;col++) {
				// comparar com todos os valores
				// nulo
				if ((get(columnNames[col])==null) && (columnValues[col]==null)) {
				   qtyEquals++;
				}
			    // nao nulo
				if ((get(columnNames[col])!=null) && (columnValues[col]!=null)) {
					// se for numero, usar fun??o para comparar
					if (jUtil==null) throw new IllegalArgumentException("JwideDTO: JwideUtil is not a singleton anymore ???");
					if (jUtil.isNumber(get(columnNames[col]))) {
						if (jUtil.isNumbersEquals(get(columnNames[col]),columnValues[col])) qtyEquals++;					
					}
					// se na for, comparacao direta
					else {
                       if (get(columnNames[col]).equals(columnValues[col])) qtyEquals++;					   	
					}
				}				   
			}
			// se todos os valores baterem, foi encontrado
			if (qtyEquals==columnNames.length) {
				rowNumber=i;
				l=true;		
				break; 
			}
		}
		absolute(rowNumber); // se n?o encontrar, voltar ? posicao original
		return l;		
	}
	
	@SuppressWarnings("unchecked")
	public void sort(String[] columns) {
		 Collections.sort(rows, new JwideDTOSort(this,columns,JwideDTOSort.ASCENDING));
	}
	
	@SuppressWarnings("unchecked")
	public void sortDescending(String[] columns) {
		 Collections.sort(rows, new JwideDTOSort(this,columns,JwideDTOSort.DESCENDING));
	}
	
	public void sort() {
		 sort(this.getColumns());
	}
	
}