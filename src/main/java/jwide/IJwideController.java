package jwide;

public interface IJwideController {

	public final String TO_XML="To Xml";
	public final String TO_JSON="To Json";
	public final String TO_REPORT="To Report";
	public final String TO_EXCEL="To Excel";
	public final String TO_PAGE="To Page";
	public final String TO_TEXT="To Text";
}
