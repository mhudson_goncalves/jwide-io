package jwide;

import java.awt.Color;
import java.io.ByteArrayInputStream;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.rtf.RTFEditorKit;

/**
 * Converte texto no formato RTF para o formato HTML.<br /><br />
 * C?digo baseado no conversor RTF2HTML, utilizado pelo 
 * <a href="http://webcat.sf.net">webcat</a>, desenvolvido
 * pelo <a href="http://xldb.di.fc.ul.pt/">Grupo XLDB</a>, 
 * do <a href="http://www.di.fc.ul.pt/">Departamento de Inform?tica</a> 
 * da <a href="http://www.fc.ul.pt/">Faculdade de Ci?ncias</a> da 
 * <a href="http://www.ul.pt/">Universidade de Lisboa</a>, Portugal.<br />
 * WebCAT foi escrito por <a href="mailto:bmartins@xldb.di.fc.ul.pt">Bruno Martins</a>.<br /><br />
 * 
 * A Classe Rtf2Html, ? licenciada sob os termos da 
 * <a href="http://www.opensource.org/licenses/bsd-license.php">Licen?a BSD</a>.
 * 
 * @author <a href="mailto:bmartins@xldb.di.fc.ul.pt">Bruno Martins</a>
 */
public class Rtf2Html {

    /**
     * Converte texto no formato RTF para o formato HTML.<br /><br />
     * C?digo baseado no conversor RTF2HTML, utilizado pelo 
     * <a href="http://webcat.sf.net">webcat</a>, desenvolvido
     * pelo <a href="http://xldb.di.fc.ul.pt/">Grupo XLDB</a>, 
     * do <a href="http://www.di.fc.ul.pt/">Departamento de Inform?tica</a> 
     * da <a href="http://www.fc.ul.pt/">Faculdade de Ci?ncias</a> da 
     * <a href="http://www.ul.pt/">Universidade de Lisboa</a>, Portugal.<br />
     * WebCAT foi escrito por <a href=" ">Bruno Martins</a>.<br /><br />
     * 
     * A Classe Rtf2Html, ? licenciada sob os termos da 
     * <a href="http://www.opensource.org/licenses/bsd-license.php">Licen?a BSD</a>.
     * 
     * @author <a href="mailto:bmartins@xldb.di.fc.ul.pt">Bruno Martins</a>
     */
		
    private class HTMLStateMachine {
    	public boolean acceptFonts;
        private String fontName;
        private Color color;
        private int size;
        private int alignment;
        private boolean bold;
        private boolean italic;
        private boolean underline;
        private double firstLineIndent;
        private double oldLeftIndent;
        private double oldRightIndent;
        private double leftIndent;
        private double rightIndent;
        private boolean firstLine;

        /**
         * Construtor Padr?o
         */
        HTMLStateMachine() {
            acceptFonts = true;
            fontName = "";
            alignment = -1;
            bold = false;
            italic = false;
            underline = false;
            color = null;
            size = -1;
            firstLineIndent = 0.0D;
            oldLeftIndent = 0.0D;
            oldRightIndent = 0.0D;
            leftIndent = 0.0D;
            rightIndent = 0.0D;
            firstLine = false;
        }

        /**
         * Atualiza estados
         * @param attributeset
         * @param stringbuffer
         * @param element
         */
        public void updateState(
            AttributeSet attributeset,
            StringBuffer stringbuffer,
            Element element) {
            String s = element.getName();
            if (s.equalsIgnoreCase("paragraph")) {
                firstLine = true;
            }
            leftIndent =
                updateDouble(
                    attributeset,
                    leftIndent,
                    StyleConstants.LeftIndent);
            rightIndent =
                updateDouble(
                    attributeset,
                    rightIndent,
                    StyleConstants.RightIndent);
            if (leftIndent != oldLeftIndent || rightIndent != oldRightIndent) {
                closeIndentTable(stringbuffer, oldLeftIndent, oldRightIndent);
            }
            bold =
                updateBoolean(
                    attributeset,
                    StyleConstants.Bold,
                    "b",
                    bold,
                    stringbuffer);
            italic =
                updateBoolean(
                    attributeset,
                    StyleConstants.Italic,
                    "i",
                    italic,
                    stringbuffer);
            underline =
                updateBoolean(
                    attributeset,
                    StyleConstants.Underline,
                    "u",
                    underline,
                    stringbuffer);
            size = updateFontSize(attributeset, size, stringbuffer);
            color = updateFontColor(attributeset, color, stringbuffer);
            if (acceptFonts) {
                fontName = updateFontName(attributeset, fontName, stringbuffer);
            }
            alignment = updateAlignment(attributeset, alignment, stringbuffer);
            firstLineIndent =
                updateDouble(
                    attributeset,
                    firstLineIndent,
                    StyleConstants.FirstLineIndent);
            if (leftIndent != oldLeftIndent || rightIndent != oldRightIndent) {
                openIndentTable(stringbuffer, leftIndent, rightIndent);
                oldLeftIndent = leftIndent;
                oldRightIndent = rightIndent;
            }
        }

        /**
         * Abre tabela identada
         * @param stringbuffer
         * @param d
         * @param d1
         */
        private void openIndentTable(
            StringBuffer stringbuffer,
            double d,
            double d1) {
            if (d != 0.0D || d1 != 0.0D) {
                closeSubsetTags(stringbuffer);
                stringbuffer.append("<table><tr>");
                String s = getSpaceTab((int) (d / 4D));
                if (s.length() > 0) {
                    stringbuffer.append("<td>" + s + "</td>");
                }
                stringbuffer.append("<td>");
            }
        }

        /**
         * Fecha tabela identada
         * @param stringbuffer
         * @param d
         * @param d1
         */
        private void closeIndentTable(
            StringBuffer stringbuffer,
            double d,
            double d1) {
            if (d != 0.0D || d1 != 0.0D) {
                closeSubsetTags(stringbuffer);
                stringbuffer.append("</td>");
                String s = getSpaceTab((int) (d1 / 4D));
                if (s.length() > 0) {
                    stringbuffer.append("<td>" + s + "</td>");
                }
                stringbuffer.append("</tr></table>");
            }
        }

        /**
         * Fecha tags
         * @param stringbuffer
         */
        public void closeTags(StringBuffer stringbuffer) {
            closeSubsetTags(stringbuffer);
            closeTag(alignment, -1, "div", stringbuffer);
            alignment = -1;
            closeIndentTable(stringbuffer, oldLeftIndent, oldRightIndent);
        }

        /**
         * Fecha conjunto de tags
         * @param stringbuffer
         */
        private void closeSubsetTags(StringBuffer stringbuffer) {
            closeTag(bold, "b", stringbuffer);
            closeTag(italic, "i", stringbuffer);
            closeTag(underline, "u", stringbuffer);
            closeTag(color, "font", stringbuffer);
            closeTag(fontName, "font", stringbuffer);
            closeTag(size, -1, "font", stringbuffer);
            bold = false;
            italic = false;
            underline = false;
            color = null;
            fontName = "";
            size = -1;
        }

        /**
         * Fecha Tag
         * @param flag
         * @param s
         * @param stringbuffer
         */
        private void closeTag(
            boolean flag,
            String s,
            StringBuffer stringbuffer) {
            if (flag) {
                stringbuffer.append("</" + s + ">");
            }
        }

        /**
         * Fecha Tag
         * @param color1
         * @param s
         * @param stringbuffer
         */
        private void closeTag(
            Color color1,
            String s,
            StringBuffer stringbuffer) {
            if (color1 != null) {
                stringbuffer.append("</" + s + ">");
            }
        }

        /**
         * Fecha Tag
         * @param s
         * @param s1
         * @param stringbuffer
         */
        private void closeTag(String s, String s1, StringBuffer stringbuffer) {
            if (s.length() > 0) {
                stringbuffer.append("</" + s1 + ">");
            }
        }

        /**
         * Fecha Tag
         * @param i
         * @param j
         * @param s
         * @param stringbuffer
         */
        private void closeTag(
            int i,
            int j,
            String s,
            StringBuffer stringbuffer) {
            if (i > j) {
                stringbuffer.append("</" + s + ">");
            }
        }

        /**
         * Atualiza alinhamentos
         * @param attributeset
         * @param k
         * @param stringbuffer
         * @return
         */
        private int updateAlignment(
            AttributeSet attributeset,
            int k,
            StringBuffer stringbuffer) {
            int i = k;
            Object obj = attributeset.getAttribute(StyleConstants.Alignment);
            if (obj == null)
                return i;
            int j = ((Integer) obj).intValue();
            if (j == 3) {
                j = 0;
            }
            if (j != i && j >= 0 && j <= 2) {
                if (i > -1) {
                    stringbuffer.append("</div>");
                }
                //stringbuffer.append("<div align=\"" + alignNames[j] + "\">");
                i = j;
            }
            return i;
        }

        /**
         * Atualiza cores das fontes
         * @param attributeset
         * @param color3
         * @param stringbuffer
         * @return
         */
        private Color updateFontColor(
            AttributeSet attributeset,
            Color color3,
            StringBuffer stringbuffer) {
            Color color1 = color3;
            Object obj = attributeset.getAttribute(StyleConstants.Foreground);
            if (obj == null)
                return color1;
            Color color2 = (Color) obj;
            if (color2 != color1) {
                if (color1 != null) {
                    stringbuffer.append("</font>");
                }
                if (color2 != null) {
                    stringbuffer.append("<font color=\"#" + makeColorString(color2) + "\">");
                }
            }
            color1 = color2;
            return color1;
        }

        /**
         * Atualiza o nome das fontes
         * @param attributeset
         * @param s2
         * @param stringbuffer
         * @return
         */
        private String updateFontName(
            AttributeSet attributeset,
            String s2,
            StringBuffer stringbuffer) {
            String s = s2;
            Object obj = attributeset.getAttribute(StyleConstants.FontFamily);
            if (obj == null)
                return s;
            String s1 = (String) obj;
            if (!s1.equals(s)) {
                if (!s.equals("")) {
                    stringbuffer.append("</font>");
                }
                stringbuffer.append("<font face=\"" + s1 + "\">");
            }
            s = s1;
            return s;
        }

        /**
         * Atualiza atributos do tipo Double
         * @param attributeset
         * @param d2
         * @param obj
         * @return
         */
        private double updateDouble(
            AttributeSet attributeset,
            double d2,
            Object obj) {
            double d = d2;
            Object obj1 = attributeset.getAttribute(obj);
            if (obj1 != null) {
                d = ((Float) obj1).floatValue();
            }
            return d;
        }

        /**
         * Atualiza o tamanho das fontes
         * @param attributeset
         * @param k
         * @param stringbuffer
         * @return
         */
        private int updateFontSize(
            AttributeSet attributeset,
            int k,
            StringBuffer stringbuffer) {
            int i = k;
            Object obj = attributeset.getAttribute(StyleConstants.FontSize);
            if (obj == null)
                return i;
            int j = ((Integer) obj).intValue();
            if (j != i) {
                if (i != -1) {
                    stringbuffer.append("</font>");
                }
                stringbuffer.append("<font size=\"" + j / 4 + "\">");
            }
            i = j;
            return i;
        }

        /**
         * Atualiza atributos do tipo Boolean
         * @param attributeset
         * @param obj
         * @param s
         * @param flag2
         * @param stringbuffer
         * @return
         */
        private boolean updateBoolean(
            AttributeSet attributeset,
            Object obj,
            String s,
            boolean flag2,
            StringBuffer stringbuffer) {
            boolean flag = flag2;
            Object obj1 = attributeset.getAttribute(obj);
            if (obj1 != null) {
                boolean flag1 = ((Boolean) obj1).booleanValue();
                if (flag1 != flag) {
                    if (flag1) {
                        stringbuffer.append("<" + s + ">");
                    } else {
                        stringbuffer.append("</" + s + ">");
                    }
                }
                flag = flag1;
            }
            return flag;
        }

        /**
         * Cria String com nome de cores
         * @param color1
         * @return
         */
        private String makeColorString(Color color1) {
            String s = Long.toString(color1.getRGB() & 0xffffff, 16);
            if (s.length() < 6) {
                StringBuffer stringbuffer = new StringBuffer();
                for (int i = s.length(); i < 6; i++) {
                    stringbuffer.append("0");
                }
                stringbuffer.append(s);
                s = stringbuffer.toString();
            }
            return s;
        }

        /**
         * Cria identa??o para primeira linha
         * @param s2
         * @return
         */
        public String performFirstLineIndent(String s2) {
            String s = s2;
            if (firstLine) {
                if (firstLineIndent != 0.0D) {
                    int i = (int) (firstLineIndent / 4D);
                    s = getSpaceTab(i) + s;
                }
                firstLine = false;
            }
            return s;
        }

        /**
         * Coleta o atributo spaceTab do objeto HTMLStateMachine
         * @param i
         * @return
         */
        public String getSpaceTab(int i) {
            StringBuffer stringbuffer = new StringBuffer();
            for (int j = 0; j < i; j++) {
                stringbuffer.append("&nbsp;");
            }
            return stringbuffer.toString();
        }

    }

    /**
     * Construtor padr?o
     */
    public Rtf2Html() {
            System.setProperty( "java.awt.headless", "true" );
    }
   
    /**
     * Converte uma String RTF em uma String HTML
     * @param s4
     * @return
     */
    
    /**
     * Converte RTF de um Reader para uma String HTML
     * @param input
     * @return
     * @throws IOException
     */
        /**
     * Converte RTF de um InputStream para uma String HTML
     * @param input
     * @return
     * @throws IOException
     */
    
    /**
     * Converte arquivo RTF em uma String HTML
     * @param input
     * @return
     * @throws IOException
     */
    
    /**
     * Converte RTF de uma URL para uma String HTML
     * @param input
     * @return
     * @throws IOException
     */
   
    /**
     * Converte String RTF em String HTML
     * @param s2
     * @param htmlstatemachine
     * @return
     */
    
    /**
     * Leitor de String
     * @param s
     * @param document
     * @param rtfeditorkit
     */
    private void readString(
        String s,
        Document document,
        RTFEditorKit rtfeditorkit) {

        try {
            ByteArrayInputStream bytearrayinputstream =
                new ByteArrayInputStream(s.getBytes());
            rtfeditorkit.read(bytearrayinputstream, document, 0);

        } catch (Exception exception) {
            exception.printStackTrace( System.out );
            return;
        }
    }

    /**
     * Leitor de Documento
     * @param document
     * @param htmlstatemachine
     * @return
     */
    private String scanDocument(
        Document document,
        HTMLStateMachine htmlstatemachine) {
        String s = "";
        try {
            StringBuffer stringbuffer = new StringBuffer();
            Element element = document.getDefaultRootElement();
            recurseElements(element, document, stringbuffer, htmlstatemachine);
            htmlstatemachine.closeTags(stringbuffer);
            s = stringbuffer.toString();
        } catch (Exception exception) {
            exception.printStackTrace( System.out );
            return s;
        }
        return s;
    }

    /**
     * Leitor de Elementos
     * @param element
     * @param document
     * @param stringbuffer
     * @param htmlstatemachine
     */
    private void recurseElements(
        Element element,
        Document document,
        StringBuffer stringbuffer,
        HTMLStateMachine htmlstatemachine) {
        for (int i = 0; i < element.getElementCount(); i++) {
            Element element1 = element.getElement(i);
            scanAttributes(element1, document, stringbuffer, htmlstatemachine);
            recurseElements(element1, document, stringbuffer, htmlstatemachine);
        }
    }

    /**
     * Leitor de Atributos
     * @param element
     * @param document
     * @param stringbuffer
     * @param htmlstatemachine
     */
    private void scanAttributes(
        Element element,
        Document document,
        StringBuffer stringbuffer,
        HTMLStateMachine htmlstatemachine) {
        try {
            int i = element.getStartOffset();
            int j = element.getEndOffset();
            String s = document.getText(i, j - i);
            javax.swing.text.AttributeSet attributeset = element.getAttributes();
            htmlstatemachine.updateState(attributeset, stringbuffer, element);
            String s1 = element.getName();
            if (s1.equalsIgnoreCase("content")) {            	
                s = s.replaceAll("\\tab", htmlstatemachine.getSpaceTab(4));
                s = s.replaceAll("\\t", htmlstatemachine.getSpaceTab(4));
                s = s.replaceAll("\\n", "<br/>");                            
                s = htmlstatemachine.performFirstLineIndent(s);
                stringbuffer.append(s);
            }
        } catch (BadLocationException badlocationexception) {
            badlocationexception.printStackTrace( System.out );
            return;        }
    }

    /**
     * Transforma um File em um InputStream
     * @param in
     * @return
     * @throws Exception
     */
       /**
     * Transforma um URL em um InputStream
     * @param in
     * @return
     * @throws Exception
     */
   
    /**
     * L? um InputStream
     * @param in
     * @return
     * @throws Exception
     */
       
    /**
     * Converte String RTF para String HTML
     * @param s
     * @return
     */
    public String convertRTFStringToHTML(final String s) {
        HTMLStateMachine htmlstatemachine = new HTMLStateMachine();
        RTFEditorKit rtfeditorkit = new RTFEditorKit();
        DefaultStyledDocument defaultstyleddocument = new DefaultStyledDocument();
        readString(s, defaultstyleddocument, rtfeditorkit);
        return scanDocument(defaultstyleddocument, htmlstatemachine);
    }
    
    /**
     * Converte uma String RTF em uma String HTML, convertendo barras, se necess?rio.
     * @param rtf String RTF a converter
     * @return String HTML convertida.
     */
    public String rtf2html( final String rtf ) {
        System.out.println( "String original: " + rtf);
        String s = rtf.replaceAll("\\\\\\\\", "" + (char)92 + (char)92 );
        return convertRTFStringToHTML( s );
    }
}