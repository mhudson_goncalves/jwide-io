package jwide;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import java.io.ByteArrayInputStream;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class JwideXmlSchemaParse {	
	SchemaFactory FactorySchema =SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
	
	public JwideXmlSchemaParse() {	
	}

	
	public void parseXmlFromSchemaFile(String xmlContent,String xsdFileName) throws Exception {

		SAXParserFactory factory=SAXParserFactory.newInstance();
		Schema schema=FactorySchema.newSchema(new File(xsdFileName));

		factory.setNamespaceAware(true); 
		factory.setValidating(true);
		factory.setSchema(schema);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true); 
		DocumentBuilder parser = dbf.newDocumentBuilder();

		Validator validator = schema.newValidator(); 
		InputSource Isource=new InputSource();
		StreamSource Sstream=new StreamSource();

		Isource.setByteStream(new ByteArrayInputStream(xmlContent.getBytes()));
		Sstream.setInputStream(new ByteArrayInputStream(xmlContent.getBytes()));

		parser.parse(Isource);
		validator.validate(Sstream);
	}


}
