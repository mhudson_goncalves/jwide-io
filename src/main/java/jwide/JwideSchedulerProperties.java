package jwide;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JwideSchedulerProperties extends JwideDTO {
	
	protected static final Long SECONDS_SCHEDULER = Long.valueOf(60);
	
	private JwideFileHandler fh = new JwideFileHandler();	
	private JwideJsonReader jsonReader = new JwideJsonReader();
	private static final String PROPERTIES_FILE_NAME="jwide-scheduler.json";
	
    protected static final String DATE_FORMAT="yyyy/MM/dd HH:mm:ss";   
	
	protected static final String HOST_IP="host_ip";
	protected static final String HOST_NAME="host_name";
	protected static final String CONTEXT="context";
	protected static final String RUN_ONCE="run_once";
	protected static final String HOUR="hour";
	protected static final String DAY_OF_WEEK="day_of_week";
	protected static final String DAY_OF_MONTH="day_of_month";   
	protected static final String MONTH="month";
	protected static final String INTERVAL="interval"; // intervalo em minutos: 5
	protected static final String START_DATE="start_date";
	protected static final String END_DATE="end_date";
	protected static final String START_TIME="start_time";
	protected static final String END_TIME="end_time";
		
	protected static final String CLASS="class";
	protected static final String METHOD="method";
	protected static final String METHOD_PARAM="param"; // Injetar uma ?nica string com o conte?do (se houver)
		
	private static final List<Scheduler> SchedulerPool = Collections.synchronizedList(new ArrayList<Scheduler>());	

	public JwideSchedulerProperties(){		
	}
	
	/*
	 Crie uma classe extendendo JwideSchedulerProperties
	 Crie um m?todo para adicionar o Scheduler e execut?-lo via action, schedulado, etc:
	 
	 public void metodoAddAgenda() {
	 
	    super.addScheduler(
	    		            new Scheduler().
	    		                add(HOUR_TAG,"17:26,17:28,17:30,18:17").
	    		                add(CLASS,"teste.SchedulerTeste").
	    		                add(METHOD,"gravarArquivoDinamico")
	    		          );
     }	    		          
	 */
	protected void addScheduler(Scheduler Scheduler){
		// adicionar somente se n?o houver no pool, evitando duplo insert
		if (!SchedulerPool.contains(Scheduler)){
			SchedulerPool.add(Scheduler);
		}
	}
	
	protected void removeScheduler(Scheduler Scheduler){
		// remover somente se houver no pool
		if (SchedulerPool.contains(Scheduler)){
			SchedulerPool.remove(Scheduler);
		}
	}
	
	protected List<Scheduler> retrieveSchedulers(){
		List<Scheduler> schedulers = new ArrayList<Scheduler>();

		for (Scheduler s:SchedulerPool){			
			schedulers.add(s);			
		}

		return schedulers;
	}
		
	protected List<Scheduler> retrieveSchedulers(String className,String methodName){
		List<Scheduler> schedulers = new ArrayList<Scheduler>();
		
		for (Scheduler s:SchedulerPool){		
			if (s.contains(className,methodName)){
               schedulers.add(s);				
			}
		}
			
		return schedulers;
	}
	
	protected Integer indexOf(Scheduler scheduler){
		for (Scheduler s:SchedulerPool){		
			if (s.toString().equals(scheduler.toString())) return SchedulerPool.indexOf(s);
		}
		return -1;
	}
	
	protected void readProperties() throws Exception {
		
		if (this.isSchedulerFileCreated())	{			
			List<JwideDataObject> values = jsonReader.parseFromFile(this.getPropertiesFileName());
			for (JwideDataObject value:values){
				if (value!=null && value.getColumns()!=null && value.getColumns().contains(CLASS)){
					Scheduler scheduler = new Scheduler();
					for (String c:value.getColumns()){
						scheduler.add(c, value.getValue(c).toString());
					}
					SchedulerPool.add(scheduler);
				}
			}
		}		
	}
	
	protected boolean isSchedulerFileCreated() {
		return fh.isFileCreated(this.getPropertiesFileName());
	}
	
	private String getPropertiesFileName() {		
		return JwideUtil.getInstance().getJwideResourcePath(PROPERTIES_FILE_NAME);
	}	
	
	// Inclus?o de Scheduler em tempo de execu??o \\
	protected class Scheduler {
		
		private List<String> cols = new ArrayList<>();
		private List<Object> row  = new ArrayList<>();
		private Timestamp lastExecution = JwideUtil.getInstance().getTodayTimestamp();
		private int runCount = 0;
		
		public Scheduler(){				
		}		
	
		public Scheduler add(String tag, String value){
			
			int i= cols.indexOf(tag.toLowerCase());
			if (i<0){
				cols.add(tag.toLowerCase());
				row.add(value);
			} else {
				row.set(i,value);
			}
			return this;
		}
		
		public String getString(String column){
			return row.get(cols.indexOf(column)).toString();
		}
		
		protected boolean exists(String column){
			return (cols.contains(column));
		}
		
		protected boolean contains(String className,String methodName){
			return (
					 cols.contains(CLASS) &&
					 cols.contains(METHOD) &&
					 row.get(cols.indexOf(CLASS)).equals(className) &&
					 row.get(cols.indexOf(METHOD)).equals(methodName)
				   );
					 
		}	
		
		protected Timestamp getLastExecution(){
			return this.lastExecution;
		}
		
		protected void updateExecute(){
			this.lastExecution = JwideUtil.getInstance().getTodayTimestamp();
			this.runCount++;
		}	
		
		protected int getRunCount(){
			return this.runCount;
		}
		
		public String toString(){
			int i=-1;
			StringBuffer content = new StringBuffer();
			for (String col:cols){
				content.append(col);
				content.append("|");
				content.append(row.get(++i));
				content.append("|");
			}			
			return content.toString();
		}
	}
	
}
