/*
 * JwideUtil.java
 *
 * Created on 21 de Julho de 2004, 10:17
 */

package jwide;

import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;

import java.text.DecimalFormatSymbols;
import java.net.InetAddress;

/**
 * 
 * @author  MHRG
 */

public class JwideUtil {
	
	private static JwideUtil instance;
	
	public final String SECOND="seconds";
	public final String MINUTE="minute";
	public final String HOUR="hour";	
	public final String DAY="day";
	public final String DAY_OF_WEEK="dayOfWeek";
	public final String MONTH="month";
	public final String YEAR="year";
	
	private static final String DATE_FORMAT="dd/MM/yyyy HH:mm:ss.SSS";
	private static final String DATE_FORMAT_OTHER="yyyy-MM-dd HH:mm:ss.SSS";
	
	//private static final String TIME_ZONE="GMT-3:00";

	private static final Pattern tokenPattern = Pattern.compile("\\|\\$(\\w+)\\$\\|"); //|$61$|
	private static final String BEGIN_TOKEN="|$";
	private static final String END_TOKEN="$|";
	private static final char[] VALID_CHARS={' ','.',',','{','}','!','@','#','$','%','&','*','(',')','-','_','=','+','[',']',':',';','?','\\','/'};
	
	private static final String Q = "" +'"';
	
	private static final List<String> INVALID_XML_CHARS=Arrays.asList("&","<",">");
	private static final List<String> VALID_XML_CHARS  =Arrays.asList("&amp;","&lt;","&gt;");
	
	protected static final String JNDI_LOOKUP_PREFIX = "java:comp/env/"; 
	                                              
	private DecimalFormatSymbols decFormatSymbols = new DecimalFormatSymbols();   
	private InitialContext initialContext = null;
	private JwideProperties properties = null;
    
    /** Creates a new instance of JwideUtil */
    private JwideUtil() {  
    	decFormatSymbols.setZeroDigit('0');   
    	decFormatSymbols.setDecimalSeparator('.');   
    	decFormatSymbols.setMonetaryDecimalSeparator('.');   
    	decFormatSymbols.setDigit('#');   
    	decFormatSymbols.setGroupingSeparator(',');
    	//TimeZone.setDefault(TimeZone.getTimeZone(TIME_ZONE)); // horario de ver?o 
    	//JwideTimeZone.getInstance(); // horario de ver?o
    }
    
    
    public synchronized static JwideUtil getInstance() {
	    if( instance == null ) {
	        instance = new JwideUtil();
	    }
	    return instance;
	}
    
    // variaveis no context.xml: <Environment name="id-recurso" value="10" type="java.lang.Integer" override="false"/>   
    public Object getLookupValue(String value) throws Exception {
    	if (initialContext==null){
    		initialContext = new InitialContext();
    	}
    	return initialContext.lookup(JNDI_LOOKUP_PREFIX + value);
    }
    
    public String getProperty(String name) throws Exception {
    	if (properties==null) {
    		properties = new JwideProperties(this.getLookupValue("jwide.properties.folder").toString());
    	}
    	return properties.getProperty(name);   	
    }
       
    // transforma uma string em timestamp
    public java.sql.Timestamp strToTimestamp(String dateStr) {
    	if (dateStr==null) return null;
    	
    	StringBuffer newDate = new StringBuffer(dateStr.trim());
    	if (newDate.toString().length()==10) newDate.append(" 00:00:00.000");
    	if (newDate.toString().length()==16) newDate.append(":00.000");
    	if (newDate.toString().length()==19) newDate.append(".000");  
    	try{
    		return new Timestamp(new SimpleDateFormat(this.extractDateFormat(dateStr)).parse(newDate.toString()).getTime());
    	} catch (Exception e){    		
    	}
    	
    	return null;   	
    }
    
    private String extractDateFormat(String value){
    	if (value==null){
    		return DATE_FORMAT;
    	}

    	String f=null;
    	if (value.length()>=10){
    		if ((value.substring(2, 3) + value.substring(5, 6)).equals("//")) { // dd/mm/yyyy
    			f = DATE_FORMAT;
    		} else if ((value.substring(4, 5) + value.substring(7, 8)).equals("--")) { // yyyy-mm-dd
    			f = DATE_FORMAT_OTHER;
    		}
    	}

    	if (f!=null){
    		return f;
    	} else {
    		return DATE_FORMAT;
    	}
    }
    
    public java.sql.Timestamp strToTimestamp(String DtString,String dateFormat) {
    	if (DtString==null) return null;      

    	SimpleDateFormat DtFmt = new SimpleDateFormat(dateFormat);
    	DtString = DtString.trim();      
    	java.sql.Timestamp Dt=null;

    	try {        
    		Dt=new java.sql.Timestamp(DtFmt.parse(DtString).getTime());
    	}
    	catch (java.text.ParseException e) {
    		Dt=null;
    	}                      
    	return Dt;
    }   
    
    public Date getToday() {
        return new java.util.Date();            
    }
    
    public java.sql.Timestamp getTodayTimestamp() {
        return new java.sql.Timestamp(getToday().getTime());
    }
    
    public java.sql.Timestamp getSimpleTodayTimestamp(){
    	return this.strToTimestamp(this.formatDate(this.getTodayTimestamp()).substring(0,10));
    }
    
    public java.sql.Timestamp getSimpleTimestamp(Timestamp date){
    	if (date==null) return null;
    	return this.strToTimestamp(this.formatDate(date).substring(0,10));
    }
    
    public Date getFirstDate(Date date) {
    	GregorianCalendar cal = new GregorianCalendar();
    	cal.setTime(date);
    	cal.set(Calendar.DAY_OF_MONTH,01);    	
    	
    	return cal.getTime();    
    }
    
    public Date getLastDate(Date date) {
    	GregorianCalendar cal = new GregorianCalendar();
    	cal.setTime(date);
    	cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH));    	
    	
    	return cal.getTime();    
  	}
    
    public String formatDate(Timestamp data) {
    	
    	if (data==null) return "";
        SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT);
        String d=fmt.format(data);
        if ((d.length()==23) && (d.substring(11,23).equals("00:00:00.000"))) {        
        	d=d.replaceAll(" 00:00:00.000","");        	
        } 
        
        return d;    	
    }
    
    public String getDayOfWeek(int dayOfWeek) {
    	switch(dayOfWeek){   
		case 01:   
			return "DOMINGO";
		case 02:   
			return "SEGUNDA-FEIRA";			
		case 03:   
			return "TERCA-FEIRA";			
		case 04:   
			return "QUARTA-FEIRA";		  
		case 05:   
			return "QUINTA-FEIRA";			
		case 06:   
			return "SEXTA-FEIRA";			
		case 07:   
			return "SABADO";			
		}
		return "UNKNOW";
    }
    
    public int datePart(String Type,Timestamp data) {   
    	Calendar calendario = new GregorianCalendar();
    	calendario.clear();
    	calendario.setTime(data);
    	
    	int partDate=0;
    	    	
    	if (Type.equals(DAY_OF_WEEK)) partDate=calendario.get(Calendar.DAY_OF_WEEK);  
    	if (Type.equals(DAY))         partDate=calendario.get(Calendar.DAY_OF_MONTH);
    	if (Type.equals(SECOND))      partDate=calendario.get(Calendar.SECOND);
    	if (Type.equals(MINUTE))      partDate=calendario.get(Calendar.MINUTE);
    	if (Type.equals(HOUR))        partDate=calendario.get(Calendar.HOUR_OF_DAY);
    	if (Type.equals(MONTH))       partDate=calendario.get(Calendar.MONTH);
    	if (Type.equals(YEAR))        partDate=calendario.get(Calendar.YEAR);

    	return partDate;
    }
    
    public boolean isLastDayOfMonth(Timestamp date){
    	int actualDay=this.datePart(DAY, date);
    	int lastDay=this.datePart(DAY,this.lastTimestamp(date));
    	return (actualDay==lastDay);
    }
    
    public Timestamp dateAdd(String Type,int Qty,Timestamp data) {
    	Calendar calendario = new GregorianCalendar();
    	calendario.clear();
    	calendario.setTime(data);
    	
    	if (Type.equals(DAY_OF_WEEK)) calendario.add(Calendar.DAY_OF_WEEK,Qty);    
    	if (Type.equals(DAY))         calendario.add(Calendar.DAY_OF_MONTH,Qty);         
    	if (Type.equals(SECOND))      calendario.add(Calendar.SECOND,Qty);
    	if (Type.equals(MINUTE))      calendario.add(Calendar.MINUTE,Qty);    	
    	if (Type.equals(HOUR))        calendario.add(Calendar.HOUR,Qty);
    	if (Type.equals(MONTH))       calendario.add(Calendar.MONTH,Qty);
    	if (Type.equals(YEAR))        calendario.add(Calendar.YEAR,Qty);
    	
    	return new Timestamp(calendario.getTimeInMillis());
    }    
    
    public Timestamp dateAdd(String Type,int Qty,Timestamp data,int maxDayOfMonth){
    	Timestamp newDate=this.dateAdd(Type, Qty, data);
    	// se somar um mes em 28/02 = 28/03, mas o usuario quer 31/03. ent?o: passar o 31 em maxDayOfMonth 
    	int actualDay=this.datePart(DAY,newDate);  
    	if (actualDay<maxDayOfMonth){
    		int lastDay=this.datePart(DAY,this.lastTimestamp(newDate));
    		if (maxDayOfMonth>lastDay) newDate=this.dateAdd(DAY,(lastDay-actualDay),newDate);
    		else                       newDate=this.dateAdd(DAY,(maxDayOfMonth-actualDay),newDate);
    	}
    	return newDate;
    }
    
    public Timestamp lastTimestamp(Timestamp date){
    	Calendar calendario = new GregorianCalendar();
    	calendario.setTimeInMillis(this.getLastDate(date).getTime());     
    	return new Timestamp(calendario.getTimeInMillis());     
    }
     
    public Timestamp firstTimestamp(Timestamp date){
    	Calendar calendario = new GregorianCalendar();
    	calendario.setTimeInMillis(this.getFirstDate(date).getTime());     
    	return new Timestamp(calendario.getTimeInMillis());     
    }
    
    public String formatDate(Date data) {
    	if (data==null) return "";
    	return formatDate(new java.sql.Timestamp(data.getTime()));
    }
    
    public String formatDate(java.sql.Date data) {
    	if (data==null) return "";
    	return formatDate(new java.sql.Timestamp(data.getTime()));
    }       
    
    public String formatDate(String format,Date data) {
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        return fmt.format(data);
    }     
    
    public String formatFloat(String format,Double valor) {
    	if (valor==null) return "";
    	return formatFloat(format,valor.doubleValue());
    }
    public String formatFloat(String format,double valor) {    	
    	NumberFormat fmt=new DecimalFormat(format,decFormatSymbols);    	
    	return fmt.format(valor);
    }
    
    public String formatFloat(String format,Integer valor) {
    	if (valor==null) return "";
    	return formatFloat(format,valor.intValue());    	
    }
    public String formatFloat(String format,int valor) {    	
    	NumberFormat fmt=new DecimalFormat(format,decFormatSymbols);
    	return fmt.format(valor);
    }
    public String formatFloat(String format,BigDecimal valor) {
    	if (valor==null) return "";
    	NumberFormat fmt=new DecimalFormat(format,decFormatSymbols);
    	return fmt.format(valor);
    }
    
    public boolean isNumber(Object value) {
    	try  {
    	   Double.valueOf(value.toString().trim()).doubleValue();
    	   return true;
    	}
    	catch (Exception e) {    	   
    	   return false;
    	}
    }
    
    public boolean isNumbersEquals(Object number1,Object number2) {
    	boolean e=(isNumber(number1) && isNumber(number2));
    	
    	if (e) {
    		try {
    		   double v1=Double.valueOf(number1.toString().trim()).doubleValue();
    		   double v2=Double.valueOf(number2.toString().trim()).doubleValue();
    		   e=(v1==v2);
    		}
    		catch (Exception ecp) {
    		   e=false;
    		}
    	}
    	
    	return e;
    }
    
    public long getDateDif(String Type,Date dtFirst,Date dtLast) {
    	
        long diffMillis = dtLast.getTime()-dtFirst.getTime();     
        
        long diff=-1;        
        
        if (Type.equals(SECOND)) {
           diff = diffMillis/(1000); // 604800
        }        

        if (Type.equals(MINUTE)) {
           diff = diffMillis/(60*1000); // 10080
        }        
        
        if (Type.equals(HOUR)) {
           diff = diffMillis/(60*60*1000); // 168
        }        
        
        if (Type.equals(DAY)) {
           diff = 0;
           int factor=1;
           
           Timestamp dt1=this.getSimpleTimestamp(new Timestamp(dtFirst.getTime()));
           Timestamp dt2=this.getSimpleTimestamp(new Timestamp(dtLast.getTime()));
               
           // data final menor, inverter
           if (dt2.before(dt1)){
        	   dt2=this.getSimpleTimestamp(new Timestamp(dtFirst.getTime()));
               dt1=this.getSimpleTimestamp(new Timestamp(dtLast.getTime()));
               factor=-1;
           }
           while (dt1.before(dt2)){
              dt1=this.dateAdd(this.DAY,01,dt1);
              diff++;
           }          
           
           diff=(diff*factor);
        }        
    	return diff;
    }
    
    public List<String> getElementsByRegex(String expression,String regex) {
       Pattern parameter = Pattern.compile(regex+"(\\w+)");    	
       Matcher m = parameter.matcher(expression);       
       List<String> parameterNames = new ArrayList<String>();
   
       while (m.find()) {
          parameterNames.add(m.group(1));         
       }
       return parameterNames;	
    }     
   
    public boolean isJsonContentType(String contentType){
    	if (contentType!=null) for (String c:lineToArray(contentType,";")) {
    		if (JwideConsts.JSON_CONTENT_TYPES.contains(c.trim().toLowerCase())){
    			return true;
    		}
    	}
    	return false;
    }
    
    public boolean isXmlContentType(String contentType){
    	if (contentType!=null) for (String c:lineToArray(contentType,";")) {
    		if (JwideConsts.XML_CONTENT_TYPES.contains(c.trim().toLowerCase())){
    			return true;
    		}
    	}
    	return false;
    }   
    
    public String quote(Object value){
    	return (Q+ (value!=null?value:"") +Q);
    }
    
    public String formatStackTraceInformation (Exception e) {
    	StringBuffer Err = new StringBuffer();
    	
    	Throwable ex = e.getCause();
    	if (ex==null){
    	    Err.append(e.getMessage());
    	    Err.append(e.toString());
    	    return Err.toString();
    	}
    	ex.printStackTrace();    	
    	
    	Err.append("Error Message:\n");
    	Err.append(ex.getMessage());    	
    	Err.append("\n==============================================================");
    	
    	Err.append("The stack according to printStackTrace():\n");    	
    	Err.append("");
    	
    	StackTraceElement[] stackElements = ex.getStackTrace();
    	    	
    	Err.append("The " + stackElements.length +
    			" element" +
    			((stackElements.length == 1) ? "": "s") +
    	" of the stack trace:\n");
    	
    	
    	for (int lcv = 0; lcv < stackElements.length; lcv++) 	{
    		Err.append("\nFilename: " +
    				stackElements[lcv].getFileName());
    		Err.append("\nLine number: " +
    				stackElements[lcv].getLineNumber());
    		
    		String className = stackElements[lcv].getClassName();
    		
    		Err.append("\nFull class name: " + className);    		
    		
    		Err.append("\nMethod name: " +stackElements[lcv].getMethodName());
    		Err.append("\nNative method?: " +	stackElements[lcv].isNativeMethod());
    		
    		Err.append("\ntoString(): " +stackElements[lcv].toString());
    		Err.append("\n==============================================================");
    	}
    	Err.append("");
    	
    	return Err.toString();
    }   
   
    public ArrayList<String> lineToArray(String line) {
    	return lineToArray(line,",");
    }
    
    public ArrayList<String> lineToArray(String line,String separator) {
    	// transforma codigo,descricao em arrayy 0-codigo,1-descricao
    	ArrayList<String> lines = new ArrayList<String>();
    	if (line!=null) {
    		StringBuffer lineComponent = new StringBuffer("");			
    		for (int i=0;i<line.length();i++) {
    			if (line.substring(i,i+1).equals(separator)) {
    				lines.add(lineComponent.toString());
    				lineComponent.setLength(0);
    			}
    			else lineComponent.append(line.substring(i,i+1));		
    		}
    		if (lineComponent.length()>0) lines.add(lineComponent.toString());
    	}
    	
    	return lines;
    }   
    
    public String arrayToLine(List<String> arrayL) {    	
    	StringBuffer line = new StringBuffer("");
    	if (arrayL!=null) {
    		int i=0;
    		for (String c:arrayL) {
    			if (i>0) line.append(",");
    			line.append(c);
    			i++;
    		}    		
    	}    	
    	return line.toString();    	
    }
    
    public String getMyIP() {
    	try {    		
    		return InetAddress.getLocalHost().getHostAddress();
    	} catch (Exception e) {
    		return "";
    	}
    }
    
    public String getMyHostName() {
    	try {    		
    		return InetAddress.getLocalHost().getHostName();
    	} catch (Exception e) {
    		return "";
    	}
    }
    
    public Object decodeCharacter(Object value) {
		if (value==null) return null;
		if (!(value instanceof String)) return value;

		Matcher tokenMatcher = tokenPattern.matcher(value.toString());
		StringBuffer newValue = new StringBuffer();

		while (tokenMatcher.find()) {	   
			try {
				int ascii=new Integer(tokenMatcher.group(1).trim()).intValue();
				tokenMatcher.appendReplacement(newValue,((char)ascii)+"");
			} catch (Exception e){
			}
		}
		tokenMatcher.appendTail(newValue);
		return newValue.toString();
	}
    
    public Object encondeCharacter(Object value) {
		if (value==null) return null;
		if (!(value instanceof String)) return value;
 
		StringBuffer newValue = new StringBuffer();
		boolean validChar;
		for (char c:value.toString().toCharArray()) {
			validChar=false;
			if (!validChar) validChar=(c>='a' && c<='z');
			if (!validChar) validChar=(c>='A' && c<='Z');
			if (!validChar) validChar=(c>='0' && c<='9');
			
			if (!validChar) for (char vc:VALID_CHARS) if (!validChar) validChar=(c==vc);
			
			if (!validChar) newValue.append(BEGIN_TOKEN+((int)c)+END_TOKEN);
			else            newValue.append(c);
		}
		return newValue.toString();
	}
    
    public String toXmlValidChars(String value){
    	if (value==null) return null;
    	
    	int index=-1;
    	for (String c:INVALID_XML_CHARS) {
    		index=value.indexOf(c);
    		if (index>=0) break;
    	}
    	if (index==-1) return value;    	
    	
    	StringBuffer newValue = new StringBuffer();
    	for (char c:value.toCharArray()){
    	    index=INVALID_XML_CHARS.indexOf(c+"");
    	    if (index>=0) newValue.append(VALID_XML_CHARS.get(index));
    	    else          newValue.append(c);    	    
    	}
    	return newValue.toString();
    }    
   
    // /myapp
    public String getContextPath(){    	
    	return JwideStoreRealPath.getContextPath();
    }
    
    public String getRawContextPath(){
    	return this.getContextPath().replace("/","");
    }

    public String getResourcePath(String path) {	

    	try {
    		String resource=
    				this.getClass().getResource("/").toURI().
    				toString().    				
    				replace("file:/","/").
    				replace("//","/").
    				replace("%20"," ");

    		return resource.
    				substring(0,resource.indexOf("WEB-INF"))+
    				"META-INF"+
    				(path.indexOf("/")==0?path:"/" +path);
    	} catch (Exception e){
    		return null;
    	}  
    }
    
    ///C:/tomcat/webapps/project-io/META-INF/jwide/ + resource
    public String getJwideResourcePath(String resource){
    	return this.getResourcePath("/jwide/" + resource);
    }     
      
}
