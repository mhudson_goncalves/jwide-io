package jwide;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

public class JwideExcelHandler {

    public JwideExcelHandler(){

    }

    public JwideDTO loadFromFile(String fileName) throws Exception{
        return this.loadFromFile(fileName,1); // 1a linha 
    }
    
    public JwideDTO loadFromFile(String fileName,int headerLine) throws Exception{

    	JwideDTO SheetDTO = new JwideDTO();
    	FileInputStream fileInputStream = new FileInputStream(fileName);

    	try (HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream)){
    		HSSFSheet worksheet = workbook.getSheetAt(0);
    		Iterator<Row> iterator = worksheet.iterator();

    		int lineCount=0;
    		int unknow=0;
    		while (iterator.hasNext()) {
    			List<Object> row = new ArrayList<Object>(); 
    			lineCount++;

    			Row currentRow = iterator.next();
    			Iterator<Cell> cellIterator = currentRow.iterator();

    			while (cellIterator.hasNext()) {
    				Cell currentCell = cellIterator.next();    

    				if (lineCount==1){
    					if (currentCell.getCellTypeEnum() == CellType.BLANK){
    						row.add("unknow" + (++unknow));
    					} else if (currentCell.getCellTypeEnum() == CellType._NONE){
    						row.add("unknow" + (++unknow));
    					} else {
    						row.add(currentCell.getStringCellValue());
    					}            		
    				} else {

    					if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
    						row.add(currentCell.getNumericCellValue());
    					} else if (currentCell.getCellTypeEnum() == CellType.BOOLEAN){
    						row.add(currentCell.getBooleanCellValue());
    					} else if (currentCell.getCellTypeEnum() == CellType.BLANK){
    						row.add(null);
    					} else if (currentCell.getCellTypeEnum() == CellType._NONE){
    						row.add(null);
    					} else {
    						row.add(currentCell.getStringCellValue());
    					}
    				}
    			}

    			if (lineCount==1){
    				List<String> cols = new ArrayList<>();
    				for (Object o:row){
    					cols.add(o.toString());
    				}
    				SheetDTO.setColumns(cols.toArray(new String[]{}));
    			} else {
    				if (row.size()>SheetDTO.getColumnCount()) throw new Exception("Erro na linha:" + (lineCount) + "\n" + " Linha possui mais colunas que o cabe?alho!" );
    				// Se a linha tiver menos colunas do que o cabe?alho, completa. 
    				while (row.size()<SheetDTO.getColumnCount()) {
    					row.add(null);
    				}
    				SheetDTO.addRow(row);
    			}
    			workbook.close(); 
    		}

    	} catch (Exception e){
    		throw new Exception(e.getMessage());
    	} 

    	return SheetDTO;
    }

}