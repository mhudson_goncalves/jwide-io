package jwide;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtilsBean;

public class JwideReflection {
	
	private static JwideReflection instance=null;	
	private PropertyUtilsBean PropUtils=new PropertyUtilsBean();
	
	private JwideReflection(){}
	
	public synchronized static JwideReflection getInstance() {
		if (instance==null ) instance = new JwideReflection();
		return instance;
	}
	
	public Object invokeMethod(Object objeto,String metodo) throws Exception {			
		Method meth = objeto.getClass().getMethod(metodo,new Class[0]);		
		return meth.invoke(objeto,new Object[0]);
	}

	public Object invokeMethod(Object objeto,String metodo,Class<?> paramTypes,Object... Objects) throws Exception {	
		Method meth = objeto.getClass().getMethod(metodo,paramTypes);	
		meth.setAccessible(true); // se for private ou protected, setar para acessivel
		return meth.invoke(objeto,Objects);
	}
   
	public Object invokeDeclaredMethod(Object objeto,String metodo,Object[] Objects) throws Exception {			
		Method meth = objeto.getClass().getDeclaredMethod(metodo,new Class[0]);	
		meth.setAccessible(true); // se for private ou protected, setar para acessivel
		return meth.invoke(objeto,Objects);
	}
	// pega os metodos da classe pai e publicos da classe filha
	public List<String> getMethods(Object objeto) {
		List<String> Metodos = new ArrayList<>();	
		for (Method m:objeto.getClass().getMethods()) {
			Metodos.add(m.getName());
		}
		return Metodos;		
	}
	// pega somente os metodos da classe atual, inclusive os protected e private 
	public List<String> getDeclaredMethods(Object objeto) {
		List<String> Metodos = new ArrayList<>();		
		for (Method m:objeto.getClass().getDeclaredMethods()) {
			Metodos.add(m.getName());
		}
		return Metodos;		
	}
	
	public ArrayList<String> getReadableMethods(Object objeto) { 
		ArrayList<String> fields = new ArrayList<String>();
		// pegar as propriedades da classe (getters)
		PropertyDescriptor[] PropDesc=PropUtils.getPropertyDescriptors(objeto);
		for (int i=0; i< PropDesc.length; i++) {
			String propertyName = PropDesc[i].getName();			
			if ("class".equals(propertyName)) {
				continue; // Se for uma classe ocasionara em erro					
			}		      			
			// se a propriedade ? de leitura (getXXX)
			if (PropUtils.isReadable(objeto,propertyName)) {
				fields.add(propertyName);
			}
		}
		return fields;
	}
	
	public Class<?> getPropertyReturnClass(Object objeto,String propertyName) throws Exception {
		return PropUtils.getPropertyDescriptor(objeto,propertyName).getPropertyType();
	}
	
	public List<String> getWriteableMethods(Object objeto) { 
		List<String> fields = new ArrayList<>();
		// pegar as propriedades da classe (getters)
		PropertyDescriptor[] PropDesc=PropUtils.getPropertyDescriptors(objeto);
		for (int i=0; i< PropDesc.length; i++) {
			String propertyName = PropDesc[i].getName();			
			if ("class".equals(propertyName)) {
				continue; // Se for uma classe ocasionara em erro					
			}		      			
			// se a propriedade ? de escrita (setXXX)
			if (PropUtils.isWriteable(objeto,propertyName)) {
				fields.add(propertyName);
			}
		}
		return fields;
	}	
	
}
