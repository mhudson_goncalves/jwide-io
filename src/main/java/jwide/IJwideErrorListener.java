package jwide;

public interface IJwideErrorListener {
	
	public void addErrorMessage(String message);
	public boolean isStackValid();
	public boolean hasErrors();
	public void clear();
	
}
