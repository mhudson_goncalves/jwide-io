package jwide;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JwidePrintBody {
	
	public enum Align {
		LEFT,
		RIGHT,
		TRIM
	}
	
	private String body=null;
	private Pattern bodyPattern = Pattern.compile("_*_");
	private Matcher bodyMatch;
	private ArrayList<String> fields;
	private int currentField=0;
		
	public JwidePrintBody() {		
	}
	
	public void beginPrint(String body) {
		this.body=body;

		currentField=0;
		bodyMatch = bodyPattern.matcher(body);
		fields = new ArrayList<String>();
		while (bodyMatch.find()) {
			fields.add(bodyMatch.group());			
		}
	}
	
	public void autoPrintText(String text) throws Exception {
		this.autoPrintText(text, Align.LEFT);
	}
	
	public void autoPrintText(String text, Align align) throws Exception {
		currentField++;

		if (currentField>fields.size()) 
			throw new IllegalArgumentException("JwidePrint.autoPrintText - Invalid field number: "+currentField);

		StringBuffer fieldValue= new StringBuffer(text!=null?text:"");
		int fieldLength;
		if (align != Align.TRIM) {
			fieldLength=fields.get(currentField-1).length();
			
			while (fieldValue.length()<fieldLength) {			
				if (align == Align.LEFT) fieldValue.append(" ");
				else                     fieldValue.insert(0," ");
			}		
		} else {
			fieldLength=fieldValue.toString().length();
		}
		body=body.replaceFirst(fields.get(currentField-1),fieldValue.toString().substring(0,fieldLength));		
	}

	public void replace(String oldValue,String newValue) {
		body=body.replace(oldValue,newValue);
	}
	public String endPrint() {
		return body;
	}
}
