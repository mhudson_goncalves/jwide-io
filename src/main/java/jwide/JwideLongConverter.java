package jwide;

import javax.json.JsonNumber;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class JwideLongConverter implements Converter {
	
	   JwideUtil jwUtil = JwideUtil.getInstance();
		
	   public JwideLongConverter() {   	
	   }
	   
	   @SuppressWarnings({ "rawtypes", "unchecked" })
	   public Object convert(Class type, Object value) throws ConversionException {
	      if (value == null) return null;

	      // Support Calendar and Timestamp conversion
	      if (value instanceof Long) {
	      	 return value;         
	      }
	      else if (value instanceof String) {
	    	  try {
	             return new Long(value.toString().trim().replace(",",""));
	    	  } catch (Exception e) {
	    		 return null;
	    	  }
	      } else if (value instanceof JsonNumber){
	    	  return ((JsonNumber) value).longValue();
	      }
	      else {
	         throw new ConversionException("Type not supported: " +
	         value.getClass().getName());
	      }
	   }

}
