package jwide;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonStructure;
import javax.json.JsonValue;

import jwide.JwideDataObject;
import jwide.JwideFileHandler;
import jwide.JwideFileHandler.JwideFileReader;
import jwide.JwideUtil;

public class JwideJsonReader {

	private JwideUtil jUtil = JwideUtil.getInstance();
	private JwideFileHandler fileHandler = new JwideFileHandler();
	protected static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	private boolean caseSensitive = false;
	
	public JwideJsonReader() {
	} 
	
	public JwideJsonReader(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	} 
	
	public void setCaseSensitive(boolean caseSensitive){
		this.caseSensitive  = caseSensitive;
	}
	
	public boolean isCaseSensitive(){
		return this.caseSensitive;
	}

	public List<JwideDataObject> parse(String content) throws Exception {
		return this.parse(new ByteArrayInputStream(content.getBytes("UTF-8")));
	}
	
	public List<JwideDataObject> parseFromUrl(String url) throws Exception {
		return this.parse(new URL(url).openStream());
	}

	public List<JwideDataObject> parseFromFile(String file) throws Exception {
		JwideFileReader reader = fileHandler.newFileReader(file);
		StringBuffer content = new StringBuffer();
		while (reader.hasNextLine()){
			content.append(reader.readLine());
		}
		reader.close();

		return this.parse(content.toString());
	}
	
	public List<JwideDataObject> toDataObjects(JsonArray jsonArray) throws Exception{
		if (jsonArray==null){
			return null;
		}

		List<JwideDataObject> values = new ArrayList<JwideDataObject>();

		for (int i=0;i<jsonArray.size();i++) {				
			JsonObject value =(JsonObject)jsonArray.get(i);

			List<String> cols = new ArrayList<>();
			for (String key : value.keySet()) {
				cols.add(key);
			}
			values.add(this.toDataObject(value, cols.toArray(new String[0])));
		}

		return values;
	}

	//-->>
	public JwideDataObject toDataObject(JsonObject jsonObject) throws Exception{
		if (jsonObject==null){
			return null;
		}

		List<String> cols = new ArrayList<>();
		for (String key : jsonObject.keySet()) {
			cols.add(key);
		}
		return this.toDataObject(jsonObject, cols.toArray(new String[0]));
	}

	public JwideDataObject toDataObject(JsonObject jsonObject, String... columns) throws Exception {
		if (jsonObject==null){
			return null;
		}		

		List<Object> values = new ArrayList<>();
		for (String c:columns){	

			Object value = jsonObject.get(c);
			if (value == null){
				values.add(null);
			} else if (value instanceof JsonArray){

				try { // pode ser array sem estrutura json: ["a","b","c"]
					values.add(this.toDataObjects((JsonArray)value));
				} catch (Exception e){
					values.add(value);
				}
				/*				
				StringBuilder array = new StringBuilder();
				for (int i=0;i<((JsonArray)value).size();i++) {
					if (array.length()>0){
						array.append(", ");
					}
					array.append( ((JsonArray)value).get(i) );
				}
				values.add("[" + array.toString() + "]");
				 */
			} else if (value instanceof JsonString){
				values.add(jsonObject.getString(c));
			} else {	
				values.add(value);
			}
		}	

		return new JwideDataObject(Arrays.asList(columns), values);
	}	

	public String toJson(JwideDataObject value) throws Exception {
		return this.toJsonBuilder(value).build().toString();
	}
	
	public String toJson(String tag, List<JwideDataObject> values) throws Exception {
		JwideDataObject root = new JwideDataObject();
		root.addValue(tag, values);
		return this.toJson(root);
	}
	
	public String toJson(List<JwideDataObject> values) throws Exception {
		return this.toJsonArrayBuilder(values).build().toString();
	}
		
	public JsonArrayBuilder toJsonArrayBuilder(List<JwideDataObject> data) throws Exception {  
	
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (JwideDataObject o:data){					
			arrayBuilder.add(this.toJsonBuilder(o));
		}		
		return arrayBuilder;
	}
	
	@SuppressWarnings("unchecked")
	public JsonObjectBuilder toJsonBuilder(JwideDataObject data) throws Exception {	       

		if (data==null){
			throw new IllegalArgumentException("JwideJsonReader.toJson Data Cannot Be Null");
		}
		
		JsonObjectBuilder JSonBuilder = Json.createObjectBuilder();

		for (String column:data.getColumns()){

			Object value=data.getValue(column);
			String col=(this.isCaseSensitive()?column:column.toLowerCase());

			if (value==null){
				JSonBuilder.addNull(col);	
			} else if (this.isJsonContent(value)){
				List<JwideDataObject> datas = this.parse(new ByteArrayInputStream(value.toString().getBytes("UTF-8")));
				JsonArrayBuilder JsonArray = Json.createArrayBuilder();
				for (JwideDataObject d:datas){
					JsonArray.add(this.toJsonBuilder(d));									
				}	
				JSonBuilder.add(col, Json.createObjectBuilder().add("data", JsonArray));
			} else if (value instanceof JsonArray){
				JSonBuilder.add(col, (JsonArray)value);
			} else if (value instanceof Integer){
				JSonBuilder.add(col, (Integer)value);
			} else if (value instanceof Double){
				JSonBuilder.add(col, (Double)value);
			} else if (value instanceof BigDecimal){				
				JSonBuilder.add(col,(BigDecimal)value);
			} else if (value instanceof Timestamp){				
				JSonBuilder.add(col, jUtil.formatDate(DATE_FORMAT, (Timestamp)value).replace(" 00:00:00.000",""));
			} else if (value instanceof Date){
				JSonBuilder.add(col, jUtil.formatDate((Date)value));
			} else if (value instanceof Boolean){
				JSonBuilder.add(col, (Boolean)value);
			} else if (value instanceof Long){
				JSonBuilder.add(col, (Long)value);
			} else if (value instanceof List<?>){//else if ( (value instanceof List<?>) && (((List<?>)value).size()>0) && (((List<?>)value).get(0) instanceof JwideDataObject)){
				try {
					JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
					for (JwideDataObject o:((List<JwideDataObject>)value)){	
						arrayBuilder.add(this.toJsonBuilder(o));
					}				
					JSonBuilder.add(col, arrayBuilder);
				} catch (Exception e){
					JSonBuilder.add(col, value.toString());
				}
			} else if (value instanceof JwideDataObject){				
				JSonBuilder.add(col, this.toJsonBuilder((JwideDataObject)value));
			} else if (value instanceof JsonObject){	
				JSonBuilder.add(col, (JsonObject)value);
			} else if (value instanceof JsonValue){
				JSonBuilder.add(col, (JsonValue)value);
			} else {
				JSonBuilder.add(col, String.valueOf(value));
			}
		}	

		return JSonBuilder;
	}

	private boolean isJsonContent(Object value){
		if (value==null) return false;
		if (value.toString().length()<=this.jSonBegin().length()) return false;
		return (value.toString().substring(0,this.jSonBegin().length()).equals(this.jSonBegin()));
	}

	protected String jSonBegin(){
		StringBuffer json = new StringBuffer();
		json.append("{");
		json.append(this.toJsonTag("data"));
		json.append("[");
		return json.toString();	
	}

	protected String jSonEnd(){
		StringBuffer json = new StringBuffer();
		json.append("\n");
		json.append("]}");
		return json.toString();	
	}

	private String toJsonTag(String name){
		if (this.isCaseSensitive()){
			return jUtil.quote(name)+":";
		} else {
			return jUtil.quote( (name.toUpperCase().equals(name)?name.toLowerCase():name) )+ ":";
		}
	}

	private List<JwideDataObject> parse(InputStream is) throws Exception {

		JsonReader jsonReader = Json.createReader(is);
		
		JsonStructure jsonStructure = jsonReader.read();
		
		// Raiz
		//JsonObject rootJSON = jsonReader.readObject();
		jsonReader.close();
		is.close();	

		// parse
		List<JwideDataObject> values = null;
		
		if (jsonStructure instanceof JsonObject){

			JsonObject rootJSON = (JsonObject)jsonStructure;

			if  (rootJSON.keySet().size()==1 && rootJSON.containsKey("data")){
				try {
					values = this.toDataObjects(rootJSON.getJsonArray("data"));
				} catch (Exception e){
					values = new ArrayList<>();
					values.add(this.toDataObject(rootJSON));
				}
			} else {
				values = new ArrayList<>();
				values.add(this.toDataObject(rootJSON));				
			}
		} else if (jsonStructure instanceof JsonArray){
			JsonArray jsonArray = (JsonArray)jsonStructure;
			values = this.toDataObjects(jsonArray);
		}

		return values;
	}		

}
