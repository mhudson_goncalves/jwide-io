package jwide;

import javax.json.JsonNumber;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class JwideDoubleConverter implements Converter {
	
	   JwideUtil jwUtil = JwideUtil.getInstance();
		
	   public JwideDoubleConverter() {   	
	   }

	   /* (non-Javadoc)
	      * @see org.apache.commons.beanutils.Converter#convert(java.lang.Class,
	   java.lang.Object)
	   */
	   
	  @SuppressWarnings({ "rawtypes", "unchecked" })
	  public Object convert(Class type, Object value) throws ConversionException {
	      if (value == null) return null;

	      // Support Calendar and Timestamp conversion
	      if (value instanceof Double) {
	      	 return value;         
	      } else if (value instanceof String) {
	    	  try {
	             return new Double(value.toString().trim().replace(",",""));
	    	  } catch (Exception e) {
	    		 return null;
	    	  }
	      } else if (value instanceof JsonNumber){
	    	  return (JsonNumber)((JsonNumber) value).bigDecimalValue();
	      }
	      else {
	         throw new ConversionException("Type not supported: " +
	         value.getClass().getName());
	      }
	   }

}
