package jwide;
import jwide.JwideDTO;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSetMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JwideDatabaseObjects {

	/*
	 * TODO: Converter campos Double com ate 6 casas e sem decimais para Integer 
	 * TODO: Criar uma factory para conexoes com outro nome ConnectionFactory
	 * TODO: Try..catch no dao para execu??o das queries, fechando a conexao ao final
	 */
	
	private static final List<String> POSTGRESQL_CONNECTION_ID = Arrays.asList("postgresql");
	private static final List<String> ORACLE_CONNECTION_ID = Arrays.asList("oracle");
	private static final List<String> SQL_SERVER_CONNECTION_ID = Arrays.asList("sqlserver");
	
	private JwideUtil jUtil = JwideUtil.getInstance();

	public JwideDatabaseObjects() {
	}

	public boolean isPkColumn(Connection con,String DatabaseObjectName,String column) {
		JwideDTO pk = this.getTablePrimaryKey(con, DatabaseObjectName);
		return pk.locate("COLUMN_NAME",column);		
	}

	public JwideDTO getDatabaseObjects(Connection con,String[] DatabaseObjectsType) throws Exception {
		/*TABLE_CAT String => table catalog (may be null) 
			 TABLE_SCHEM String => table schema (may be null) 
			 TABLE_NAME String => table name 
			 TABLE_TYPE String => table type. Typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM". 
			 REMARKS String => explanatory comment on the table 
		 */
		
		JwideDTO DbObjects = new JwideDTO();
		DbObjects.populate(getMetadata(con).getTables(null, null, "%",DatabaseObjectsType)); 
		return DbObjects;
	}

	public JwideDTO getTableColumns(Connection Con, String DatabaseObjectName) throws Exception {
		// COLUMN_NAME
		// TYPE_NAME
		// DATA_TYPE
		// COLUMN_SIZE
		// DECIMAL_DIGITS
		// COLUMN_DEF (Default)
		// IS_NULLABLE
		
		JwideDTO PatternDTO = this.getDatabaseObjectPattern(Con, DatabaseObjectName);
		
		JwideDTO Cols = new JwideDTO();	                 
		Cols.populate(getMetadata(Con).getColumns(
				                                  PatternDTO.getString("catalog"), 
				                                  PatternDTO.getString("schema"), 
				                                  PatternDTO.getString("name"),
				                                  "%"
				                                  )
				);
		return Cols;
	}
	
	public JwideDTO getProcedureColumns(Connection Con, String DatabaseObjectName) throws Exception {
		// COLUMN_NAME
		// TYPE_NAME
		// DATA_TYPE
		// COLUMN_SIZE
		// DECIMAL_DIGITS
		// COLUMN_DEF (Default)
		// IS_NULLABLE
		
		JwideDTO PatternDTO = this.getDatabaseObjectPattern(Con, DatabaseObjectName);
		
		JwideDTO Cols = new JwideDTO();	                 
		Cols.populate(getMetadata(Con).getProcedureColumns(
				                                  PatternDTO.getString("catalog"), 
				                                  PatternDTO.getString("schema"), 
				                                  PatternDTO.getString("name"),
				                                  "%"
				                                  )
				);
		return Cols;
	}
	
    public JwideDTO getDatabaseObjectPattern(Connection Con, String DatabaseObjectName){
    	
    	JwideDTO dto = new JwideDTO();
    	dto.setColumns("catalog","schema","name");
    	if (DatabaseObjectName!=null){
    		String aowner =  null;
    		String catalog = null;
    		String name =  null;	

    		List<String> info = jUtil.lineToArray(DatabaseObjectName, "."); 

    		if (info.size()==3) {
    			aowner = info.get(0);
    			catalog = info.get(1);
    			name = info.get(2);
    		} else if (info.size()==2) {
    			aowner = info.get(0);
    			name = info.get(1);
    		} else {				
    			name = info.get(0);
    		}
    		
    		dto.addRow(this.formatDatabaseObject(Con, catalog), this.formatDatabaseObject(Con, aowner), this.formatDatabaseObject(Con, name));
    	}
    	dto.first();
    	return dto;    	
    }
	
	public JwideDTO getTablePrimaryKey(Connection Con,String DatabaseObjectName) {
		/*
			 TABLE_CAT String => table catalog (may be null) 
			 TABLE_SCHEM String => table schema (may be null) 
			 TABLE_NAME String => table name 
			 COLUMN_NAME String => column name 
			 KEY_SEQ short => sequence number within primary key 
			 PK_NAME String => primary key name (may be null) 
		 */
		JwideDTO Pk = new JwideDTO();
		JwideDTO PatternDTO = this.getDatabaseObjectPattern(Con, DatabaseObjectName);

		try  {                     
			Pk.populate(getMetadata(Con).getPrimaryKeys(
					                                    PatternDTO.getString("catalog"), 
                                                        PatternDTO.getString("schema"), 
                                                        PatternDTO.getString("name")
					                                   )
					    );             
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return Pk;
	}

	public JwideDTO getTableIndexes(Connection Con,String DatabaseObjectName) {
		/*TABLE_CAT String => table catalog (may be null) 
			 TABLE_SCHEM String => table schema (may be null) 
			 TABLE_NAME String => table name 
			 NON_UNIQUE boolean => Can index values be non-unique. false when TYPE is tableIndexStatistic 
			 INDEX_QUALIFIER String => index catalog (may be null); null when TYPE is tableIndexStatistic 
			 INDEX_NAME String => index name; null when TYPE is tableIndexStatistic 
			 TYPE short => index type: 
			 tableIndexStatistic - this identifies table statistics that are returned in conjuction with a table's index descriptions 
			 tableIndexClustered - this is a clustered index 
			 tableIndexHashed - this is a hashed index 
			 tableIndexOther - this is some other style of index 
			 ORDINAL_POSITION short => column sequence number within index; zero when TYPE is tableIndexStatistic 
			 COLUMN_NAME String => column name; null when TYPE is tableIndexStatistic 
			 ASC_OR_DESC String => column sort sequence, "A" => ascending, "D" => descending, may be null if sort sequence is not supported; null when TYPE is tableIndexStatistic 
			 CARDINALITY int => When TYPE is tableIndexStatistic, then this is the number of rows in the table; otherwise, it is the number of unique values in the index. 
			 PAGES int => When TYPE is tableIndexStatisic then this is the number of pages used for the table, otherwise it is the number of pages used for the current index. 
			 FILTER_CONDITION String => Filter condition, if any. (may be null) 
		 */
		JwideDTO Idx = new JwideDTO();
		JwideDTO PatternDTO = this.getDatabaseObjectPattern(Con, DatabaseObjectName);
		
		try  {                     
			Idx.populate(getMetadata(Con).getIndexInfo( 
					                                   PatternDTO.getString("catalog"), 
                                                       PatternDTO.getString("schema"), 
                                                       PatternDTO.getString("name"), 
                                                       false,
                                                       true
                                                      )); 
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return Idx;
	}

	public JwideDTO getTableForeignKeys(Connection Con,String DatabaseObjectName) {
		/* PKTABLE_CAT String => primary key table catalog being imported (may be null) 
			 PKTABLE_SCHEM String => primary key table schema being imported (may be null) 
			 PKTABLE_NAME String => primary key table name being imported 
			 PKCOLUMN_NAME String => primary key column name being imported 
			 FKTABLE_CAT String => foreign key table catalog (may be null) 
			 FKTABLE_SCHEM String => foreign key table schema (may be null) 
			 FKTABLE_NAME String => foreign key table name 
			 FKCOLUMN_NAME String => foreign key column name 
			 KEY_SEQ short => sequence number within a foreign key 
			 UPDATE_RULE short => What happens to a foreign key when the primary key is updated: 
			 importedNoAction - do not allow update of primary key if it has been imported 
			 importedKeyCascade - change imported key to agree with primary key update 
			 importedKeySetNull - change imported key to NULL if its primary key has been updated 
			 importedKeySetDefault - change imported key to default values if its primary key has been updated 
			 importedKeyRestrict - same as importedKeyNoAction (for ODBC 2.x compatibility) 
			 DELETE_RULE short => What happens to the foreign key when primary is deleted. 
			 importedKeyNoAction - do not allow delete of primary key if it has been imported 
			 importedKeyCascade - delete rows that import a deleted key 
			 importedKeySetNull - change imported key to NULL if its primary key has been deleted 
			 importedKeyRestrict - same as importedKeyNoAction (for ODBC 2.x compatibility) 
			 importedKeySetDefault - change imported key to default if its primary key has been deleted 
			 FK_NAME String => foreign key name (may be null) 
			 PK_NAME String => primary key name (may be null) 
			 DEFERRABILITY short => can the evaluation of foreign key constraints be deferred until commit 
			 importedKeyInitiallyDeferred - see SQL92 for definition 
			 importedKeyInitiallyImmediate - see SQL92 for definition 
			 importedKeyNotDeferrable - see SQL92 for definition 
		 */

		JwideDTO Fk = new JwideDTO();
		JwideDTO PatternDTO = this.getDatabaseObjectPattern(Con, DatabaseObjectName);
		
		try  {                     
			Fk.populate(getMetadata(Con).getImportedKeys(
					                                     PatternDTO.getString("catalog"), 
                                                         PatternDTO.getString("schema"), 
                                                         PatternDTO.getString("name")
                                                        )); 
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return Fk;
	}
	
	public JwideResultSetMetaDataDTO getResultSetMetaData(ResultSet rs) throws Exception {
		JwideResultSetMetaDataDTO mt = new JwideResultSetMetaDataDTO();
		mt.setColumns(new String[]{"column_name","column_type","column_type_number","display_size","scale"});

		ResultSetMetaData rsmd = rs.getMetaData();
		
		for (int i=1;i<=rsmd.getColumnCount();i++) { 
			List<Object> row = new ArrayList<Object>();

			row.add(rsmd.getColumnName(i));
			row.add(rsmd.getColumnTypeName(i));
			row.add(rsmd.getColumnType(i));
			row.add(rsmd.getColumnDisplaySize(i));
			row.add(rsmd.getScale(i)); // casas decimais
			mt.addRow(row);
		}
		return mt;	    
	}
	
	public static boolean isPostgreSqlConnection(Connection con){
		return JwideDatabaseObjects.isConnectionBelongsTo(con, POSTGRESQL_CONNECTION_ID);
	}
	
	public static boolean isOracleConnection(Connection con){
		return JwideDatabaseObjects.isConnectionBelongsTo(con, ORACLE_CONNECTION_ID);
	}
	
	public static boolean isSqlServerConnection(Connection con){
		return JwideDatabaseObjects.isConnectionBelongsTo(con, SQL_SERVER_CONNECTION_ID);
	}		
	
	private static boolean isConnectionBelongsTo(Connection con,List<String> identifications){		
		if (con!=null) {
			String strCon=con.toString().toLowerCase();
			for (String c:identifications) if (strCon.indexOf(c.toLowerCase())>=0) return true;
		}
		
		return false;
	}
	
	private DatabaseMetaData getMetadata(Connection con) {
		DatabaseMetaData DbMd=null;            
		try  {
			DbMd=con.getMetaData();                            
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return DbMd;
	}
	
	private String formatDatabaseObject(Connection Con,String dbObjectName){
		if (Con==null) return dbObjectName;
		if (dbObjectName==null) return dbObjectName;
		
	    if  (JwideDatabaseObjects.isOracleConnection(Con)) return  dbObjectName.toUpperCase();
	    return dbObjectName;
	}
	
}


