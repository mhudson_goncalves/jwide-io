/*
 * Created on 16/11/2004
 */
package jwide;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jwide.annotations.Inject;
import jwide.annotations.Validate;

/**
 * @author MHRG
 */

public class JwideValidator implements IJwideValidator {
	//private static JwideValidator instance;
	
	private JwideUtil jUtil = JwideUtil.getInstance();
	private JwideErrors jErrors = new JwideErrors();		
	private JwideReflection jReflection = JwideReflection.getInstance();
	private List<IJwideErrorListener> listeners = new ArrayList<IJwideErrorListener>();
	
	private static final String EMAIL_REGEX = "\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";
	private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
	
	public JwideValidator() {		
	}	
	
	public JwideValidator getNewInstance() {
		JwideValidator val = new JwideValidator();	  
		return val;	
	}
	
	public void addErrorListener(IJwideErrorListener listener){
		listeners.add(listener);
	}
	
	public void validate(Short value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Byte value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Boolean value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Long value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Float value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Integer value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Double value,String name,String... params) throws Exception {
		this.validateObject(value,name,params);		
	}
	
	public void validate(Object value,String name,String... params) throws Exception {
		this.validateObject(value, name, params);
	}
	
	public void validate(JwideVO valueObject) throws Exception {		
		List<String> fields = new ArrayList<>();
		
		for (Field field : valueObject.getClass().getDeclaredFields()) {			
			fields.add(field.getName());
		}
		this.validate(valueObject , fields.toArray(new String[]{}));
	}
	
	public void validate(JwideVO valueObject, String... fields) throws Exception {	

		for (String f:fields) {				
			Field field = valueObject.getClass().getDeclaredField(f.toLowerCase().trim());
			// Inject of class
			if (field.isAnnotationPresent(Inject.class)) {
				field.setAccessible(true);	
				Class<? extends JwideBO> bo = field.getAnnotation(Inject.class).name();
				try {
					jReflection.invokeMethod( bo.getConstructor().newInstance(), field.getAnnotation(Inject.class).method(), field.getType(), field.get(valueObject));
				} catch (Exception e){
					this.assign(e);
				}
			}
			// Validation
			if (field.isAnnotationPresent(Validate.class)) {   
				field.setAccessible(true);	
				this.validate(field.get(valueObject), field.getAnnotation(Validate.class).name(), field.getAnnotation(Validate.class).params() );
			}
		}		 
	}	
	
	private void validateObject(Object value,String name,String... params) throws Exception {
		String p; // parametro
		String c; // complemento	
		
		for (int i=0;i<params.length;i++) {
			p="{"+this.getErrorId(params[i])+"}";
			c=getErrorComplement(params[i]);	   	   
			
			boolean validError=false; // se o erro existe
			if (p.equals(REQUIRED)) {
				required(value,name); // REQUIRED
				validError=true;
			} else if (p.equals(NOT_REQUIRED)) {
				not_required(value,name); // NOT REQUIRED
				validError=true;
			} else if (p.equals(UPPER)) {
				upper(value,name);    // UPPER
				validError=true;
			} else if (p.equals(LOWER)) {
				lower(value,name);    // LOWER
				validError=true;	   	   	  
			} else if (p.equals(CHECK)) {
				check(value,c,name);  // CHECK+"A,B,C,D"
				validError=true;
			} else if (p.equals(RANGE)) {
				range(value,c,name);  // RANGE+"10.45,95.40"	ou RANGE+"01/01/1990,31/12/2050"
				validError=true;		      
			} else if (p.equals(SIZE)) {
				size(value,c,name);   // MAXSIZE+"0,35"
				validError=true;
			} else if (p.equals(EMAIL)){
				email(value,name);
				validError=true;
			}
			
			// valida��o n�o reconhecida
			if (!validError) {
				throw new JwideException("JwideValidator - ErrorType No Recognized: "+p);
			}
		}
		
	}
	
	public JwideErrors getErrors() {
		// se houver erros no listener (primefaces)
		boolean hasListenerValid=false;
		boolean hasErrorsInStack=false;
		for (IJwideErrorListener listener:listeners){
			if (listener.isStackValid()){
				hasListenerValid=true;
			}
			if (listener.isStackValid()){
				if (!hasErrorsInStack){
					hasErrorsInStack=(listener.hasErrors());
				}
			}
		}
		
		// se n?o houver erros no listener, invalidar a lista
		if (hasListenerValid){
			if (!hasErrorsInStack){
				jErrors.clearErrors();
			}
		}	
		
		return jErrors;
	}
	
	public JwideDTO getErrorsList(){
		return this.getErrorsList(this);
	}
	
	public void assign(JwideValidator validator){
		JwideDTO ErrDTO=this.getErrorsList(validator);
		ErrDTO.beforeFirst();
		while (ErrDTO.next()) this.addError(ErrDTO.getString("mensagem"));
	}	
	
	public void assign(Exception e){	
		Exception exception = null;		
		if ((e instanceof InvocationTargetException)){
			exception = this.extractJwideException( ((InvocationTargetException)e).getTargetException());
		}
		if (exception==null){
			exception = e;
		}		

		if (exception.getMessage()!=null){
			this.addError(exception.getMessage());
		} else if (exception.getCause()!=null && exception.getCause().getMessage()!=null){
			this.addError(exception.getCause().getMessage());
		} else {
			this.addError(jUtil.formatStackTraceInformation(exception));
		} 	
	}
	
	private JwideException extractJwideException(Throwable throwable){		
		if (throwable instanceof JwideException){
			return ((JwideException)throwable);
		} else if (throwable instanceof InvocationTargetException){
			return this.extractJwideException( ((InvocationTargetException)throwable).getTargetException());
		} else {
			return null;
		}
	}
	
	public void printStackTrace(Exception e){
		if (e!=null){		

			StringBuffer message = new StringBuffer();			
			message.append(e.getMessage());			

			StringWriter trace = new StringWriter();
			e.printStackTrace(new PrintWriter(trace));		

			message.append("\n");
			message.append(trace.toString());
			try { trace.close(); } catch (Exception _e){}
			
			this.addError(message.toString());
		}
	}	
	
	public void flush(String value) throws Exception {
		this.addError(value);
		this.flush();
	}
	
	public void flush(String type, String...values) throws Exception {
		this.addError(type, values);
		this.flush();
	}
	
	public void flush(JwideVO valueObject, String...values) throws Exception {
		this.validate(valueObject, values);
		this.flush();
	}
	
	public void flush(JwideVO valueObject) throws Exception {
		this.validate(valueObject);
		this.flush();
	}
		
	public void flush() throws Exception {		
		StringBuffer msg = new StringBuffer();
		JwideDTO ErrDTO = this.getErrorsList();
		ErrDTO.beforeFirst();
		while (ErrDTO.next()){
			if (msg.length()>0){
				msg.append("\n");
			}
			msg.append(ErrDTO.get("mensagem"));
		}
		
		if (msg.length()>0){
			this.getErrors().clearErrors();
			throw new JwideException(msg.toString());	
		}		
	}
	
	private JwideDTO getErrorsList(JwideValidator validator){
		JwideErrorsList ErrList = new JwideErrorsList();
		ErrList.setErrors(validator.getErrors());
		JwideDTO ErrDTO = ErrList.getErrorsList();
		ErrDTO.first();
		return ErrDTO;
	}
	
	public boolean isEmpty() {
		return this.getErrors().isEmpty();
	}
	
	private void check(Object value,String complement,String name) {
		// parametro nao indicado
		if (value==null) return;
		
		int pos=0;		
		try {			
			List<String> elements=jUtil.lineToArray(complement);
			pos=elements.indexOf(value.toString());
		}
		catch (Exception e) {
			throw new IllegalArgumentException("JwideValidator.check: Illegal Object Value: "+value.toString());
		}		
		if (pos<0) addInternalError(CHECK,new String[]{name,complement}); 
	}
	
	private void range(Object value,String complement,String name) {			
		if (value==null) return;
		if (complement.indexOf(",")<0)
			throw new IllegalArgumentException("JwideValidator.range: there is no comma separator: "+complement);
		
		// verificar qual "range" utilizar 		
		if (complement.indexOf("/")>=0) rangeDate(value,complement,name); 
		else                            rangeValue(value,complement,name);	
	}
	
	private void rangeDate(Object value,String complement,String name) {		
		// pegar as duas faixas
		int vg=complement.indexOf(",");
		
		Timestamp r1=null;
		Timestamp r2=null;        
		
		String vStr1;
		String vStr2;
		
		try {
			vStr1=complement.substring(0,vg);
			vStr2=complement.substring((vg+1),complement.length());
		}
		catch (Exception e) { 		
			throw new IllegalArgumentException("JwideValidator.range: Illegal RangeValue: "+complement);
		}        
		
		try {
			r1=jUtil.strToTimestamp(vStr1);
			r2=jUtil.strToTimestamp(vStr2);
		}
		catch (Exception e) { 		
			throw new IllegalArgumentException("JwideValidator.range: Illegal RangeValue: "+complement);
		}
		
		// se parametro nao for indicado, enviar o erro
		if (value==null) {
			addInternalError(RANGE,new String[]{name,vStr1,vStr2});
			return;
		}
		
		// verificar se a faixa esta correta
		Timestamp dtValue;
		try {
			dtValue=(java.sql.Timestamp)value;				
		}
		catch (Exception e) {
			throw new IllegalArgumentException("JwideValidator.range: Illegal Date Value: "+value);					
		}		
		
		try {
			if ((dtValue.getTime()<r1.getTime()) ||
					(dtValue.getTime()>r2.getTime())) {
				addInternalError(RANGE,new String[]{name,vStr1,vStr2});
			}			
		}
		catch (Exception  e){
			addInternalError(RANGE,new String[]{name,vStr1,vStr2});			
		}		
	}	
	
	private void rangeValue(Object value,String complement,String name) {		
		// pegar as duas faixas
		int vg=complement.indexOf(",");
		
		double r1=0;
		double r2=0;
		
		String vStr1;
		String vStr2;
		
		try {
			vStr1=complement.substring(0,vg);
			vStr2=complement.substring((vg+1),complement.length());
		}
		catch (Exception e) { 		
			throw new IllegalArgumentException("JwideValidator.range: Illegal RangeValue: "+complement);
		}        
		
		try {
			r1=Double.parseDouble(vStr1);
			r2=Double.parseDouble(vStr2);
		}
		catch (Exception e) { 		
			throw new IllegalArgumentException("JwideValidator.range: Illegal RangeValue: "+complement);
		}
		
		// se parametro nao for indicado, enviar o erro
		if (value==null) {
			addInternalError(RANGE,new String[]{name,vStr1,vStr2});	
			return;
		}
		
		// verificar se a faixa esta correta
		
		try {
			if ((Double.parseDouble(value.toString())<r1) ||
					(Double.parseDouble(value.toString())>r2)) {
				addInternalError(RANGE,new String[]{name,vStr1,vStr2});
			}			
		}
		catch (Exception  e) {
			addInternalError(RANGE,new String[]{name,vStr1,vStr2});			
		}		
	}	
	
	private void required(Object value,String name) {
		if (value==null) addInternalError(REQUIRED,new String[]{name});
		else {
			if (value.toString().trim().length()==0) addInternalError(REQUIRED,new String[]{name});
		}
	}
	
	private void not_required(Object value,String name) {
		if (value!=null) addInternalError(NOT_REQUIRED,new String[]{name});
	}
	
	private void upper(Object value,String name) {
		if (value==null) return;
		if (!value.toString().equals(value.toString().toUpperCase())) {		
			addInternalError(UPPER,new String[]{name+" - "+value.toString()});
		}
	}
	
	private void lower(Object value,String name) {
		if (value==null) return;
		if (!value.toString().equals(value.toString().toLowerCase())) {		
			addInternalError(LOWER,new String[]{name});
		}
	}
	
	private void size(Object value,String complement,String name) {		
		// pegar as duas faixas
		int vg=complement.indexOf(",");
		
		int minSize=0;
		int maxSize=0;
		
		String vStr1;
		String vStr2;
		
		try {
			vStr1=complement.substring(0,vg);
			vStr2=complement.substring((vg+1),complement.length());
		}
		catch (Exception e) { 		
			throw new IllegalArgumentException("JwideValidator.size: Illegal RangeValue: "+complement);
		}        
		
		try {
			minSize=Integer.parseInt(vStr1);
			maxSize=Integer.parseInt(vStr2);
		}
		catch (Exception e) { 		
			throw new IllegalArgumentException("JwideValidator.size: Illegal SizeValue: "+complement);
		}
		
		// se parametro nao for indicado, enviar o erro
		if ((value==null) && (minSize>0)) {
			addInternalError(SIZE,new String[]{name,vStr1,vStr2});
			return;
		}
		if (value==null) return; // se o size=0 o valor pode estar nulo
		
		
		if ((value.toString().length()<minSize) || (value.toString().length()>maxSize)) {
			addInternalError(SIZE,new String[]{name,vStr1,vStr2});			
		}					
	}	
	
	private void email(Object value,String name){
		if (value!=null && !value.toString().trim().equals("")){
			Matcher matcher = EMAIL_PATTERN.matcher(value.toString());
			if (!matcher.matches()){
				addInternalError(EMAIL,new String[]{name});
			}
		}	
	}
	
	private String getErrorId(String type) {
		// retirar o {}
		if (type.substring(0,1).equals("{")){
			return type.substring(1, type.indexOf("}"));
		} else {
			return type;
		}	
	}
	
	private String getErrorComplement(String type) {
		String t=type;
		t=t.substring(t.indexOf("}")+1,t.length());	    	    		
		return t;		
	}
	
	public void addError(String type, String value) {
		this.addError(type, new String[]{value});
	}
	
	public void addError(String name) {
		if (name!=null){
			if (name.indexOf("{")==0 && name.endsWith("}") && name.indexOf(".")>0){
			   this.addError(name, new String[]{});	
			} else {
				this.addError("user.general",new String[]{name});
			}
		}		
	}
	
	public void addError(String type, String... values) {
		jErrors.addError(this.getErrorId(type), values);
		if (listeners.size()>0){
			String message= new JwideErrorsList().getLastErrorMessage(jErrors);
			for (IJwideErrorListener listener:listeners){
				listener.addErrorMessage(message);
			}
		}
	}
	
	private void addInternalError(String type,String[] names) {
		// Esta rotina trata o erro, eliminando as {}
		this.addError(this.getErrorId(type),names);
	}
}
