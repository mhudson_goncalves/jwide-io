package jwide;

import java.io.Writer;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import jwide.JwideProcedure;
import jwide.JwideQuery;

public class JwideSimpleQuery {

	private static final int FETCH_SIZE=20;	

	public JwideSimpleQuery() {
	}
		
	public void close(ResultSet Rs) throws Exception {
		Rs.getStatement().close();
		Rs.close();
	}
	
	public Integer nextVal(Connection Con,String sequenceName) throws Exception {
		String Sql=null;
		if (JwideDatabaseObjects.isOracleConnection(Con)){
			Sql =
				  " SELECT "+ sequenceName + ".nextval seq_value"+
                  " FROM dual";
		} else if (JwideDatabaseObjects.isPostgreSqlConnection(Con)){
			Sql =
				  " select nextval('" + sequenceName + "') seq_value";
		}
		
		ResultSet Rs=this.open(Con, Sql);
		Rs.next();
		Integer nextVal=Rs.getInt("seq_value");
		this.close(Rs);
		
		return nextVal; 
	}
	
	public Timestamp currentDate(Connection Con) throws Exception {
		String Sql=null;
		if (JwideDatabaseObjects.isOracleConnection(Con)){
			Sql=
				   " SELECT"+ 
                   " current_date date_value"+
                   " FROM dual";
		} else if (JwideDatabaseObjects.isPostgreSqlConnection(Con)){
			Sql=
				   " SELECT"+ 
                   " current_timestamp date_value"+
                   " FROM dual";
		}
		
		ResultSet Rs=this.open(Con, Sql);
		Rs.next();
		Timestamp currentDate=Rs.getTimestamp("date_value");
		this.close(Rs);
		
		return currentDate; 
	}
	
	public Boolean setAutocommit(Connection Con,Boolean value){
		try {
			Con.setAutoCommit(value);
			return true;
		} catch (Exception e){
			return false;
		}
	}
	
	public Clob strToClob(Connection Con,String value) throws Exception {
		if (value==null) return null;
		
		Clob ClobValue = Con.createClob();
		Writer ClobWriter = ClobValue.setCharacterStream(1);
		//ClobWriter.write(new String(value.getBytes("UTF-8")));		
		ClobWriter.write(value);
		ClobWriter.close();	
		
		return ClobValue;
	}
	
	public String clobToStr(Clob value) throws Exception {
		if (value==null) return null;		
		return (String)value.getSubString(1,(int)value.length());      
	}
	
	public JwideDTO toDto(Connection Con,String sql) throws Exception {
		ResultSet Rs = this.open(Con, sql);
		JwideDTO dto = new JwideDTO().populate(Rs);
		this.close(Rs);
		return dto;
	}
	
	public JwideDTO toDto(Connection Con,String sql,String params,Object...values) throws Exception {
		ResultSet Rs = this.open(Con, sql, params, values);
		JwideDTO dto = new JwideDTO().populate(Rs);
		this.close(Rs);
		return dto;
	}
	
	public ResultSet open(Connection Con,String sql) throws Exception {
		JwideQuery Qry = this.createQuery(Con, sql, false, null,new Object[]{});
		Qry.executeQuery();
		return Qry.getResultSet();	
	}
	
	public ResultSet open(Connection Con,String sql,String params,Object...values) throws Exception {
		JwideQuery Qry = this.createQuery(Con, sql, true, params, values);
		Qry.executeQuery();
		return Qry.getResultSet();
	}
	
	public ResultSet openForBuilk(Connection Con,String sql) throws Exception {
		JwideQuery Qry = this.createQuery(Con, sql, true, null,new Object[]{});
		Qry.executeQuery();
		return Qry.getResultSet();	
	}
	
	public ResultSet openForBuilk(Connection Con,String sql,String params,Object...values) throws Exception {
		JwideQuery Qry = this.createQuery(Con, sql, true, params, values);
		Qry.executeQuery();
		return Qry.getResultSet();
	}
	
	public Integer update(Connection Con,String sql) throws Exception {
		String err=null;
		Integer rowsAffected=0;	
		JwideQuery Qry = this.createQuery(Con, sql, false, null,new Object[]{});
		
		try {
			Qry.executeUpdate();
			rowsAffected=Qry.getUpdateCount();
		} catch (Exception e){
           err=e.getMessage();
		} finally {		
			Qry.closeStatement();
		}
		
		if (err!=null){
			throw new IllegalArgumentException(err);
		}
		
		return rowsAffected;
	}
	
	public Integer update(Connection Con,String sql,String params,Object...values) throws Exception {
		JwideQuery Qry = this.createQuery(Con, sql, false, params, values);
		Exception error = null;
		Integer rowsAffected=0;	
		try {
			Qry.executeUpdate();
			rowsAffected=Qry.getUpdateCount();
		} catch (Exception e){
           error = e;
		} finally {		
			Qry.closeStatement();
		}
		
		if (error!=null){
			throw error;
		}
		
		return rowsAffected;
	}
		
	public void execProc(Connection Con,String sql) throws Exception {
		this.execProc(Con, sql,null,new Object[]{});		
	}
	
	public void execProc(Connection Con, String sql, String params, Object...values) throws Exception {
		// " { call CORRIGIR_SEGURADO_CONTAS (:protocolo) }"
		JwideProcedure Proc = new JwideProcedure();
		Proc.setConnection(Con);
		Proc.setSql(sql);		
		Proc.prepareCall();
		
		if (params!=null) {
			int i=0;
			for (String p:params.split(",")){
				Proc.setParameterValue(p.trim(),values[i++]);
			}
		}
		Proc.executeUpdate();				
		Proc.closeStatement();	
	}
	
	private JwideQuery createQuery(Connection Con,String sql,Boolean forBuilk, String params,Object...values) throws Exception {
		JwideQuery Qry = new JwideQuery();
		Qry.setConnection(Con);
		Qry.setSql(sql);
		if (forBuilk) {
			Qry.prepareStatementForBuilk(FETCH_SIZE);
		} else {
			Qry.prepareStatement();
		}
		if (params!=null) {
			int i=0;
			for (String p:params.split(",")){
				Qry.setParameterValue(p.trim(),values[i++]);
			}
		}
		return Qry;
	}

}
