package jwide;


import java.awt.print.PrinterJob;
import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Sides;

public class JwidePrintHandler {	    
  
 
  public JwidePrintHandler() {
  }
  
  /* Listar as impressoras instaladas */
  public JwideDTO getPrintersList() {
	 JwideDTO Printers = new JwideDTO();
	 Printers.setColumns(new String[]{"printer_name"});
	 PrintService[] LPrinters=this.lookupPrinters(); 
	  for (PrintService p : LPrinters) {   
		  Printers.addRow(new Object[]{p.getName()});   
	  }   
	  return Printers;
  }  
  
  // criar o servi?o de impress?o atrav?s do nome da impressora
  public PrintService createPrintService(String printerName) {
	  JwideDTO Printers=this.getPrintersList();

	  int i=0;
	  Printers.beforeFirst();
	  while (Printers.next()) {
		  if (Printers.getString("printer_name").equals(printerName)) {
			  PrintService[] LPrinters=this.lookupPrinters();
			  return LPrinters[i];				  
		  }
		  i++;
	  }
	  return null;
  }  

  public PrintRequestAttributeSet createAttributeSet(){
	  PrintRequestAttributeSet prASet = new HashPrintRequestAttributeSet(); 
	  return prASet;
  }
  
  public void setDuplex(PrintRequestAttributeSet prASet){
	  prASet.add(Sides.DUPLEX); // prASet.add( Sides.TUMBLE );//DUPLEX SEM INVERS?O   	
  }  
  
  private PrintService[] lookupPrinters() {
	  return PrinterJob.lookupPrintServices();	  
  }

}
