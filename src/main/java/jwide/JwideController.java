/*
  * Created on 23/02/2005 
 */

package jwide; 

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * @author MHRG
 */

 
public class JwideController implements IJwideValidator, IJwideController { 	
			
	protected JwideValidator validator = new JwideValidator();
	protected JwideParam params=new JwideParam();
	protected JwideDTO ReturnDTO=new JwideDTO();
	
	private String parseContent=null;
	protected byte[] reportContent=null;	
	protected String excelFileName=null;
		
	private HttpServletRequest request=null;	
	private JwideJsonReader jsonReader = new JwideJsonReader();
	private JwideXmlReader xmlReader = new JwideXmlReader();	
		
	String action="";
	String lastResult="";
	String sessionID="";
	String controllerPageName="";
	String realPath="";
        
	public void setAction(String action) {
		this.action=action;		
	}
	
	public String getAction() {
		return action;
	}	
	
	public void setLastResult(String lastResult) {
		this.lastResult=lastResult;
	}
	
	public void init(){		
	}
	
	protected String getParseContent(){
		return this.parseContent;
	}
	
	public String getLastResult() {
		return this.lastResult;
	}
		
	public void setSessionID(String sessionID) {
		this.sessionID=sessionID;
	}
	protected String getSessionID() {
		return this.getRequest().getSession().getId();
	}
	
	public void setRealPath(String realPath){
		this.realPath=realPath;		
	}
	
	protected byte[] getToRequestReport(){
		return reportContent;
	}
	
	protected String render(JwideDTO dto){		
		if (JwideUtil.getInstance().isJsonContentType(request.getContentType())){
			return this.renderToJson(dto);
		} else if (JwideUtil.getInstance().isXmlContentType(request.getContentType())){
			return this.renderToXml(dto);
		} else {
			return this.renderToJson(dto);
		}		
	}
	
	protected String renderToJson(JwideDTO dto){
		this.parseContent = dto.toJson();
		return TO_JSON;
	}
	
	protected String renderToXml(JwideDTO dto) {		
		try {
			this.parseContent = dto.toXml();
		}catch (Exception e){
           validator.assign(e);
		}
		return TO_XML;
	}
	
	protected String render(JwideDataObject data) throws Exception {		
		if (JwideUtil.getInstance().isJsonContentType(request.getContentType())){
			return this.renderToJson(data);
		} else if (JwideUtil.getInstance().isXmlContentType(request.getContentType())){
			return this.renderToXml(data);
		} else {
			return this.renderToJson(data);
		}		
	}
	
	protected String renderToJson(JwideDataObject data) throws Exception {
		jsonReader.setCaseSensitive(true);
		this.parseContent = jsonReader.toJson(data);
		return TO_JSON;
	}
	
	protected String renderToXml(JwideDataObject data) throws Exception {		
		this.parseContent = xmlReader.toXml(data);
		return TO_XML;
	}
		
	protected String render(List<JwideDataObject> data) throws Exception {		
		if (JwideUtil.getInstance().isJsonContentType(request.getContentType())){
			return this.renderToJson(data);
		} else if (JwideUtil.getInstance().isXmlContentType(request.getContentType())){
			return this.renderToXml(data);
		} else {
			return this.renderToJson(data);
		}		
	}
	
	protected String renderToJson(List<JwideDataObject> data) throws Exception {
		jsonReader.setCaseSensitive(true);
		this.parseContent = jsonReader.toJson(data);
		return TO_JSON;
	}
	
	protected String renderToXml(List<JwideDataObject> data) throws Exception {		
		this.parseContent = xmlReader.toXml(data);
		return TO_XML;
	}
	
	protected String renderToJson(Exception e){
		validator.addError(e.getMessage());
		return TO_JSON;
	}
	
	protected String renderToXml(Exception e){
		validator.addError(e.getMessage());
		return TO_XML;
	}
	
	protected String renderToPage(String page){	
		return TO_PAGE + page;
	}
	
	protected String renderToReport(byte[] content){
		this.reportContent = content;
		return TO_REPORT;
	}
	
	protected String renderToExcel(JwideDTO dto, String fileName){
		this.excelFileName = fileName;
		this.parseContent = dto.toHtml();
		return TO_EXCEL;
	}
	
	protected String getRealPath(){
		return this.realPath;
	}	
	
	public void setRequest(HttpServletRequest request){
		this.request=request;
	}
	public HttpServletRequest getRequest(){
		return this.request;
	}
	
	public void setParams(JwideParam params){
		this.params=params;
	}
	
}
