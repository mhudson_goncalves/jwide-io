package jwide;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class JwideMailAutenticator extends Authenticator {

	public String userName = null;
	public String password = null;

	public JwideMailAutenticator(String userName, String password) {
		this.userName=userName;
		this.password=password;
	}
	
	public JwideMailAutenticator(JwideMailVO VO) {
		this(VO.getUserName(),VO.getPassword());
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName,password);
	}
}


