package jwide;

public class SqlTypesDTO extends JwideDTO {

	public SqlTypesDTO(){
		init();
	}
	
	public String getSqlType(int Type) {
		this.locate("id",Type);
		return this.getString("description");
	}

	
	private void init() {
		this.setColumns(new String[]{"id","description"});

		this.addRow(new Object[]{java.sql.Types.ARRAY,"ARRAY"});
		this.addRow(new Object[]{java.sql.Types.BIGINT,"BIGINT"});
		this.addRow(new Object[]{java.sql.Types.BINARY,"BINARY"});
		this.addRow(new Object[]{java.sql.Types.BIT,"BIT"});
		this.addRow(new Object[]{java.sql.Types.BLOB,"BLOB"});
		this.addRow(new Object[]{java.sql.Types.CHAR,"CHAR"});         
		this.addRow(new Object[]{java.sql.Types.CLOB,"CLOB"});
		this.addRow(new Object[]{java.sql.Types.DATE,"DATE"});
		this.addRow(new Object[]{java.sql.Types.DECIMAL,"DECIMAL"});
		this.addRow(new Object[]{java.sql.Types.DISTINCT,"DISTINCT"});
		this.addRow(new Object[]{java.sql.Types.DOUBLE,"DOUBLE"});
		this.addRow(new Object[]{java.sql.Types.FLOAT,"FLOAT"});
		this.addRow(new Object[]{java.sql.Types.INTEGER,"INTEGER"});
		this.addRow(new Object[]{java.sql.Types.JAVA_OBJECT,"JAVA_OBJECT"});
		this.addRow(new Object[]{java.sql.Types.LONGVARBINARY,"LONGVARBINARY"});
		this.addRow(new Object[]{java.sql.Types.LONGVARCHAR,"LONGVARCHAR"});
		this.addRow(new Object[]{java.sql.Types.NULL,"NULL"});
		this.addRow(new Object[]{java.sql.Types.NUMERIC,"NUMERIC"});
		this.addRow(new Object[]{java.sql.Types.OTHER,"OTHER"});
		this.addRow(new Object[]{java.sql.Types.REAL,"REAL"});
		this.addRow(new Object[]{java.sql.Types.REF,"REF"});
		this.addRow(new Object[]{java.sql.Types.SMALLINT,"SMALLINT"});
		this.addRow(new Object[]{java.sql.Types.STRUCT,"STRUCT"});
		this.addRow(new Object[]{java.sql.Types.TIME,"TIME"});
		this.addRow(new Object[]{java.sql.Types.TIMESTAMP,"TIMESTAMP"});
		this.addRow(new Object[]{java.sql.Types.TINYINT,"TINYINT"});
		this.addRow(new Object[]{java.sql.Types.VARBINARY,"VARBINARY"});
		this.addRow(new Object[]{java.sql.Types.VARCHAR,"VARCHAR"});
	}
}
