package jwide;

import jwide.JwideUtil;
import java.util.TimerTask;
import java.lang.reflect.Constructor;
import java.sql.Timestamp;
import jwide.JwideReflection;
import jwide.JwideSchedulerProperties.Scheduler;

public class JwideSchedulerTask extends TimerTask {   
	
	private JwideSchedulerProperties schedulerProperties = new JwideSchedulerProperties();	
    private JwideReflection Reflect = JwideReflection.getInstance();
    private JwideLog Log = JwideLog.getInstance();
    private JwideStack Stack = JwideStack.getInstance();
    private JwideUtil jUtil = JwideUtil.getInstance();  
  
    /*
     * protected static final String HOST_IP="host_ip";
	protected static final String HOST_NAME="host_name";
	protected static final String CONTEXT="context";
	protected static final String RUN_ONCE="run_once";
	protected static final String HOUR="hour";
	protected static final String DAY_OF_WEEK="day_of_week";
	protected static final String DAY_OF_MONTH="day_of_month";   
	protected static final String MONTH="month";
	protected static final String INTERVAL="interval"; // 
	protected static final String START_DATE="start_date";
	protected static final String END_DATE="end_date";
	protected static final String START_TIME="start_time";
	protected static final String END_TIME="end_time";
	(non-Javadoc)
     * @see java.util.TimerTask#run()
     */
      
	private final String contextPath;	
    
    public JwideSchedulerTask(String contextPath) {    	
		this.contextPath=contextPath; //-> /myapp
		
    	try {
    		schedulerProperties.readProperties();
    	} catch (Exception e){
    		Log.writeLog( this.getClass(), e);
    	}
    }
    
    protected int getSchedulersCount(){
    	return schedulerProperties.retrieveSchedulers().size();
    }
    
    protected boolean isSchedulerValid(){
    	return schedulerProperties.isSchedulerFileCreated(); // even if the file is empty, the scheduler must to up
    }
    
	public void run() {
		
		Log.writeLog(JwideLog.LogType.INFO, this.getClass().getName() + " RUNNING...");
		Timestamp actual = jUtil.getTodayTimestamp();
		for (Scheduler scheduler:schedulerProperties.retrieveSchedulers()){
			if (this.isProcessInScheduler(scheduler, actual)){				
				Runnable task = () -> {								                    
					this.executeProcess(scheduler);							                  
				};	
				new Thread(task).start();
			}
		}		
	}
	
	private Boolean isProcessInScheduler(Scheduler scheduler, Timestamp actual) {
		Boolean pCandidate=true;
		
		if (pCandidate) pCandidate=this.isHourCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isDayOfWeekCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isDayOfMonthCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isMonthCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isStartDateCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isEndDateCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isStartTimeCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isEndTimeCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isHostIpCandidate(scheduler);
		if (pCandidate) pCandidate=this.isHostNameCandidate(scheduler);
		if (pCandidate) pCandidate=this.isIntervalCandidate(scheduler, actual);
		if (pCandidate) pCandidate=this.isContextCandidate(scheduler);
		if (pCandidate) pCandidate=this.isRunOnceCandidate(scheduler);
		
		return pCandidate; 
	}
	
	// -- is hour in scheduler ? -- \\
	private Boolean isHourCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.HOUR)) return true; // isnt select
		
		String hours = scheduler.getString(JwideSchedulerProperties.HOUR);
		Boolean isCandidate=false;	
		
		try {			
			for (String hour:jUtil.lineToArray(hours)) {				
				if (!isCandidate) {
					isCandidate = this.isTimeBetween(hour, scheduler.getLastExecution(), actual);
				}			
			}
		}catch (Exception e) {
			this.addLogError("READING HOURS", hours, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	
	// -- is day of week in schedule ?? -- \\
	private Boolean isDayOfWeekCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.DAY_OF_WEEK)) return true; // isnt select
		
		String dayOfWeek = scheduler.getString(JwideSchedulerProperties.DAY_OF_WEEK);
	
		Boolean isCandidate=false;
		int day1=jUtil.datePart(jUtil.DAY_OF_WEEK,scheduler.getLastExecution());
		int day2=jUtil.datePart(jUtil.DAY_OF_WEEK,actual);
		
		try {		
			for (String dd:jUtil.lineToArray(dayOfWeek)) {	 
				int pDay=new Integer(dd.trim());
				if (!isCandidate){
				    isCandidate = (pDay==day1 || pDay==day2);
				}				
			}
		} catch (Exception e) {
			this.addLogError("READING DAY OF WEEK",dayOfWeek, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	
	// -- is day of month in schedule ?? -- \\
	private Boolean isDayOfMonthCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.DAY_OF_MONTH)) return true; // isnt select
		
		String dayOfMonth = scheduler.getString(JwideSchedulerProperties.DAY_OF_MONTH);
	
		Boolean isCandidate=false;
		int day1=jUtil.datePart(jUtil.DAY, scheduler.getLastExecution());
		int day2=jUtil.datePart(jUtil.DAY, actual);
		
		try {			
			for (String dd:jUtil.lineToArray(dayOfMonth)) {
				int pDay=new Integer(dd.trim());
				if (!isCandidate){
					isCandidate = (pDay==day1 || pDay==day2);
				}				
			}
		}catch (Exception e) {
			this.addLogError("READING DAY OF MONTH", dayOfMonth, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	// -- is month in schedule ?? -- \\
	private Boolean isMonthCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.MONTH)) return true; // isnt select
		String month = scheduler.getString(JwideSchedulerProperties.MONTH);
	
		Boolean isCandidate=false;
		int month1=jUtil.datePart(jUtil.MONTH, scheduler.getLastExecution()); 
		int month2=jUtil.datePart(jUtil.MONTH, actual); 
		
		month1++;
		month2++;
	
		try {
			for (String mm:jUtil.lineToArray(month)) {
				int pMonth=new Integer(mm.trim());
				if (!isCandidate){
					isCandidate = (pMonth==month1 || pMonth==month2);
				}				
			}
		}catch (Exception e) {
			this.addLogError("READING MONTH",month, e); 
			isCandidate=false;
		}
		return isCandidate;
	}
	// -- is start date in schedule ?? -- \\
	private Boolean isStartDateCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.START_DATE)) return true; // isnt select
		String startDate = scheduler.getString(JwideSchedulerProperties.START_DATE);
	
		Boolean isCandidate=false;
		
		try {
			Timestamp sDate=this.strToTimestamp(startDate);
			isCandidate = (sDate.compareTo(actual)<=0);
		}catch (Exception e) {
			this.addLogError("READING START DATE",startDate, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	// -- is end date in schedule ?? -- \\
	private Boolean isEndDateCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.END_DATE)) return true; // isnt select
		String endDate = scheduler.getString(JwideSchedulerProperties.END_DATE);
	
		Boolean isCandidate=false;
		
		try {
			Timestamp eDate=this.strToTimestamp(endDate);
			isCandidate = (eDate.compareTo(actual)>=0);		
		}catch (Exception e) {
			this.addLogError("READING END DATE", endDate, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	// -- is start time in schedule ?? -- \\
	private Boolean isStartTimeCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.START_TIME)) return true; // isnt select
		String startTime = scheduler.getString(JwideSchedulerProperties.START_TIME);
	
		Boolean isCandidate=false;
		
		try {
			Timestamp sTime=jUtil.strToTimestamp(jUtil.formatDate(actual).substring(0,10)+" "+startTime);			
			if (sTime.before(actual)) isCandidate=true;			
		}catch (Exception e) {
			this.addLogError("READING START TIME", startTime, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	
	// -- is interval in schedule ?? -- \\
	private Boolean isIntervalCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.INTERVAL)) return true; // isnt select
	
		boolean isCandidate=false;		
		String dtStrIni=jUtil.formatDate(scheduler.getLastExecution()).substring(0,10) + " " + jUtil.formatDate("HH:mm", scheduler.getLastExecution());
		String dtStrFim=jUtil.formatDate(actual).substring(0,10) + " " + jUtil.formatDate("HH:mm",actual);
		
		try {
			long dif=jUtil.getDateDif(jUtil.MINUTE,jUtil.strToTimestamp(dtStrIni),jUtil.strToTimestamp(dtStrFim));
			long interval=new Long(scheduler.getString(JwideSchedulerProperties.INTERVAL).trim());
			isCandidate=(dif>=interval);	
		} catch (Exception e) {
			this.addLogError("READING INTERVAL ", scheduler.getString(JwideSchedulerProperties.INTERVAL).trim(), e);
			isCandidate=false;
		}

		return isCandidate;
	}	
		
	private Boolean isContextCandidate(Scheduler scheduler) {
		if (!scheduler.exists(JwideSchedulerProperties.CONTEXT)) return true; 
		return (this.getContextPath().replace("/","").equals(scheduler.getString(JwideSchedulerProperties.CONTEXT)));
	}
	
	private Boolean isRunOnceCandidate(Scheduler scheduler) {
		if (!scheduler.exists(JwideSchedulerProperties.RUN_ONCE)) return true; 
		return (scheduler.getRunCount()==0);
	}	
	
	// -- is end time in schedule ?? -- \\
	private Boolean isEndTimeCandidate(Scheduler scheduler, Timestamp actual) {
		if (!scheduler.exists(JwideSchedulerProperties.END_TIME)) return true; // isnt select
		
		String endTime = scheduler.getString(JwideSchedulerProperties.END_TIME);
	
		Boolean isCandidate=false;
		
		try {
			Timestamp eTime=jUtil.strToTimestamp(jUtil.formatDate(actual).substring(0,10)+" "+endTime);			
			if (eTime.after(actual)) isCandidate=true;			
		}catch (Exception e) {
			this.addLogError("READING END TIME", endTime, e);
			isCandidate=false;
		}
		return isCandidate;
	}
	// -- is Host Ip  equals ?? -- \\
	private Boolean isHostIpCandidate(Scheduler scheduler) {
		if (!scheduler.exists(JwideSchedulerProperties.HOST_IP)) return true; 		
				
		try {
			return jUtil.getMyIP().equals(scheduler.getString(JwideSchedulerProperties.HOST_IP).trim());		
		}catch (Exception e) {
			this.addLogError("READING IP ADDRESS", "", e);			
		}
		return false;
	}	
	
	private Boolean isHostNameCandidate(Scheduler scheduler) {	
		if (!scheduler.exists(JwideSchedulerProperties.HOST_NAME)) return true; // qq host name est? liberado		
		
		try {
			return jUtil.getMyHostName().equals(scheduler.getString(JwideSchedulerProperties.HOST_NAME).trim());		
		}catch (Exception e) {
			this.addLogError("READING HOST NAME", "", e);			
		}
		return false;
	}	
	
	private boolean isTimeBetween(String time, Timestamp last, Timestamp actual){
		if (time==null) return false;
		if (time.length()==04) time = "0" + time; 
		Timestamp date = jUtil.strToTimestamp(jUtil.formatDate(actual).substring(0,10) + " " + time.trim());
		
		return ( date.compareTo(last)>=0 && date.compareTo(actual)<=0 );
	}
	
	private void addLogError(String description, String message, Exception e) {
		Log.writeLog(this.getClass(), e, message);
	}
	
	private void addLogInfo(String description, String message) {
		Log.writeLog(JwideLog.LogType.INFO, this.getClass().getName() + "\n" + message +" on " + description);
	}
		
	private void executeProcess(Scheduler scheduler){
		String key=null;
		boolean on_execute = false;
		
		try {
			String param = (scheduler.exists(JwideSchedulerProperties.METHOD_PARAM)?scheduler.getString(JwideSchedulerProperties.METHOD_PARAM):"");		
			key = this.formatKey(scheduler.getString(JwideSchedulerProperties.CLASS), scheduler.getString(JwideSchedulerProperties.METHOD), param);
			on_execute = Stack.insertIfNotExists(key, key);
			if (on_execute){
				this.invokeMethod(scheduler);
				JwideDataSource.commit();
				this.addLogInfo(key,"Success");
			}
		} catch (Exception e){
			if (on_execute){
				JwideDataSource.rollback();
			}
			this.addLogError("EXECUTE PROCESS", key, e);			
		} finally {
			if (on_execute){	
				JwideDataSource.close();	
				scheduler.updateExecute();						
				Stack.remove(key);				
			}
		}
	}
	
	private String formatKey(String clazz, String method, String param){
		return "Class: " + clazz + " Method: " + method + (!param.equals("")?(" Param: " + param):"");
	}
	
	private void invokeMethod(Scheduler scheduler) throws Exception {		
		String param=this.getParamValue(scheduler);
		this.invokeMethod(scheduler.getString(JwideSchedulerProperties.CLASS),scheduler.getString(JwideSchedulerProperties.METHOD),param);
	}
	
	private void invokeMethod(String className, String methodName, String param) throws Exception {			

		Constructor<?> constructor = Class.forName(className).getConstructor();	
		if (param!=null){
			Reflect.invokeMethod(constructor.newInstance(),methodName,String.class,param);
		} else {
			Reflect.invokeMethod(constructor.newInstance(),methodName);
		}			
	}
	
	private Timestamp strToTimestamp(String dateStr) {
		if (dateStr==null) return null;	
		if (dateStr.length()==10) dateStr=dateStr+" 00:00:00";
	    if (dateStr.length()==16) dateStr=dateStr+":00";
	    return jUtil.strToTimestamp(dateStr,JwideSchedulerProperties.DATE_FORMAT);
	}
	
	private String getParamValue(Scheduler scheduler){
		if (scheduler.exists(JwideSchedulerProperties.METHOD_PARAM)){
			return scheduler.getString(JwideSchedulerProperties.METHOD_PARAM);
		} else {
			return null;
		}
	}	
	
	private String getContextPath(){
		return this.contextPath;
	}

}
