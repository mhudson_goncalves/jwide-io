/*
 * Created on 23/02/2005
 */

package jwide;

import java.util.Arrays;
import java.util.List;

/**
 * @author MHRG
 */

public interface JwideConsts {	
	
	public static final String XML_ENCODING="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	public static final String ACTION_METHOD_PREFIX = "on";
	
	public final String SPLIT="|";
	
	public final String LAST_PAGE="_LAST_PAGE";
	public final String ERRORS="Errors";		
	
	public final String VO_SUFIX="VO";	
	public final String CONTROLLER_SUFIX="Controller";
	public final String URL_ACTION_PREFIX="action.";	
	
    public final List<String> JSON_CONTENT_TYPES = Arrays.asList("application/json");
    public final List<String> XML_CONTENT_TYPES = Arrays.asList("text/xml","application/xml");   
    
    public static final String CONTROLLER_FILTER_DOT = ".jw";
	public static final String CONTROLLER_FILTER_BAR = "api"; // "/jd
	
}
