package jwide;

public abstract class StoreRealPath {
	private static ThreadLocal<String> storageRealPath = new ThreadLocal<String>();
	
	public static String getRealPath(){
		return storageRealPath.get();
	}
	public static void setRealPath(String realPath){
		storageRealPath.set(realPath);
	}
} 
