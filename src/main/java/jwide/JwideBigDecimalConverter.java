package jwide;

import java.math.BigDecimal;

import javax.json.JsonNumber;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

public class JwideBigDecimalConverter implements Converter {

	JwideUtil jwUtil = JwideUtil.getInstance();

	public JwideBigDecimalConverter() {   	
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.beanutils.Converter#convert(java.lang.Class,
		   java.lang.Object)
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object convert(Class type, Object value) throws ConversionException {
		if (value == null) return null;
		
		if (value instanceof BigDecimal) {
			return value;         
		}
		else if (value instanceof String) {
			try {
				return new BigDecimal(value.toString().trim().replace(",",""));
			} catch (Exception e) {
				return null;
			}
		} else if (value instanceof JsonNumber){
			return ((JsonNumber) value).bigDecimalValue();
		} else {
			throw new ConversionException("Type not supported: " +	value.getClass().getName());
		}
	}

}
