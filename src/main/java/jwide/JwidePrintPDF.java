package jwide;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.print.attribute.PrintRequestAttributeSet;

import jwide.JwidePrintHandler;
import jwide.JwideRandom;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFRenderer;

public class JwidePrintPDF {

	private static final String JOB_PREFIX = "PDFDOC";
	
	private PrinterJob pjob = null;
	private JwideRandom Random = new JwideRandom();
	private JwidePrintHandler pHandler = new JwidePrintHandler();
	private boolean duplex=false;
	
	public static void main(String[] args) throws IOException, PrinterException {
		if (args.length != 1) {
			System.err.println("JwidePrintPDF: pdffile.dpf, [printer name]");
		}
		String printerName=null;
		
		if (args.length==2) printerName=args[1];
		
		System.out.println("Printing: " + args[0]);
		// Create a PDFFile from a File reference
		FileInputStream fis = new FileInputStream(args[0]);
		JwidePrintPDF printPDFFile = new JwidePrintPDF(fis);
		
		if (printerName==null)	printPDFFile.print();
		else                    printPDFFile.print(printerName);
	}
	
	public JwidePrintPDF(){		
	}
	
	public JwidePrintPDF(InputStream inputStream) throws IOException, PrinterException {
		byte[] pdfContent = new byte[inputStream.available()];
		inputStream.read(pdfContent, 0, inputStream.available());
		initialize(pdfContent);
	}
	
	public JwidePrintPDF(byte[] content) throws IOException, PrinterException {
		initialize(content);
	}
	
	public void setDuplex(boolean duplex){
		this.duplex=duplex;
	}
	public boolean isDuplex(){
		return this.duplex;
	}
	
	public void print(byte[] content) throws Exception {
		this.initialize(content);
		this.print();
	}
	
	public void print(byte[] content,String printerName) throws Exception {
		this.initialize(content);
		this.print(printerName);
	}
	
	public void print() throws PrinterException {		
		pjob.print(this.createAttributeSet());		
	}
	
	public void print(String printerName) throws PrinterException {
		pjob.setPrintService(pHandler.createPrintService(printerName));
		pjob.print(this.createAttributeSet());
	}	
		
	private void initialize(byte[] pdfContent) throws IOException, PrinterException {
		ByteBuffer bb = ByteBuffer.wrap(pdfContent);
		// Create PDF Print Page
		PDFFile pdfFile = new PDFFile(bb);
		PDFPrintPage pages = new PDFPrintPage(pdfFile);

		// Create Print Job
		pjob = PrinterJob.getPrinterJob();
		PageFormat pf = PrinterJob.getPrinterJob().defaultPage();
		pjob.setJobName(this.createJobName());
		
		Book book = new Book();
		book.append(pages, pf, pdfFile.getNumPages());
		pjob.setPageable(book);		

		// to remove margins
		Paper paper = new Paper();		
		
		paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight());
		//
		// double margin = 36; // half inch
		 //   paper.setImageableArea(8, 8, paper.getWidth() - margin * 2, paper.getHeight()
		  //      - margin * 2);
        paper.setImageableArea(paper.getImageableX(),paper.getImageableY(), paper.getWidth(), paper.getHeight());
		pf.setPaper(paper);
	}		
	
	private String createJobName(){
		return JOB_PREFIX+"-"+Random.range(Integer.MAX_VALUE);		
	}
	
	private PrintRequestAttributeSet createAttributeSet(){
		PrintRequestAttributeSet aSet = pHandler.createAttributeSet();
		if (this.isDuplex()) pHandler.setDuplex(aSet);
		
		return aSet;
	}
}

/**
 * Class that actually converts the PDF file into Printable format
 */
class PDFPrintPage implements Printable {

	private PDFFile file;

	PDFPrintPage(PDFFile file) {
		this.file = file;
	}

	public int print(Graphics g, PageFormat format, int index) throws PrinterException {
		int pagenum = index + 1;
		if ((pagenum >= 1) && (pagenum <= file.getNumPages())) {
			Graphics2D g2 = (Graphics2D) g;
			PDFPage page = file.getPage(pagenum);

			// fit the PDFPage into the printing area
			Rectangle imageArea = new Rectangle((int) format.getImageableX(), (int) format.getImageableY(),
					(int) format.getImageableWidth(), (int) format.getImageableHeight());
			g2.translate(0, 0);
			PDFRenderer pgs = new PDFRenderer(page, g2, imageArea, null, null);
			try {
				page.waitForFinish();
				pgs.run();
			} catch (InterruptedException ie) {
				// nothing to do
			}
			return PAGE_EXISTS;
		} else {
			return NO_SUCH_PAGE;
		}
	}
}