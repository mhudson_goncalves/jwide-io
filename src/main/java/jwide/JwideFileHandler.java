package jwide;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class JwideFileHandler {	
	
	private static long MAX_IO_CHUNK_SIZE = 10000;
	
	/** Creates a new instance of JwideFileManipulation */
	public JwideFileHandler() {	
	}
	
	public void createFileIfNotExists(String file) throws Exception {
		if (!this.isFileCreated(file)) {
			File f = new File(file);  
			f.createNewFile();			
		}		
	}
	
	public JwideFileWriter newFileWriter(String file) throws Exception {
		return new JwideFileWriter(file);
	}
	
	public JwideFileReader newFileReader(String file) throws Exception {
		return new JwideFileReader(file);
	}
	
	public String loadFileToString(String fileName) throws Exception {
		JwideFileReader reader = this.newFileReader(fileName);
		StringBuffer arqStr=new StringBuffer("");
        int l=0;
		String errMessage=null;
		try {			
			while (reader.hasNextLine()) {
				l++;
				if (l>1) arqStr.append("\n");
				arqStr.append(reader.readLine());				
			}		
		}
		catch (Exception e) {
			errMessage=e.getMessage();	   
		}
		finally {
			reader.close();	   
		}
		if (errMessage!=null) throw new FileNotFoundException(errMessage);
				  
		return arqStr.toString();	
	}
	
	public boolean isFileCreated(String FileName) {
		File f = new File(FileName);
		boolean e=f.exists();
		return e;
	}
	
	public void createFolderIfNotExists(String folder) throws Exception {
		if (!this.isFileCreated(folder)) {
			File fdr = new File(folder);  
			fdr.mkdirs();
		}
	}
	
	public boolean deleteFile(String FileName) {
		return (new File(FileName)).delete();		
	}
	
	public String[] getTextInLine(String Text) {
		// pega um texto continuo (pode ser vindo de em arquivo)
		// e transforma cada quebra de linha em um elemento no array de strings
		
		String[] TextInLine;
		int qty=0;
		char p;
		
		// conta a qtd de linhas quebradas
		for (int i=0;i<Text.length();i++) {
			p=Text.charAt(i);
			if (p=='\n') qty++;            
		}
		qty++;
		TextInLine= new String[qty];
		// zerar todas as linhas
		for (int i=0;i<qty;i++) TextInLine[i]="";
		
		// incluir linha a linha
		qty=0;
		String s="";
		for (int i=0;i<Text.length();i++) {
			p=Text.charAt(i);            
			if (p=='\n') {
				TextInLine[qty]=s;
				qty++;   
				s="";
			}
			else s=s+p;
		}
		// ultima quebra
		TextInLine[qty]=s;
		return TextInLine;
	}
	
	public String[] listFilesFromFolder(String folderPath) {
		File f = new File(folderPath);
		File[] files=f.listFiles();
		
		ArrayList<String> fileNames = new ArrayList<String>();
		
		for (int i=0;i<files.length;i++) {
			if (files[i].isFile()) {				
				fileNames.add(files[i].getName());				
			}
		}
		
		String[] names = new String[fileNames.size()];
		for (int i=0;i<fileNames.size();i++) {
			names[i]=fileNames.get(i).toString();
		}
		return names;		
	}
	
	public Long fileSize(String filePath) {
		File f = new File(filePath);
		return f.length();		
	}
	
	public String[] listDirectoriesFromFolder(String folderPath) {
		File f = new File(folderPath);
		File[] files=f.listFiles();
		
		ArrayList<String> dirNames = new ArrayList<String>();
		
		for (int i=0;i<files.length;i++) {
			if (files[i].isDirectory()) {				
				dirNames.add(files[i].getName());				
			}
		}
		
		String[] names = new String[dirNames.size()];
		for (int i=0;i<dirNames.size();i++) {
			names[i]=dirNames.get(i).toString();
		}
		return names;		
	}
	
	public Timestamp getLastModifiedDate(String fileName){
	   File file = new File(fileName);
	   return new Timestamp(file.lastModified());
	}	
	
	public void copyFile(String fileIn, String fileOut) throws Exception {	
		
		File fIn=new File(fileIn);
		
		FileChannel sourceChannel=null;
		FileChannel destinationChannel=null;	
		
		FileInputStream fileInputStream = new FileInputStream(fIn);
		FileOutputStream fileOutputStream =  new FileOutputStream(new File(fileOut));				
	
		sourceChannel = fileInputStream.getChannel();	
		destinationChannel = fileOutputStream.getChannel();
		sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
		
		fileInputStream.close();
		fileOutputStream.close();
		
		sourceChannel.close();
		destinationChannel.close();		
	}
	
	public void copyLargeFile(String fileIn, String fileOut) throws Exception{   
				
		FileInputStream fileInputStream=new FileInputStream(new File(fileIn));
		FileOutputStream fileOutputStream= new FileOutputStream(new File(fileOut));		
		
		FileChannel sourceChannel = fileInputStream.getChannel();
		FileChannel destinationChannel = fileOutputStream.getChannel();
		int position=0;  
		long count = sourceChannel.size();
		long readBytes;		

		while (position < count) {
			long chunk = Math.min(MAX_IO_CHUNK_SIZE, count - position);
			readBytes = sourceChannel.transferTo(position, chunk, destinationChannel);
			position += readBytes;
		}
		
		fileInputStream.close();
		fileOutputStream.close();
	}
	
	public void moveFile(String fileIn, String fileOut) throws Exception {
		File f = new File(fileIn);
		f.renameTo(new File(fileOut));
	}
	
	public List<String> listAllDirectoriesFromFolder(String rootPath) {		
		 JwideDTO folders = new JwideDTO();
		 folders.setColumns(new String[]{"folder"});

		 folders.addRow(new Object[]{rootPath});
		 folders.beforeFirst();
		 while (folders.next()) {
			 String actualFolder=folders.getString("folder");
			 String[] dir=this.listDirectoriesFromFolder(actualFolder);
			 for (int i=0;i<dir.length;i++) {
				 folders.addRow(new Object[]{actualFolder+"/"+dir[i]});
			 }	 
		 }
		 List<String> foldersList = new ArrayList<String>();
		 folders.beforeFirst();
		 while (folders.next()) {
			 foldersList.add(folders.getString("folder"));
		 }
		 return foldersList;
	 }	
	
	public void replaceParamsInFile(String oldFileName, String newFileName,String delimiter,List<String> params,List<String> values) throws Exception{
		Pattern tokenPattern = Pattern.compile("\\"+delimiter+"(\\w+)\\"+delimiter); //#NOME#	

		JwideFileHandler fh = new JwideFileHandler();
		StringBuffer newValue = new StringBuffer();
		String content=fh.loadFileToString(oldFileName);
		Matcher tokenMatcher = tokenPattern.matcher(content);

		while (tokenMatcher.find()) {	
			int index=0;
			for (String p:params){				   
				for (int i=1;i<=tokenMatcher.groupCount();i++){
					String param=tokenMatcher.group(i).trim();
					if (p.equalsIgnoreCase(param)){						
						tokenMatcher.appendReplacement(newValue,this.replaceInvalidChar(values.get(index)));	
					} 
				}
				index++;
			}
		}
		tokenMatcher.appendTail(newValue);
		
		JwideFileWriter writer = this.newFileWriter(newFileName);
		writer.write(newValue.toString());
		writer.close();
	}
	
	private String replaceInvalidChar(String value) {
		if (value == null) return ""; 
		
		value = value.replace("$", "\\$"); // Isso elimina um erro que existia na substitui??o de par?metros que tinham o [ $ ] em seu conte?do. 
		
		return value;
	}
	
	public void replaceStringInFile(String fileName, String match, String replacingString) throws Exception{

		File file = new File(fileName);
		if(file.isDirectory()||!file.exists()){				
			return;
		}
		file = new File(fileName);
		RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
		long fpointer = randomAccessFile.getFilePointer();
		String lineData = "";
		while((lineData =randomAccessFile.readLine()) != null){
			fpointer = randomAccessFile.getFilePointer() - lineData.length()-2;
			if(lineData.indexOf(match) > 0){
				randomAccessFile.seek(fpointer);
				randomAccessFile.writeBytes(replacingString);
				// if the replacingString has less number of characters than the matching string line then enter blank spaces.
				//if(replacingString.length() < style="color: rgb(153, 0, 0);">int difference = (lineData.length() - replacingString.length())+1;
				//for(int i=0; i < style="color: rgb(51, 51, 255);">" ");
			}
		}
		randomAccessFile.close();
	}
	    
	public void zipFiles(List<String> fileNames, String outFileName,Boolean keepFolderHierarchy) throws Exception {
		byte[] bufRead = new byte[1024];

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFileName));

		for (String fileName:fileNames) {
			File file = new File(fileName);
			FileInputStream in = new FileInputStream(file);

			String zipentry = (keepFolderHierarchy)?fileName:file.getName();
			out.putNextEntry(new ZipEntry(zipentry));
			int len;

			while ((len = in.read(bufRead)) > 0) {
				out.write(bufRead, 0, len);
			}
			out.closeEntry();
			in.close();
		}
		out.close();        
	}

	public void zipFolder(String folder, String outFileName, Boolean keepFolderHierarchy) throws Exception {

		File inFolder = new File(folder);
		File outFolder = new File(outFileName);
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
		BufferedInputStream in = null;
		byte[] data    = new byte[1000];
		String files[] = inFolder.list();
		for (String file:files) {
			// CurrentFile -> PASTA OU ARQUIVO
			File currentFile = new File(inFolder.getPath() + "/" + file);

			if (currentFile.isDirectory()){ // SE FOR PASTA
				String subfiles[] = currentFile.list();
				for (String subfile:subfiles) {
					in = new BufferedInputStream(new FileInputStream(currentFile.getPath() + "/" + subfile), 1000);
					String zipentry = (keepFolderHierarchy)?currentFile.getPath()+"/"+subfile:file+"/"+subfile;
					out.putNextEntry(new ZipEntry(zipentry));
					int count;
					while((count = in.read(data,0,1000)) != -1){
						out.write(data, 0, count);
					}
					out.closeEntry();
				}
			} else { // SE FOR ARQUIVO
				in = new BufferedInputStream(new FileInputStream(inFolder.getPath() + "/" + file), 1000);
				String zipentry = (keepFolderHierarchy)?folder+"/"+file:file;
				out.putNextEntry(new ZipEntry(zipentry));
				int count;
				while((count = in.read(data,0,1000)) != -1){
					out.write(data, 0, count);
				}
				out.closeEntry();
			}
		}
		out.flush();
		out.close();        
	}
	
	public static byte[] loadFileToBytes(String fileName) throws IOException {
		File file = new File(fileName);
	    InputStream is = new FileInputStream(file);

	    // Get the size of the file
	    long length = file.length();

	    // You cannot create an array using a long type.
	    // It needs to be an int type.
	    // Before converting to an int type, check
	    // to ensure that file is not larger than Integer.MAX_VALUE.
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }

	    // Create the byte array to hold the data
	    byte[] bytes = new byte[(int)length];

	    // Read in the bytes
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    // Ensure all the bytes have been read in
	    if (offset < bytes.length) {
	    	is.close();
	        throw new IOException("Could not completely read file "+file.getName());
	    }

	    // Close the input stream and return bytes
	    is.close();
	    return bytes;
	}

	public class JwideFileWriter {

		private Charset UTF_8 = Charset.forName("UTF-8");
		private OutputStream outputStream = null;
		private WritableByteChannel channel = null;

		public String file;

		public JwideFileWriter(String file) throws Exception {
			this.file=file;
			createFileIfNotExists(file);
			this.open(file);
		}

		private void open(String file) throws Exception {
			channel = Channels.newChannel(new FileOutputStream(file));
			outputStream = Channels.newOutputStream(channel);
		}

		public void writeLn(String content) throws Exception {
			this.write(content);
			this.write("\n");				
		}

		public void write(String content) throws Exception {
			outputStream.write(content.getBytes(UTF_8));			
			outputStream.flush();				
		}		

		public void close() {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (Exception e){}
			try {
				if (channel!=null) {
					channel.close();
				}
			} catch (Exception e){}
		}		
	}
	
	public class JwideFileReader {
		
		//private FileInputStream inputStream = null;
		
		private InputStreamReader inputStream = null;
		private Scanner sc = null;
		private Charset charset = Charset.forName("UTF-8");
		
		public String file;
		
		public JwideFileReader(String file) throws Exception {
			this.file=file;
			this.open(file);
		}
		
		private void open(String file) throws Exception {
			inputStream = new InputStreamReader(new FileInputStream(file), charset); //new FileInputStream(file, "UTF-8"); //new FileInputStream(file);
			sc = new Scanner(inputStream);
		}
		
		public boolean hasNextLine() throws Exception {
			return sc.hasNextLine();
		}		
		
		public String readLine() throws Exception {			
			return sc.nextLine();
		}
		
		public void close() {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e){}
			try {
				if (sc != null) {
					sc.close();
				}
			} catch (Exception e){}
		}		
	}


}


