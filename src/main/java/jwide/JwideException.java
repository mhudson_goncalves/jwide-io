package jwide;

public class JwideException extends Exception {

	private static final long serialVersionUID = 1L;

	public JwideException() {		
	}

	public JwideException(String arg0) {
		super(arg0);
	}

	public JwideException(Throwable cause) {
		super(cause);
	}

	public JwideException(String message, Throwable cause) {
		super(message, cause);
	}

	public JwideException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
