package jwide;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class JwideLookupResources {	
	
	private List<String> names = new ArrayList<>();
	private List<Connection> connections = new ArrayList<>();
	private List<DataSource> datasources = new ArrayList<>();
	
	// DataSource
	protected JwideLookupResources() {
	}

	protected Connection getConnection(String name) throws Exception {
		
		Connection Con=null; 
		
		if (!names.contains(name)){
			DataSource ds = null;
			Connection conn=null;			
			
			ds = (DataSource)new InitialContext().lookup(JwideUtil.JNDI_LOOKUP_PREFIX + name);
			
			if (ds!=null){
				conn = ds.getConnection();
			} else {
				throw new IllegalArgumentException("JwideDataSource.getConnection: Invalid DataSource Name: " + (name!=null?name:""));
			}
			
			names.add(name);
			datasources.add(ds);
			connections.add(conn);
		}
		
		Con = connections.get(names.indexOf(name));
		if (Con==null){
			Con = datasources.get(names.indexOf(name)).getConnection();
		}

		if (Con==null){
			throw new IllegalArgumentException("JwideDataSource.getConnection: Invalid DataSource Name: " + (name!=null?name:""));
		} else {
			Con.setAutoCommit(false);
		}
		
		return Con;		
	}	

	protected void commit(){
		for (Connection Con:connections){
			try {
				Con.commit();
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
	}

	protected void rollback(){
		for (Connection Con:connections){
			try {
				Con.rollback();
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
	}
	
	protected void close(){
		
		for (Connection Con:connections){
			try {
				Con.close();
			} catch (Exception e) {
			} finally {
				Con = null;
			}
		}
		
		connections.clear();
		datasources.clear();
		names.clear();		
	}
	
}
