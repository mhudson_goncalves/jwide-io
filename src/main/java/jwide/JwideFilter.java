package jwide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jwide.generate.JwideGenerateUtils;

/**
 * Servlet Filter implementation class JwideFilter
 */
@WebFilter(urlPatterns = { "/api/*", "*.jw"})
public class JwideFilter implements Filter {
	
	private JwideUtil jUtil = JwideUtil.getInstance();
	private JwideGenerateUtils gUtil = JwideGenerateUtils.getInstance();
	private JwideTimer Timer = new JwideTimer();	
	
	private FilterConfig config;

    public JwideFilter() {       
    }
	
	public void destroy() {		
	}
	
	@Override
	public void init(FilterConfig config) {
		this.config = config;		
		// criar jwidescheduler
		this.startScheduler(config.getServletContext());		
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest=(HttpServletRequest)request;
		HttpServletResponse httpResponse= (HttpServletResponse)response;		
		/* CORS -> cross browser
		httpResponse.addHeader("Access-Control-Allow-Origin", "*");
		httpResponse.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
		httpResponse.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        */
		JwideControllerHandler control = new JwideControllerHandler(httpRequest,httpResponse,config.getServletContext());  
		control.setControllerName(this.extractControllerName(httpRequest));
		control.setActionName(this.extractActionName(httpRequest));
		
		try {			 
			// injetar o realpath e contexto
			this.setPaths( request.getServletContext());
			control.execute();
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	private void setPaths(ServletContext context){
		JwideStoreRealPath.setRealPath(context.getRealPath(""));
		JwideStoreRealPath.setContextPath(context.getContextPath());		
	}
	
	private void startScheduler(ServletContext context){
		JwideSchedulerTask Scheduler = new JwideSchedulerTask(context.getContextPath());
		if (Scheduler.isSchedulerValid()){
			Timer.addTimerTask(Scheduler, JwideSchedulerProperties.SECONDS_SCHEDULER);
		}
	}
	
	private String extractControllerName(HttpServletRequest request){	
		return gUtil.extractControllerName(this.formatUri(request));
	}
	
	private String formatUri(HttpServletRequest request){
		
		String context = request.getServletContext().getContextPath().replace("/", "").trim();
		List<String> url = new ArrayList<>();
		
		for (String u:jUtil.lineToArray(request.getRequestURI(),"/")){
			if (u==null || u.trim().equals("")){
				continue;
			} else if (!context.equals("") && url.size()==0 && u.equals(context)){
				continue;
			}
			url.add(u);
		}
		StringBuilder uri = new StringBuilder();
		for (String u:url){
			if (uri.length()>0){
				uri.append("/");
			}
			uri.append(u);
		}
		return uri.toString();
	}
	
	private String extractActionName(HttpServletRequest request){
		List<String> url=jUtil.lineToArray(this.formatUri(request), "/");
		StringBuilder action = new StringBuilder();
		
		if (url.get(0).equals(JwideConsts.CONTROLLER_FILTER_BAR)){
			url.remove(0); // /jd
			action.append(gUtil.toCamelCase(url.get(url.size()-1))); // action
		} else {
			Map<String,String[]> inputParams = request.getParameterMap();
			if (inputParams != null) {
				Iterator<String> iter = inputParams.keySet().iterator();
				while (iter.hasNext()){
					String key = (String) iter.next();  
					if (key.indexOf(JwideConsts.URL_ACTION_PREFIX)==0){
						action.append(gUtil.toCamelCase(key.replace(JwideConsts.URL_ACTION_PREFIX, "")));
					}
				}			
			}					
		}	

		return action.toString();
	}

}
