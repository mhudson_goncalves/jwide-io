/*
 * Created on 16/11/2004
 *
 */

package jwide;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import java.beans.PropertyDescriptor;
import jwide.generate.JwideGenerateUtils;

/**
 * @author MHRG
 */

public class JwideExecuteHandler {
	
	private BeanUtilsBean     BeanUtils = BeanUtilsBean.getInstance();
	private PropertyUtilsBean PropUtils = new PropertyUtilsBean();
	private JwideReflection Reflect = JwideReflection.getInstance();
	private JwideGenerateUtils gUtil =  JwideGenerateUtils.getInstance();
	
	protected static final String EMPTY_ERROR_PAGE=""; 
	
	private static final String FROM_REQUEST="fromRequest";
	private static final String FROM_SESSION="fromSession";
	private static final String TO_REQUEST="toRequest";
	private static final String TO_SESSION="toSession";
	
	private PropertyDescriptor[] PropDesc;
			
	public JwideExecuteHandler() {	
     	JwideConvertUtils.getInstance();
	}	

	public void execute(JwideController act,
			            HttpServletRequest  request,
                        HttpServletResponse response,
                        ServletContext      context, 					      
					    String methodName,
					    Map<String,Object> paramValues){
	
		// pegar as propriedades da classe
		PropDesc=PropUtils.getPropertyDescriptors(act);		
		
		try { act.setRealPath(context.getRealPath(""));}
		catch (Exception e) { act.setSessionID("");}
		
		try { act.setRequest(request);}
		catch (Exception e) {act.setRequest(null);}		
		
		try { act.setParams(this.paramValuesToParamsClass(paramValues));}
		catch (Exception e){}
				
        boolean castError=false;
		try {					
			BeanUtils.populate(act,paramValues);
		}
		catch (Exception e) {
			e.printStackTrace();
			castError=true;	
		}		
		
		act.validator.getErrors().clearErrors(); // sempre limpar os erros antes de executar
		
		// injetar as variaveis de sessao e request
		if (!castError) {
			for (int i=0; i< PropDesc.length; i++) {
				String str = PropDesc[i].getName();
				
				if ("class".equals(str)) {
					continue; // Se for uma classe ocasionara em erro					
				}		      
				
				// se a propriedade � de grava��o (setXXX)
				if (PropUtils.isWriteable(act,str)) {			
					// Setar os parametros do request
					if ((str.length()>11) && (str.substring(0,11).equals(FROM_REQUEST))) {
						
						try {					   
							Object obj=request.getAttribute(gUtil.firstLower(str.replaceFirst(FROM_REQUEST,"")));					
							if (obj!=null) {
								PropUtils.setSimpleProperty(act,str,obj);
							}											      
						} catch (Exception e){
							act.validator.getErrors().addError(e.getMessage());
							e.printStackTrace();			
						}
					}
					// Setar os parametros da sess�o
					if ((str.length()>11) && (str.substring(0,11).equals(FROM_SESSION))) {
						try {
							Object obj= request.getSession().getAttribute(gUtil.firstLower(str.replaceFirst(FROM_SESSION,"")));							
							if (obj!=null) {
								PropUtils.setSimpleProperty(act,str,obj);
							}											      
						} catch (Exception e){
							act.validator.getErrors().addError(e.getMessage());
							e.printStackTrace();			
						}						
					}
				}
			}		   
		}
		
		// executar o init
		try {
			act.init();
		} catch (Exception e) {	
			act.validator.addError("Cannot Invoke Method: init");
			act.validator.assign(e);
			e.printStackTrace();		
		}
		
		// executar as rotinas
		try {
			act.setLastResult(Reflect.invokeMethod(act, methodName).toString());
		} catch (Exception e) {	
			act.validator.assign(e);
			act.setLastResult(JwideController.TO_JSON);
		}	
		
		// se houver error de cast no beanutils, acrescentar
		if (castError) act.validator.getErrors().addError("error.mismatch"); // tipo de valores incompativeis
		
		// se n?o houver erros incluir as rotinas executadas
		if (act.validator.getErrors().isEmpty()) {
		   for (int i=0; i< PropDesc.length; i++) {
			   String str = PropDesc[i].getName();
			   
			   if ("class".equals(str)) {
                   continue; // Se for uma classe ocasionara em erro
               }		      
			   
			   // se a propriedade ? de leitura (getXXX)
			   if (PropUtils.isReadable(act,str)) {			
				   // Colocar parametros em request
				   if ((str.length()>9) && (str.substring(0,9).equals(TO_REQUEST))) {
					   
					   try {					   
						   Object obj=PropUtils.getSimpleProperty(act,str);	
						   request.setAttribute( gUtil.firstLower(str.replaceFirst(TO_REQUEST,"")),obj);			      
					   } catch (Exception e){
						   e.printStackTrace();			
					   }
				   }
				   // Colocar parametros em sessao
				   if ((str.length()>9) && (str.substring(0,9).equals(TO_SESSION))) {
					   try {
						   Object obj=PropUtils.getSimpleProperty(act,str);			          
						   request.getSession().setAttribute( gUtil.firstLower(str.replaceFirst(TO_SESSION,"")),obj);
					   } catch (Exception e){
						   e.printStackTrace();					   
					   }
				   }
			   }
			}		   
		}		
	}
	
	private JwideParam paramValuesToParamsClass(Map<String,Object> paramValues){
		JwideParam param = new JwideParam();
		param.populate(paramValues);
		return param;
	}
	
}
