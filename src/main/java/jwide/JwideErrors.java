/*
 * Created on 29/10/2004
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */
package jwide;

/**
 * @author MHRG
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.util.ArrayList;
import java.util.List;

public class JwideErrors {
	
    private List<List<?>> erros = new ArrayList<List<?>>(0);
		
    public JwideErrors() {		
	}       
		
	public void addError(String id) {
		if (id==null) id="Error Not Indicated";		
		addError(id,new String[]{});
	}
	
	public void addError(String id,String[] params) {
		List<Object> row = new ArrayList<Object>();
		row.add(id);
		row.add(this.replaceInvalidChars(params));
		erros.add(row);
	}
		
	public List<?> getErrorList() {
		return erros;
	}	
	
	public void clearErrors() {
		erros.clear();
	}
	
	public int getErrorsCount() {
		return erros.size();
	}
	
	public boolean isEmpty() {
		return (erros.size()==0);
	}
	
	protected String[] replaceInvalidChars(String[] chars){
		
		List<String> news = new ArrayList<String>();
		for (String c:chars){
			news.add(c!=null?c.replace("&","E"):"");
		}
		
		return news.toArray(new String[]{});		
	}

}