package jwide;

import java.util.Timer;
import java.util.TimerTask;

public class JwideTimer {
  private Timer timer = new Timer();
  private static final int MILISEC = 1000;  
    
  public JwideTimer() {
  }
  
  public void addTimerTask(TimerTask task,Long delaySeconds,Long repeatEverySeconds){
	  timer.scheduleAtFixedRate(task,(delaySeconds*MILISEC),(repeatEverySeconds*MILISEC));
  }
  public void addTimerTask(TimerTask task,Long repeatEverySeconds){
	  addTimerTask(task,new Long(1),repeatEverySeconds);
  }
}
