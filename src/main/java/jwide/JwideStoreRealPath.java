package jwide;

import javax.servlet.http.HttpServletRequest;

public abstract class JwideStoreRealPath {
	private static ThreadLocal<String> storageRealPath = new ThreadLocal<String>();
	private static ThreadLocal<String> storageContextPath = new ThreadLocal<String>();
	
	public static void storePaths(HttpServletRequest request){
		JwideStoreRealPath.setRealPath(request.getSession().getServletContext().getRealPath(""));
		JwideStoreRealPath.setContextPath(request.getSession().getServletContext().getContextPath());		
	}
	
	public static String getRealPath(){
		return storageRealPath.get();
	}
	public static void setRealPath(String realPath){
		storageRealPath.set(realPath);
	}
	
	public static String getContextPath(){
		return storageContextPath.get();
	}
	public static void setContextPath(String contextPath){
		storageContextPath.set(contextPath);
	}
} 
