package jwide;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

public class JavaTypesDTO extends JwideDTO {
	
	private static final Integer ORACLE_CURSOR = -10;
	
	public JavaTypesDTO(){	
		init();
	} 
	
	public String getJavaType(String typeName, int Type, Integer Size, Integer Decimals) {
		if (Size==null) Size=0;
		if (Decimals==null) Decimals=0;
		
		if (typeName.equalsIgnoreCase("REF CURSOR")){
			this.locate("id",ORACLE_CURSOR);
		} else {
			this.locate("id",Type);
			String jType=this.getString("description");
			if (jType.equalsIgnoreCase("Double") && (Size<=6) && (Decimals==0)) {
				this.locate("id",java.sql.Types.INTEGER);
			}	
		}
		return this.getString("description");
	}
	
	public Integer getJavaType(Object object) {
		Integer type=java.sql.Types.OTHER;		

		     if (object instanceof Long)              type=java.sql.Types.BIGINT;
		else if (object instanceof BigDecimal)        type=java.sql.Types.NUMERIC;
		else if (object instanceof Byte)              type=java.sql.Types.BINARY;
		else if (object instanceof Boolean)           type=java.sql.Types.BOOLEAN;
		else if (object instanceof Blob)              type=java.sql.Types.BLOB;
		else if (object instanceof Clob)              type=java.sql.Types.CLOB;
		else if (object instanceof String)            type=java.sql.Types.VARCHAR;
		else if (object instanceof Timestamp)         type=java.sql.Types.TIMESTAMP;
		else if (object instanceof Date)              type=java.sql.Types.DATE;
		else if (object instanceof Time)              type=java.sql.Types.TIME;   	
		else if (object instanceof Double)            type=java.sql.Types.DOUBLE;
		else if (object instanceof Integer)           type=java.sql.Types.INTEGER;
		else if (object instanceof Short)             type=java.sql.Types.SMALLINT;
		else if (object instanceof JwideRefCursorDTO) type=ORACLE_CURSOR;			     

		return type;
	}
	
	public Integer getJavaType(Class<?> clazz) {
		Integer type=java.sql.Types.OTHER;		

		     if (clazz.equals(Long.class))              type=java.sql.Types.BIGINT;
		else if (clazz.equals(BigDecimal.class))        type=java.sql.Types.NUMERIC;
		else if (clazz.equals(Byte.class))              type=java.sql.Types.BINARY;
		else if (clazz.equals(Boolean.class))           type=java.sql.Types.BOOLEAN;
		else if (clazz.equals(Blob.class))              type=java.sql.Types.BLOB;
		else if (clazz.equals(Clob.class))              type=java.sql.Types.CLOB;
		else if (clazz.equals(String.class))            type=java.sql.Types.VARCHAR;
		else if (clazz.equals(Timestamp.class))         type=java.sql.Types.TIMESTAMP;
		else if (clazz.equals(Date.class))              type=java.sql.Types.DATE;
		else if (clazz.equals(Time.class))              type=java.sql.Types.TIME;   	
		else if (clazz.equals(Double.class))            type=java.sql.Types.DOUBLE;
		else if (clazz.equals(Integer.class))           type=java.sql.Types.INTEGER;
		else if (clazz.equals(Short.class))             type=java.sql.Types.SMALLINT;
		else if (clazz.equals(JwideRefCursorDTO.class)) type=ORACLE_CURSOR;

		return type;
	}	
	
	public String getJavaTypeDescription(Object object) {
		Integer type=this.getJavaType(object);		
		this.locate("id",type);	    	
	    return this.getString("description");
	}	
	
	private void init() {
		
		this.setColumns(new String[]{"id","description"});	
	
		this.addRow(java.sql.Types.ARRAY,"Array");
		this.addRow(java.sql.Types.BIGINT,"Long");
		this.addRow(java.sql.Types.BINARY,"byte[]");
		this.addRow(java.sql.Types.BIT,"Boolean");
		this.addRow(java.sql.Types.BLOB,"Blob");
		this.addRow(java.sql.Types.BOOLEAN,"Boolean");
		this.addRow(java.sql.Types.CHAR,"String");
		this.addRow(java.sql.Types.CLOB,"Clob");
		this.addRow(java.sql.Types.DATALINK,"UNKNOW");
		this.addRow(java.sql.Types.DATE,"java.sql.Date");
		this.addRow(java.sql.Types.DECIMAL,"java.math.BigDecimal"); 
		this.addRow(java.sql.Types.DISTINCT,"UNKNOW");
		this.addRow(java.sql.Types.DOUBLE,"Double");
		this.addRow(java.sql.Types.FLOAT,"java.math.BigDecimal");
		this.addRow(java.sql.Types.INTEGER,"Integer");
		this.addRow(java.sql.Types.JAVA_OBJECT,"UNKNOW");
		this.addRow(java.sql.Types.LONGVARBINARY,"byte[]");
		this.addRow(java.sql.Types.LONGVARCHAR,"String");
		this.addRow(java.sql.Types.NULL,"UNKNOW");
		this.addRow(java.sql.Types.NUMERIC,"java.math.BigDecimal"); //);
		this.addRow(java.sql.Types.OTHER,"UNKNOW");
		this.addRow(java.sql.Types.REAL,"Float");
		this.addRow(java.sql.Types.REF,"Ref");
		this.addRow(java.sql.Types.SMALLINT,"Short");
		this.addRow(java.sql.Types.STRUCT,"Struct");
		this.addRow(java.sql.Types.TIME,"java.sql.Time");
		this.addRow(java.sql.Types.TIMESTAMP,"java.sql.Timestamp");
		this.addRow(java.sql.Types.TINYINT,"Byte");
		this.addRow(java.sql.Types.VARBINARY,"byte[]");
		this.addRow(java.sql.Types.VARCHAR,"String"); 		
		this.addRow(java.sql.Types.NVARCHAR,"String"); 
		
		this.addRow(ORACLE_CURSOR,"jwide.JwideRefCursorDTO");		
	}

}
