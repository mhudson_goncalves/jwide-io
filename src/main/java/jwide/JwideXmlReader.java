package jwide;

import org.dom4j.io.SAXReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class JwideXmlReader {
	
	private JwideFileHandler fh = new JwideFileHandler();
	private DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
	
	public JwideXmlReader() {		
	}	
    
    public List<JwideDataObject> parseFromFile(String fileName,String node) throws Exception {
    	/* Node="/root/row": 
    	 * <root>
    	 *  <row>
    	 *   <id>1</id>
    	 *  </row>
    	 * </roow>
    	 */
    	return this.parseFromString(fh.loadFileToString(fileName),node);
    }    
    
    public  List<JwideDataObject> parseFromString(String xmlString,String node) throws Exception {
    	return this.parseFromString(xmlString, node,null);
    }
    public  List<JwideDataObject> parseFromString(String xmlString,String node,String encoding) throws Exception {
        SAXReader reader = new SAXReader();
    	
    	XmlDefaultProcessor xmlDef = new XmlDefaultProcessor();
    	    	
    	/* Node="/root/row": 
    	 * <root>
    	 *  <row>
    	 *   <id>1</id>
    	 *  </row>
    	 * </roow>
    	 */
    	
    	if (encoding!=null) reader.setEncoding(encoding);
    	
		reader.addHandler(node, xmlDef);
		reader.read(new InputSource(new StringReader(xmlString)));
		return xmlDef.getValues(); 	
    }
    
    public String toXml(JwideDataObject value) throws Exception {	

		DocumentBuilder icBuilder = icFactory.newDocumentBuilder();
		Document doc = icBuilder.newDocument();
		Element mainRootElement = doc.createElement("root");
		doc.appendChild(mainRootElement); 
		
		this.appendChild(doc, mainRootElement, value);
		return this.transform(doc);
	}
    
    public String toXml(List<JwideDataObject> values) throws Exception {	

		DocumentBuilder icBuilder = icFactory.newDocumentBuilder();
		Document doc = icBuilder.newDocument();
		Element mainRootElement = doc.createElement("root");
		doc.appendChild(mainRootElement); 
		
		for (JwideDataObject value:values){
			this.appendChild(doc, mainRootElement, value);
		}
		return this.transform(doc);
	}
    
    private void appendChild(Document doc, Element element, JwideDataObject value){
    	if (value.getValues()!=null){			
    		element.appendChild(createChilds(doc, "row", value.getColumns(), value.getValues()));			
		} else if (value.getColumns()!=null){
			List<Object> row = new ArrayList<>();
			for (int i=0;i<value.getColumns().size();i++){
				row.add(null);
			}
			element.appendChild(createChilds(doc, "row", value.getColumns(), row));
		}		
    }
    
    private String transform(Document doc) throws Exception {
    	StreamResult result=null;
    	
    	Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
		DOMSource source = new DOMSource(doc);

		result = new StreamResult(new StringWriter());
		transformer.transform(source, result); 

		return  result.getWriter().toString();
    }
	
    @SuppressWarnings("unchecked")
	private static Node createChilds(Document doc, String tag, List<String> columns, List<Object> values) {
		Element child = doc.createElement(tag);
			
		int i=0;
		for (String c:columns){
			Object value = values.get(i);					
					
			if (value==null) {
				child.appendChild(getElementValue(doc, c, null));
			} else if ( (value instanceof List<?>) && (((List<?>)value).size()>0) && (((List<?>)value).get(0) instanceof JwideDataObject)){
				Element arrayElement = doc.createElement(c);
				child.appendChild(arrayElement); 			
				
				for (JwideDataObject o:((List<JwideDataObject>)value)){
					arrayElement.appendChild(createChilds(doc, "row", o.getColumns(), o.getValues()));			
				}				
			} else {
				child.appendChild(getElementValue(doc, c, values.get(i)));
			}
			i++;
		}
				
		return child;
	}
	
	private static Node getElementValue(Document doc, String name, Object value) {		
		Element node = doc.createElement(name);
		
		if (value==null){
			// null
		} else if (value instanceof Integer){
			node.appendChild(doc.createTextNode( Integer.toString((Integer)value) ));
		} else if (value instanceof Double){
			node.appendChild(doc.createTextNode( Double.toString((Double)value) ));
		} else if (value instanceof BigDecimal){				
			node.appendChild(doc.createTextNode( String.valueOf ((BigDecimal)value) ));
		} else if (value instanceof Timestamp){				
			node.appendChild(doc.createTextNode( JwideUtil.getInstance().formatDate((Timestamp)value)));
		} else if (value instanceof Date){
			node.appendChild(doc.createTextNode( JwideUtil.getInstance().formatDate((Date)value)));
		} else if (value instanceof Boolean){
			node.appendChild(doc.createTextNode( Boolean.toString((Boolean)value)));
		} else if (value instanceof Long){
			node.appendChild(doc.createTextNode( Long.toString((Long)value) ));
		} else {
			node.appendChild(doc.createTextNode( value.toString() ));
		}
				
		return node;
	}
    
    public Boolean hasChild(String xmlValue) {
    	String xml=(xmlValue!=null?xmlValue:"");    	
    	return ((xml.indexOf("<")>=0)  && 
    			(xml.indexOf("</")>=0) &&
    			(xml.indexOf(">")>=0));
    }
}
