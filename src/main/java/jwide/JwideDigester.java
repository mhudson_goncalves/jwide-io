package jwide;

import java.security.MessageDigest;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

public class JwideDigester {
	private static JwideDigester instance;
	
	private static final String  DEFAULT_CHARSET = "ISO8859_1";

	private JwideDigester() {
    }
	
    public synchronized static JwideDigester getInstance() {
	    if( instance == null ) {
	        instance = new JwideDigester();
	    }
	    return instance;
	}
    
    public static String digestMD5( String value) throws Exception {
    	return digestMD5(value, DEFAULT_CHARSET);
    }
    
    public static String digestMD5( String value, String charSet ) throws Exception {
    	MessageDigest wmd = MessageDigest.getInstance( "MD5" );
    	wmd.reset();
    	wmd.update( value.getBytes( charSet ) );
    	return bytesToHex ( wmd.digest() ) ;
    }
    
    public static String digestSHA2( String value) throws Exception {
    	return digestSHA2(value, DEFAULT_CHARSET);
    }
    
    public static String digestSHA2( String value, String charSet  ) throws Exception {
    	MessageDigest digest = MessageDigest.getInstance("SHA-256");    	  	
    	return bytesToHex ( digest.digest( value.getBytes(charSet) )) ;
    }   
    
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
    
    public static String digestBASE64(byte[] value) throws Exception {
       return new BASE64Encoder().encode(value);
    }
    
    public static byte[] undigestBASE64(String value) throws Exception {
       return new BASE64Decoder().decodeBuffer(value);
    }   
    
    
}
