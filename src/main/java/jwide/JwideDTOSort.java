package jwide;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({ "unchecked", "rawtypes" }) 
public class JwideDTOSort implements Comparator {  
	
    private JwideUtil jwUtil = JwideUtil.getInstance();    
    private List<Integer> allColumnPos = new ArrayList<Integer>();    
    
    public static final int DESCENDING=0;
    public static final int ASCENDING=1;
    
    private int orderType=ASCENDING;
    
	public JwideDTOSort(JwideDTO DTO,String[] columns,int orderType) {
		setColumnPos(DTO,columns);
		this.orderType=orderType;
	}
	
	protected void setColumnPos(JwideDTO DTO,String[] columns) {
		for (String name:columns) {
			allColumnPos.add(DTO.getColumnPos(name));			
		}
	}
	
	public int compare(Object obj1, Object obj2) {				 
		if (obj1==null || obj2==null) return 0;
		
		StringBuffer[] compares = this.prepareToCompare(obj1, obj2);
		
		if (isAscending()) return (compares[0].toString().compareTo(compares[1].toString()));
		else               return (compares[1].toString().compareTo(compares[0].toString()));							    
	} 
	
	public StringBuffer[] prepareToCompare(Object obj1, Object obj2) {
		List<Object> row1=(List<Object>)obj1;
		List<Object> row2=(List<Object>)obj2;

		StringBuffer compare1 = new StringBuffer();
		StringBuffer compare2 = new StringBuffer();

		for (Integer columnPos:allColumnPos) {
			compare1.append(formatToCompare(row1.get(columnPos)).compareTo(formatToCompare(row2.get(columnPos))));					 
			compare2.append(formatToCompare(row2.get(columnPos)).compareTo(formatToCompare(row1.get(columnPos))));
		}
		return new StringBuffer[]{compare1,compare2};
		//return (compare1.toString().compareTo(compare2.toString()));	
		
	}
	
	private boolean isAscending() {
		return (orderType==ASCENDING);
	}


	/* Sem os fields:
	public int compare(Object obj1, Object obj2) {				 
		if (obj1==null || obj2==null) return 0;

		List<Object> row1=(List<Object>)obj1;
		List<Object> row2=(List<Object>)obj2;

		StringBuffer compare1 = new StringBuffer();
		StringBuffer compare2 = new StringBuffer();

		for (int i=0;i<row1.size();i++) {
			compare1.append(formatToCompare(row1.get(i)).compareTo(formatToCompare(row2.get(i))));					 
			compare2.append(formatToCompare(row2.get(i)).compareTo(formatToCompare(row1.get(i))));
		}
		return (compare1.toString().compareTo(compare2.toString()));					    
	} 
	*/  

	private String formatToCompare(Object value) {
		if (value==null) return "";
		if (value instanceof String)     return value.toString();
		if (value instanceof Double)     return jwUtil.formatFloat("000000000000.0000",(Double)value);
		if (value instanceof Integer)    return jwUtil.formatFloat("000000000000",(Integer)value);
		if (value instanceof Timestamp)  return jwUtil.formatFloat("000000000000000",((Timestamp)value).getTime());
		if (value instanceof BigDecimal) return jwUtil.formatFloat("000000000000.0000",((BigDecimal)value));
		return value.toString();
	}

}
