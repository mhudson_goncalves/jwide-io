package jwide;

import org.apache.commons.fileupload.FileItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

public class JwideUploadReceiver {
    
	private static final String UPLOAD_FOLDER="uploads";
	private String uploadedFileName="";
	
	public JwideUploadReceiver(){
		
	}
	
	private void setUploadedFileName(String uploadedFileName) {
		this.uploadedFileName=uploadedFileName;
	}
	
	public String getUploadedFileName() {
		return this.uploadedFileName;
	}
	
	public void saveFiles(HttpServletRequest request) throws Exception {
		String folderName = request.getSession().getServletContext().getRealPath(UPLOAD_FOLDER);
		this.saveFilesTo(request,folderName);
	}	
	
	public void saveFilesTo(HttpServletRequest request,String folderName) throws Exception {
		File dir = new File(folderName);
		if(!dir.exists()) {
		    dir.mkdir();
		}
		
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		List<FileItem> items = upload.parseRequest(request);

		for (FileItem item : items) {
			String name = formatUploadedFileName(item.getName());
			if (!name.trim().equals("")) {
				this.setUploadedFileName(name);
				String fileName=(folderName.endsWith(File.separator)?(folderName+uploadedFileName):(folderName+File.separator+uploadedFileName));
			    File uploadedFile = new File(fileName);
		        item.write(uploadedFile);
			}
		}
	}
	
	private String formatUploadedFileName(String fileName) {
		ArrayList<String> VALIDS = new ArrayList<String>(Arrays.asList("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","x","w","y","z","."));
		
		StringBuffer name = new StringBuffer("");
		if (fileName!=null) {
			for (char c:fileName.toLowerCase().toCharArray()) {
			    if (VALIDS.contains(c+"")) name.append(c);
			    else                       name.append("_");
			}
		}
		return name.toString();
	}
}