package jwide;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

public class JwideHttpRequest {

	private static final String POST_METHOD="POST";	
	private static final String GET_METHOD="GET";	
	private static final String PUT_METHOD="PUT";
	private static final String DELETE_METHOD="DELETE";
		
	protected static final String CHARSET="UTF-8";
	
	public enum ContentType {
		FORM,
		JSON,
		XML,
		SOAP
	}
	
	public enum CertificateType {
		JKS
	}
	
	private static final String FORM = "application/x-www-form-urlencoded";
	private static final String JSON = "application/json";
	private static final String XML = "text/xml; charset=utf-8";
	private static final String SOAP = "SOAPAction";
	
	private static final int SECURITY_PORT = 443;

	private List<String> header_names = new ArrayList<>();
	private List<String> header_values = new ArrayList<>();
	
	private JwideFileHandler fileHandler = new JwideFileHandler();
	
	private int responseCode=-1;
	private String responseMessage;	
	private int timeout = (90 * 1000); // 90 segundos
	private boolean securityEnabled = false;

	public JwideHttpRequest(){
	}	
	
	public void clear(){
		header_names.clear();
		header_values.clear();		
	}
	
	public void addHeaderValues(String name,String value){
		header_names.add(name);
		header_values.add(value);
	}	
	
	public void setTimeout(int timeout){
		this.timeout = timeout;
	}
	
	public int getTimeout(){
		return this.timeout;
	}
	
	// utilizar para quem usa certificados auto-assinado usar o keytool para gerar o jks a partir do .cer exportado pelo browser
	public void loadCertificate(CertificateType certificateType, String file, String password) throws Exception {
		if (!fileHandler.isFileCreated(file)){
			new JwideValidator().flush("JwideHttpRequest: Invalid certificate file: '" + file + "'");
		}
		
		System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
	    System.setProperty("javax.net.ssl.trustStoreType", certificateType.toString());
		System.setProperty("javax.net.ssl.trustStore", file); // path/arquivo.jks
		System.setProperty("javax.net.ssl.trustStorePassword", (password!=null?password:""));  
		securityEnabled = true;
	}
	
	public String postURL(String url, ContentType contentType, String data) throws Exception {		
		return this.connect(url, POST_METHOD, contentType, data);	
	}

	public String getURL(String url, ContentType contentType, String data) throws Exception {
		return this.connect(url, GET_METHOD, contentType, data);	
	}
	
	public String putURL(String url, ContentType contentType, String data) throws Exception {
		return this.connect(url, PUT_METHOD, contentType, data);	
	}
	
	public String deleteURL(String url, ContentType contentType, String data) throws Exception {
		return this.connect(url, DELETE_METHOD, contentType, data);	
	}
	
	public int getResponseCode(){
		return responseCode;
	}
	
	public String geResponseMessage(){
		return responseMessage;
	}
	
	private String connect(String urlVal, String method, ContentType contentType, String data) throws Exception {	
		responseCode=-1;
		responseMessage=null;
		
	    URL url = new URL(urlVal);		    
	    HttpURLConnection con = this.openConnection(url);	   	    
	   
	    con.setRequestMethod(method);	 
	    con.setInstanceFollowRedirects(true);
	    con.setConnectTimeout(timeout);
		con.setReadTimeout(timeout);
		
	    if (contentType!=null){	    	
	    	if (contentType == ContentType.FORM){
	    		this.addHeaderValueIfNotExists("Content-type", FORM);
	    	} else if (contentType == ContentType.JSON){
	    		this.addHeaderValueIfNotExists("Content-type", JSON);
	    	} else if (contentType == ContentType.XML){
	    		this.addHeaderValueIfNotExists("Content-type", XML);
	    	} else if (contentType == ContentType.SOAP){
	    		this.addHeaderValueIfNotExists("Content-type", XML);
	    		this.addHeaderValueIfNotExists(SOAP, "");
	    	}	    	
	    }
	    
	    this.setHttpHeaderValues(con);
	    SSLSocket socket = null;
	    if (securityEnabled){
	    	socket = (SSLSocket) HttpsURLConnection.getDefaultSSLSocketFactory().createSocket(url.getHost(), SECURITY_PORT);
	 		socket.startHandshake();    	
	    }

	    if (data!=null){
	    	
	    	con.setRequestProperty("Content-length", String.valueOf(data.length()));
	    	con.setDoOutput(true);
	    	con.setDoInput(true);

	    	DataOutputStream output = new DataOutputStream(con.getOutputStream());
	    	output.writeBytes(data);
	    	output.close();
	    }

	    responseCode = con.getResponseCode(); 
	    responseMessage=null;
	    
	    if (responseCode<HttpServletResponse.SC_BAD_REQUEST){
	    	try {
	    		responseMessage = IOUtils.toString(con.getInputStream(), "UTF-8");
	    	} catch (Exception e){
	    		responseMessage = "";
	    	}
	    } else {
	    	try {
	    		responseMessage = IOUtils.toString(con.getErrorStream(), "UTF-8");	   
	    	} catch (Exception e){
	    		responseMessage = "";
	    	}
	    }	    
	    
	    if (socket!=null){
	    	try {
	    		socket.close();
	    	} catch (Exception e){	    		
	    	}
	    }

	    return responseMessage;  
	}
	
	private HttpURLConnection openConnection(URL url) throws Exception {
		if (url.getProtocol().equalsIgnoreCase("https")){
			return (HttpsURLConnection) url.openConnection();
		} else {
			return (HttpURLConnection) url.openConnection();
		}
	}
	
	private void addHeaderValueIfNotExists(String name, String value){
		if (!this.isHeaderValueExists(name)){
			this.addHeaderValues(name, value);
		}
	}

	private boolean isHeaderValueExists(String name){
		if (name==null){
			return false;
		}
		
		for (String n:header_names){
			if (n.equalsIgnoreCase(name)){
				return true;
			}
		}
		
		return false;
	}
	
	public String encodeParams(String[] names,String[] values) throws Exception {		
		StringBuffer attributesBody = new StringBuffer("");

		int p=0;
		for (String name:names){
			if (attributesBody.length()>0){
				attributesBody.append("&");
			}
			attributesBody.append( URLEncoder.encode(name,CHARSET) + "="+ URLEncoder.encode(values[p++],CHARSET) );
		}

		return attributesBody.toString();
	}
	
	private void setHttpHeaderValues(HttpURLConnection httpCon){
		// Adicionar as propriedades
		int p=0;
		for (String name:header_names){
			httpCon.setRequestProperty( name, header_values.get(p++) );
		}
	}
	
}
