package jwide;

import java.util.ArrayList;
import java.util.List;

public class JwideStack {

	private static JwideStack instance=null;
	private static final List<String> stack = new ArrayList<>();
	private static final List<Object> values= new ArrayList<>();
	
	
	public static JwideStack getInstance(){
		if(instance==null){
			synchronized(JwideStack.class){
				instance=new JwideStack();
			}			
		}
		return instance;
	}	   
	
	private JwideStack() {		
	}
	
	public void set(String key,Object value){
		synchronized(instance){
			if (stack.contains(key)){
				values.set(stack.indexOf(key),value);
			} else {
				stack.add(key);
				values.add(value);
			}
		}
	}	
	
	public boolean insertIfNotExists(String key,Object value){
		synchronized(instance){
			if (stack.contains(key)){
				return false;
			} else {
				stack.add(key);
				values.add(value);
				return true;
			}
		}
	}	
	
	
	public Object get(String key){
		synchronized(instance){
			if (stack.contains(key)){
				return values.get(stack.indexOf(key));
			} else {
				return null;
			}
		}
	}	
	
	public boolean contains(String key){
		synchronized(instance){
			return stack.contains(key);
		}
	}
	
	public void remove(String key){		
		synchronized(instance){	
			if (stack.contains(key)){
				values.remove(stack.indexOf(key));
				stack.remove(stack.indexOf(key));
			}			
		}
	}

}
